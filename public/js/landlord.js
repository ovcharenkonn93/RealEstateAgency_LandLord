﻿// Начальные значения переменных - начало ------------------------------------------------------------------------------

var point_flag=false;
var map_flag=false;
var map;
var marks=[];
var latlngbounds;
var mc;
var infowindow;
var ajax_timeout=false, delay_beforesend=0; // Делать ли задержку при запуске обработчика формы

window.global_data; //глобальный справочник
window.max_cost = 20;

// Начальные значения переменных - конец -------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
$(document).ready (function () {
// Обработчики - начало ------------------------------------------------------------------------------------------------

    $('.ll-search-form__type-apartment').click (ApartmentClick); // Кнопка "Квартиры"
    $('.ll-search-form__type-household').click (HouseholdClick); // Кнопка "Дом"
    $('.ll-search-form__type-land').click (LandClick); // Кнопка "Участок"
    $('.ll-search-form__minus').click (CriteriaContract); // Сворачиваем критерии по клику на минус
    $('.ll-search-form__plus').click (CriteriaExpand); // Разворачиваем критерии по клику на плюс
    $('#ll-search-form__radio-sale').click (SaleClick); // Кнопка "Продажа"
    $('#ll-search-form__radio-rent').click (RentClick); // Кнопка "Аренда"
    $('.llg-button-for-contact-form').click (CallContactForm); // Вызываем контактную форму
    $('.ll-agent-contact-form-overlay').click (HideContactForm); // Прячем контактную форму
    $('.ll-agent-contact-form-modal').click (function(event){
        event.preventDefault();
        event.stopPropagation();
    }); // Экранирование от работы обработчика по клику на фоновый слой для модального окна формы "связаться со мной"
    $('.close-modal').click (HideContactForm); // Прячем контактную форму
    $('.ll-search-form__min-value').change(MinCostChange); // Изменение минимального значения слайдера
    $('.ll-search-form__max-value').change(MaxCostChange); // Изменение максимального значения слайдера
    $('.ll-search-form__city').change(SearchFormCityChange); // Обработка района
    $('.ll-search-form__district').change(SearchFormDistrictChange); // Обработка ориентира
    $('#ll-search-form').find('select, input').change(FormChange); //Задержка после изменения формы и запуск обработчика
    $('.ll-search-results-sorting').find('select, input').change(FormChange); // Задержка после изменения формы и запуск обработчика
    //$('.ll-search-form__min-value').bind("keypress", KeypressNumericFilter(event)); // Фильтрация ввода в поле. Разрешает вводить только числовые значения
    //$('.ll-search-form__max-value').bind("keypress", KeypressNumericFilter(event)); // Фильтрация ввода в поле. Разрешает вводить только числовые значения
    //$('.llg-grey-red-skin').click (GreyRedSkin); // Серо-красный скин
    //$('.llg-brown-skin').click (BrownSkin); // Коричневый скин
    //$('.llg-logo-variant-1').click (LogoVariant1);
    //$('.llg-logo-variant-1-1').click (LogoVariant11);
    //$('.llg-logo-variant-2').click (LogoVariant2);
    //$('.llg-logo-variant-2-1').click (LogoVariant21);
    //$('.llg-logo-variant-3').click (LogoVariant3);
    //$('.llg-logo-variant-3-1').click (LogoVariant31);
    //$('.llg-logo-variant-4').click (LogoVariant4);
    //$('.llg-logo-variant-4-1').click (LogoVariant41);
    //$('.llg-logo-variant-5').click (LogoVariant5);

// Обработчики - конец -------------------------------------------------------------------------------------------------

// Запуск функций - начало ---------------------------------------------------------------------------------------------

    // Запускаем функции на всех страницах (открывающая секция)


    // Через регулярные выражения находим первую папку.
    // var folder = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');
    // Отключено, т.к. не работает, если тестируем на локалхосте или 82 сервере

    var pathArray = window.location.pathname.split( '/' );
    var folder = pathArray[1];  // Уровень вложенности будет отличаться в зависимости от доменного имени, нужно будет
                                // добавить обработчик в зависимости от домена
    if (folder == 'landlord') {folder = pathArray[2];} // для локалхоста
	if (folder == 'landlord-site') {folder = pathArray[2];}

    switch (folder) {
        case '':
            InitHomePage (); // Запуск заглавной страницы
            break;
        case 'analytics':
            InitAnalyticsPage (); // Запуск страницы аналитики
            break;
        case 'agent-info':
            InitAgentInfoPage (); // Заполнение информации об агенте
            break;
        case 'agents':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            PaginationStart('#script-pagination', '#agents-list', '.ll-agents-grid'); // Запускаем панинацию
            break;
        case 'clients':
            PaginationStart('#script-pagination', '#data-list-clients', '.ll-clients__note'); // Запускаем панинацию
            break;
        case 'favorites':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            PaginationStart('#script-pagination', '#data-list-favorites', '.ll-search-result-favorites-data'); // Запускаем панинацию
            break;
        case 'info':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            break;
        case 'search':
            InitSearchPage (); // Запуск страницы поиска при загрузке
            break;
        case 'services':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            break;
        case 'services-for-buyer':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            break;
        case 'services-for-seller':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            break;
        case 'services-for-tenant':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            break;
        case 'services-for-owner':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            break;
        case 'services-additional':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            break;
        case 'sell':
            ReadReferences ('sell'); // Чтение справочников и заполнение форм
            break;
        case 'steps-to-buy':
            InitStepsPage (); // Запуск страницы шагов
            break;
        case 'steps-to-sell':
            InitStepsPage (); // Запуск страницы шагов
            break;
        case 'steps-to-rent-in':
            InitStepsPage (); // Запуск страницы шагов
            break;
        case 'steps-to-rent-out':
            InitStepsPage (); // Запуск страницы шагов
            break;
        case 'steps-to-documents':
            InitStepsPage (); // Запуск страницы шагов
            break;
        case 'user-page':
            FirstElementInTabulation (true); // Выделяем первый элемент табуляции
            break;
        default:
            console.log('Адрес страницы не обрабатывается. Страница:', folder);
    }

    // Запускаем функции на всех страницах (закрывающая секция)
    // ...

// Запуск функций - конец ----------------------------------------------------------------------------------------------

//======================================================================================================================
// Для кода ниже нужен рефакторинг. Вынести в функции и запускать в блоке выше.
//======================================================================================================================


// Cлайдер диапазона - начало ------------------------------------------------------------------------------------------
    $(".ll-search-form__slider").slider({
        min: 0,
        max: window.max_cost,
        values: [0,1000],
        range: true,
        stop: function(event, ui) {
            $(this).siblings(".ll-search-form__min-value").val($(this).slider("values",0));
            $(this).siblings(".ll-search-form__max-value").val($(this).slider("values",1));

        },
        slide: function(event, ui){
            $(this).siblings(".ll-search-form__min-value").val($(this).slider("values",0));
            $(this).siblings(".ll-search-form__max-value").val($(this).slider("values",1));
        }
    });
// Cлайдер диапазона - конец -------------------------------------------------------------------------------------------

// Фильтрация ввода в поля
        $('.ll-search-form__min-value').add('.ll-search-form__max-value').keypress(function(event){
            var key, keyChar;
            if(!event) var event = window.event;

            if (event.keyCode) key = event.keyCode;
            else if(event.which) key = event.which;

            if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
            keyChar=String.fromCharCode(key);

            if(!/\d/.test(keyChar))	return false;
        });

// Колонки одинаковой высоты
    var maxheight = 0;
    $("div.llg-columns-with-similar-height").each(function() {
        if($(this).height() > maxheight) { maxheight = $(this).height(); }
    });

    $("div.llg-columns-with-similar-height").height(maxheight);

})