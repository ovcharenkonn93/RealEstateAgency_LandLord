// Этот файл javascript-функций собирается из нескольких .js файлов, которые находятся в папке \resources\javascript-functions\
// Его не нужно изменять, т.к. он будет перезаписан и изменения будут потеряны.
// Сборка идёт через Elixir. См. http://laravel.su/docs/5.0/elixir
// Настройки сборки находятся в файле \gulpfile.js
// ---------------------------------------------------------------------------------------------------------------------
// Добавление (удаление) в избранное
function AddToFavorites(id) {
    console.log('Запустилась функция AddToFavorites('+id+')');
    var favorites=JSON.parse($.cookie("favorites"));
	if(!favorites){
		$.cookie("favorites",JSON.stringify([id]));
		$("#fa-star-o"+id.toString()).removeClass("fa-star-o").addClass("fa-star  yellow-star");
	}
	else {
		pos=favorites.indexOf(id);
		if(pos==-1){
			favorites.push(id);
			$("#fa-star-o"+id.toString()).removeClass("fa-star-o").addClass("fa-star  yellow-star");
			$.cookie("favorites",JSON.stringify(favorites));
		}
		else {
			favorites.splice(pos,1);
			$("#fa-star-o"+id.toString()).removeClass("fa-star  yellow-star").addClass("fa-star-o");
			$.cookie("favorites",JSON.stringify(favorites));
		}
	}
}
// ---------------------------------------------------------------------------------------------------------------------
// Запрос к API для получения списка
function AjaxRequest(criteria) {
console.log('Запустилась функция AjaxRequest');
if(!this.no_first_launch){
	this.no_first_launch=1;
	criteria+="&update=1";
}
console.log('Параметры запроса к API:', criteria);
	$.ajax({
		url: 'search-sb/get_coordinates',
		type: 'GET',
		data: criteria,
		success: function(response){
			data=JSON.parse(response);
			marks.forEach(function(mark){
				mark.setMap(null);
			})
			marks.length=0;
			point_flag=true;
			if(map_flag){
				for(var i=0;i<marks.length;i++)
				{
					marks[i].setMap(null);
				}
				marks.length=0;
				data.forEach(function(coord){
					var mark=new google.maps.Marker({position: {lat: coord.latitude, lng: coord.longitude}, map: map});
					mark.addListener("click", function(){
						$.ajax({
							url: 'search-sb/get_estate_from_id',
							type: 'GET',
							data: 'id='+coord.id,
							success: function(response){
								/*var infowindow= new google.maps.InfoWindow({
									content: response
								});*/
								infowindow.setContent(response);
								infowindow.open(map, mark);
							}
						})
					});
					//mark.addListener("click", view_map_estate(mark,coord.id));
					marks.push(mark);
					latlngbounds.extend(mark.position);
				})
				map.setCenter( latlngbounds.getCenter(), map.fitBounds(latlngbounds));
			}
			else{
				data.forEach(function(coord){
				var mark=new google.maps.Marker({position: {lat: coord.latitude, lng: coord.longitude}});
				marks.push(mark);
				})
			}
		}
	});
    $.ajax(
        {
            url: 'search-sb/get',
            type: 'GET',
            data: criteria,
            success: function(data){
                $('#data-list').html(data);
                //console.log('Результат запроса к API:', data);
                $('.data-list').show();
                $('.data-grid').show();
                $('.search-result_empty').hide();
                $('.ll-pagination').show();
                $('.ll-search-results-limit').show();
                $('.ll-search-results-sorting').show();               

                if (data.length==0){
                    $('.data-list').hide();
                    $('.data-grid').hide();
                    $('.search-result_empty').show();
                    $('.ll-pagination').hide();
                    $('.ll-search-results-limit').hide();
                    $('.ll-search-results-sorting').hide();
                }

                var min_cost=Infinity, max_cost=-Infinity,
                    min_floor=Infinity, max_floor=-Infinity,
                    min_total_area=Infinity, max_total_area=-Infinity;
                var a=JSON.parse(data);
                var i=0;
                a.forEach(function(apartment)
                {
                    if(apartment.cost&&apartment.cost<min_cost) min_cost=apartment.cost;
                    if(apartment.special_price&&apartment.special_price<min_cost) min_cost=apartment.special_price;
                    if(apartment.cost&&apartment.cost>max_cost) max_cost=apartment.cost;
                    if(apartment.special_price&&apartment.special_price>max_cost) max_cost=apartment.special_price;

                    if(apartment.floor<min_floor) min_floor=apartment.floor;
                    if(apartment.floor>max_floor) max_floor=apartment.floor;

                    if(apartment.total_area<min_total_area) min_total_area=apartment.total_area;
                    if(apartment.total_area>max_total_area) max_total_area=apartment.total_area;
                });
                $('.ll-search-form__cost .ll-search-form__min-value').val(min_cost);
                $('.ll-search-form__cost .ll-search-form__max-value').val(max_cost);

                $('.ll-search-form__floor .ll-search-form__min-value').val(min_floor);
                $('.ll-search-form__floor .ll-search-form__max-value').val(max_floor);

                $('.ll-search-form__total-area .ll-search-form__min-value').val(min_total_area);
                $('.ll-search-form__total-area .ll-search-form__max-value').val(max_total_area);
            }
        });

    if (window.location.href.search('search') != -1) {
        setTimeout(paginationList, 3000);
        setTimeout(paginationGrid, 3000);
    }
}
// ---------------------------------------------------------------------------------------------------------------------

// Выбор кнопки "Квартира"
function ApartmentClick () {
    console.log('Запустилась функция ApartmentClick - Выбор кнопки "Квартира"');

    $("#ll-search-form__estate-type_apartment").prop("checked", true);
    $('.ll-search-form__type-apartment').css('borderBottom','none');
    //$('.ll-search-form__type-apartment').css("border-bottom","none");
    $('.ll-search-form__type-land').css('borderBottom','solid 1px #DDDDDD');
    $('.ll-search-form__type-household').css('borderBottom','solid 1px #DDDDDD');

    $('.llg-for-rent').hide();
    $('.llg-for-land').hide();
    $('.llg-for-household').hide();
    $('.llg-for-appartment').show();
}
// ---------------------------------------------------------------------------------------------------------------------
// Вызываем контактную форму
function CallContactForm () {
    console.log('Запустилась функция CallContactForm');
    $('.ll-agent-contact-form-overlay').css("display","block");
    $('div.ll-agent-contact-form-overlay').addClass("animation_modal");


   // $('.ll-agent-contact-form-content').css({display:"block", -webkit-animation: "fade .6s", -moz-animation: "fade 0.6s", animation: "fade 0.6s"});
  //  $('.ll-agent-contact-form-overlay-black').css('display','block');
}
// ---------------------------------------------------------------------------------------------------------------------
// выбор изображения на главной
function ChangeVisualOnHomePage ()  {
    console.log('Запустилась функция ChangeVisualOnHomePage для выбора изображения на главной');
	var img_number;
	  main_img=$.cookie('image');
	  
		 if (main_img==null) {
			    $.cookie('image', "1", { expires: 30, path: '/'} );
			}
		 if (main_img=="8") {
			    $.cookie('image', "1", { expires: 30, path: '/'} );
			}			
			else {	
				main_img=parseInt($.cookie('image')); 
				main_img=main_img+1;
				$.cookie('image', main_img.toString(), { expires: 30, path: '/'} );				
			}

			if ($.cookie('image')=="1") $('.ll-home-without-carousel').css( 'background-image', 'url("' + window.location.href + 'images/visuals/home-slider-1.jpg")' );
			//if ($.cookie('image')=="2") $('.ll-home-without-carousel').css( 'background-image', 'url("' + window.location.href + 'images/visuals/home-slider-1.jpg")' );
			if ($.cookie('image')=="3") $('.ll-home-without-carousel').css( 'background-image', 'url("' + window.location.href + 'images/visuals/home-slider-2.jpg")' );
			if ($.cookie('image')=="4") $('.ll-home-without-carousel').css( 'background-image', 'url("' + window.location.href + 'images/visuals/home-slider-3.jpg")' );
			if ($.cookie('image')=="5") $('.ll-home-without-carousel').css( 'background-image', 'url("' + window.location.href + 'images/visuals/home-slider-4.jpg")' );
			if ($.cookie('image')=="6") $('.ll-home-without-carousel').css( 'background-image', 'url("' + window.location.href + 'images/visuals/home-slider-5.jpg")' );
			if ($.cookie('image')=="7") $('.ll-home-without-carousel').css( 'background-image', 'url("' + window.location.href + 'images/visuals/home-slider-7.jpg")' );
			if ($.cookie('image')=="8") $('.ll-home-without-carousel').css( 'background-image', 'url("' + window.location.href + 'images/visuals/home-slider-8.jpg")' );			
}
// ---------------------------------------------------------------------------------------------------------------------

// Построение графика
function Chartist_Bar() {
    console.log('Запустилась функция Chartist_Bar');
    new Chartist.Bar('#ct-chart-bar', {
        labels: ['First quarter of the year', 'Second quarter of the year', 'Third quarter of the year', 'Fourth quarter of the year'],
        series: [
            [60000, 40000, 80000, 70000],
            [40000, 30000, 70000, 65000],
            [8000, 3000, 10000, 6000]
        ]
    }, {
        seriesBarDistance: 10,
        axisX: {
            offset: 60
        },
        axisY: {
            offset: 80,
            labelInterpolationFnc: function(value) {
                return value + ' CHF'
            },
            scaleMinSpace: 15
        }
    });
}
// ---------------------------------------------------------------------------------------------------------------------
// Построение графика
function Chartist_Line() {
    console.log('Запустилась функция Chartist_Line');
    new Chartist.Line('#ct-chart-line', {
        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
        series: [
            [12, 9, 7, 9, 5],
            [2, 1, 3.5, 7, 3],
            [1, 3, 4, 5, 6]
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 40
        }
    });
}
// ---------------------------------------------------------------------------------------------------------------------
// Построение графика
function Chartist_Pie() {
    console.log('Запустилась функция Chartist_Pie');
    var data = {
        series: [5, 3, 4]
    };

    var sum = function(a, b) { return a + b };

    new Chartist.Pie('#ct-chart-pie', data, {
        labelInterpolationFnc: function(value) {
            return Math.round(value / data.series.reduce(sum) * 100) + '%';
        }
    });
}
// ---------------------------------------------------------------------------------------------------------------------
// Сворачиваем критерии по клику на минус
function CriteriaContract () {
    console.log('Запустилась функция CriteriaContract');
    $(this).hide();
    $(this).siblings().hide();
    $(this).siblings(".ll-search-form__diapason-label").show();
    $(this).siblings(".ll-search-form__plus").show();
}
// ---------------------------------------------------------------------------------------------------------------------
// Разворачиваем критерии по клику на плюс
function CriteriaExpand () {
    console.log('Запустилась функция CriteriaExpand');
    $(this).siblings().show();
    $(this).hide();
}
// ---------------------------------------------------------------------------------------------------------------------
// Удаление со страницы favorites
function DelFromFavorites(atf)
{
    console.log('Запустилась функция DelFromFavorites');
    string=$(this).parent();// выбираем всю строку в таблице
    item_id=parseInt(atf.id);  //получаем id товара

    string.remove();// удаляем строку

    order=JSON.parse($.cookie('favorites'));//получаем массив с объектами из куки
    for(var i=0;i<order.length; i++)
    {
        if(order[i].item_id==item_id)
        {
            order.splice(i,1); //удаляем из массива объект
        }
    }

    $.cookie('favorites', JSON.stringify(order), { expires: 7, path: '/'} );

    star_color();
}
// ---------------------------------------------------------------------------------------------------------------------
// Заполняем select значениями из справочника
// FillOptions (data, 'Имя таблицы','Имя поля','селектор CSS куда будут подставляться значения')
function FillOptions (base, table, field, selector) {
    console.log('Запустилась функция FillOptions для заполнения ', selector, ' значениями из справочника');
    var element = base[table];
    $.each(element, function(index, value){
        var elementvalue = element[index];
        var datatokens='';
        if (selector=='.ll-search-form__city') {
            datatokens=' data-tokens="'+elementvalue[field]+'" ';
        }
        $(selector).append("<option value="+elementvalue["id"]+datatokens+">"+elementvalue[field]+"</option>");
    });
}
// ---------------------------------------------------------------------------------------------------------------------
// Заполняем результаты поиска
// FillSearchResult (массив с объектами, селектор блока внутри которого будут вставляться данные, номер элемента массива)
function FillSearchResult (dataarray, selector) {
    console.log('Запустилась функция FillSearchResult для', selector);
    $(selector).show();

// Для универсальности выносим значения на верхний уровень
    try {dataarray.street = dataarray.address.street;} catch(error) {}
    try {dataarray.city = dataarray.address.type_district.city.city;} catch(error) {}
    try {dataarray.city = dataarray.address.type_administrative_district.city.city;} catch(error) {}
    try {dataarray.type_apartment = dataarray.type_apartment.type_apartment;} catch(error) {}
    try {dataarray.phone_mob = dataarray.employee_assigned.phone_mob;} catch(error) {}
    try {dataarray.employee_assigned_id = dataarray.employee_assigned.id;} catch(error) {}
    try {dataarray.employee_assigned_src = dataarray.employee_assigned.src;} catch(error) {}

// Заполняем стандартные текстовые блоки
    var textfields = ['name', 'surname', 'patronymic', 'city', 'district', 'district_admin', 'street', 	'floor',
        'floor_all', 'total_area', 'living_area', 'photo_count', 'text_website', 'phone_mob', 'type_apartment',
        'created_at', 'email', 'photo_count'];
    $.each(textfields, function(index, value){
        $(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).text(dataarray[value]);
		console.log(dataarray[value]);
    });

// Заполняем стандартные блоки изображений
    var host = 'http://ll1.dego1n.ru/';
    var imagefields = ['cover_src', 'employee_assigned_src', 'src'];
    $.each(imagefields, function(index, value){
        $(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).attr('src',host+dataarray[value]);
    });

// Заполняем стандартные блоки ссылок
    var linkfields = ['', ''];
    $.each(linkfields, function(index, value){
        $(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).attr("href",dataarray[value]);
    });

// Заполняем нестандартные блоки
    if ((dataarray.special_price == '') ||
        (dataarray.special_price == null) ||
        (dataarray.special_price == 0)) {
        $(selector).find('.ll-data__new-price').text(number_format(dataarray['cost'],0,'',' '));
        $(selector).find('.ll-data__old-price').text('');
        $(selector).find('.ll-estate-info__old-price').hide ();
        $(selector).find('.ll-search-result-list__old-price').hide ();
        $(selector).find('.ll-search-result-grid__old-price').hide ();
    } else {
        $(selector).find('.ll-data__new-price').text(number_format(dataarray['special_price'],0,'',' '));
        $(selector).find('.ll-data__old-price').text(number_format(dataarray['cost'],0,'',' '));
    }

    if ((dataarray.text_website == '') || (dataarray.text_website == null)) {
        $('.ll-estate-info__description').hide ();
    }

    $(selector).find('.ll-data__link').attr("href",'info/'+dataarray['id']);
    $(selector).find('.ll-data__agent-link').attr("href",'../agent-info/'+dataarray['employee_assigned_id']);

    $(selector).find('.add-to-favorites').attr("id",dataarray['id']);
    $(selector).find('.list-fa-star-o').attr("id",'fa-star-o'+dataarray['id']);
    $(selector).find('.grid-fa-star-o').attr("id",'grid-fa-star-o'+dataarray['id']);
    //star_color(); // Изменяем цвет звёздочки для избранного
//--------------Инициализация карты
    var latitude = 0;
    var longitude = 0;
    try {latitude = dataarray.address.latitude;} catch(error) {}
    try {longitude = dataarray.address.longitude;} catch(error) {}
    //if (latitude != 0) and (longitude != 0) {
    //console.log('Широта:',latitude);
    //console.log('Долгота:',longitude);
    //var myMap ;
    //ymaps.ready(init);
    //function init () {
    //myMap=new ymaps.Map("map", {
    //center: [latitude, longitude],
    //zoom: 7,
    //type: "yandex#map",
    // Карта будет создана без
    // элементов управления.
    //controls: []
    //});
    /* }, {
     searchControlProvider: 'yandex#search'
     });*/
    //}
    //}

// Ещё нужно заполнить такие блоки

//ll-estate-info__title
//ll-data__adress
//ll-data__city
//ll-estate-info__date-of-publication
$('#SearchRooms').selectpicker('refresh');
}
// ---------------------------------------------------------------------------------------------------------------------
// Выдеяем первый элемент табуляции
function FirstElementInTabulation (first) {
    console.log('Запустилась функция FirstElementInTabulation');
    $(".ll-toggle li").each(function() {
        if ($(this).css('display') !== 'none') {
            $(this).addClass("active");
            if (first) return false;
            $(this).removeClass("active");
        }
    });
    $('.llg-for__all').removeClass("active");
}
// ---------------------------------------------------------------------------------------------------------------------
// Задержка после изменения формы и запуск обработчика
function FormChange() {
    console.log('Запустилась функция FormChange');
    if(ajax_timeout) clearTimeout(ajax_timeout);
    ajax_timeout=setTimeout(sendForm,delay_beforesend)
}
// ---------------------------------------------------------------------------------------------------------------------
// Получаем параметры из адресной строки и подставляем в форму
function GetParametersFromURL () {
    console.log('Запустилась функция GetParametersFromURL для обработки параметров адресной строки и изменения формы');

    $.urlParam = function(name){ //получить параметры из адресной строки
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }

    var rooms = '';
    var realestate_type_value = 'apartment';

    if (window.location.href.search('deal_type') != -1) { //если параметры поиска переданы
        if (window.location.href.search('city') != -1) {
            console.log('Меняем город. city =', $.urlParam('city'));
            $('#ll-search-form__city').get(0).selectedIndex = $.urlParam('city');
            $("#ll-search-form__city [value='"+$.urlParam('city')+"']").attr("selected", "selected");
        }
        if (window.location.href.search('price') != -1) {
            $('.ll-search-form__cost .ll-search-form__min-value').val(0);
            $('.ll-search-form__cost .ll-search-form__max-value').val($.urlParam('price'));
        }

        console.log('Нужно дописать обработку комнат - сделать мультиселект');
        if (window.location.href.search('rooms') != -1) {
            rooms = '&id_type_apartment='+$.urlParam('rooms');
            rooms = rooms.replace("one","5;6;7;");
            rooms = rooms.replace("two","8;9;");
            rooms = rooms.replace("three","10;");
            rooms = rooms.replace("more","11;12;");
        }

        if (window.location.href.search('select_type') != -1) {
            switch ($.urlParam('select_type')) {
                case '':
                    console.log('select_type не передавался');
                    break;
                case 'households':
                    HouseholdClick ();
                    break;
                case 'land':
                    LandClick ();
                    break;
                default:
                    ApartmentClick ();
                    break;
            }
        } else ApartmentClick ();

        if (window.location.href.search('deal_type') != -1) {
            switch ($.urlParam('deal_type')) {
                case '':
                    console.log('deal_type не передавался');
                    break;
                case 'rent':
                    RentClick ();
                    break;
                default:
                    SaleClick ();
                    break;
            }
        } else SaleClick ();
    } else // Если параметры поиска не переданы
    {
        ApartmentClick ();
        SaleClick ();
    }
}
// ---------------------------------------------------------------------------------------------------------------------
// Прячем контактную форму
function HideContactForm () {
    console.log('Запустилась функция HideContactForm');
    $('div.ll-agent-contact-form-overlay').removeClass("animation_modal");
    $('.ll-agent-contact-form-overlay').css("display","none");
  //  $('.ll-agent-contact-form-overlay-black').css('display','none');
}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор кнопки "Дом"
function HouseholdClick () {
    console.log('Запустилась функция HouseholdClick - Выбор кнопки "Дом"');

    $("#ll-search-form__estate-type_household").prop("checked", true);
    $('.ll-search-form__type-household').css('borderBottom','none');
    $('.ll-search-form__type-apartment').css('borderBottom','solid 1px #DDDDDD');
    $('.ll-search-form__type-land').css('borderBottom','solid 1px #DDDDDD');
    $('.llg-for-rent').hide();
    $('.llg-for-land').hide();
    $('.llg-for-appartment').hide();
    $('.llg-for-household').show();
    $.ajax(
        {
            url: 'api',
            type: 'GET',
            data: {
                type: 'search',
                format: 'plain',
                realestate_type: 'households'
            },
            success: function(data){
                var min=Infinity, max=-Infinity;
                var a=JSON.parse(data);
                a.forEach(function(households)
                {
                    if(households.cost&&households.cost<min) min=households.cost;
                    if(households.special_price&&households.special_price<min) min=households.special_price;
                    if(households.cost&&households.cost>max) max=households.cost;
                    if(households.special_price&&households.special_price>max) max=households.special_price;
                });
                $('.ll-search-form__cost .ll-search-form__min-value').val(min_cost);
                $('.ll-search-form__cost .ll-search-form__max-value').val(max_cost);
            }
        });
}
// ---------------------------------------------------------------------------------------------------------------------
// Запуск страницы со списком агентов
function InitAgentInfoPage () {
    console.log('Запустилась функция InitAgentInfoPage');
    FirstElementInTabulation (true); // Выделяем первый элемент табуляции
    // пока выполняется скрипт из resources/views/layouts/agent-info.blade.php т.к. используется {{ $agentInfoId }}
}
// ---------------------------------------------------------------------------------------------------------------------
// Запуск страницы аналитики
function InitAnalyticsPage () {
    console.log('Запустилась функция InitAnalyticsPage');
    ReadReferences ('analytics'); // Чтение справочников и заполнение форм
    Chartist_Line();
    Chartist_Pie();
    Chartist_Bar();
}
// ---------------------------------------------------------------------------------------------------------------------
// Запуск заглавной страницы
function InitHomePage () {
    console.log('Запустилась функция InitHomePage');
    $('#ll-search-form-home__select-type').change(SearchFormEstateTypeChange);
    $(".llg-button-search").click (SearchButtonClick);

    //$('#ll-search-form-home__radio-sale').click (SwitchToSell);
    //$('#ll-search-form-home__radio-for-rent').click (SwitchToSell);
    //$('#ll-search-form-home__radio-buy').click (SwitchToSearch);
    //$('#ll-search-form-home__radio-rent').click (SwitchToSearch);
    //$('.llg-news-variant-1').click (NewsVariant1);
    //$('.llg-news-variant-2').click (NewsVariant2);
    //$('.llg-news-variant-3').click (NewsVariant3);
    //$('.llg-news-variant-4').click (NewsVariant4);

    ReadReferences ('home'); // Чтение справочников и заполнение форм
    ChangeVisualOnHomePage (); // Смена изображений на главной
}
// ---------------------------------------------------------------------------------------------------------------------
// Запуск страницы поиска при загрузке
function InitSearchPage () {
    console.log('Запустилась функция InitSearchPage');

    FirstElementInTabulation (true); // Выделяем первый элемент табуляции
    ReadReferences ('search'); // Читаем справочники и заполнеяем форму
    GetParametersFromURL (); // Получаем параметры из адресной строки и подставляем в форму
    sendForm(); // Отправляем форму
}
// ---------------------------------------------------------------------------------------------------------------------
// Запуск страницы шагов
function InitStepsPage () {
    console.log('Запустилась функция InitStepsPage');
    $('#carousel_banner').carouFredSel({
        synchronise: '#carousel_text',
        width: '100%',
        items: {
            visible: 3
        },
        scroll: {
            items: 1,
            duration: 1100,
            timeoutDuration: 7500
        },
        pagination: {
            container: '#carousel_pagination',
            anchorBuilder: function(nr) {
                return '<a id="step_'+nr+'" href="#"></a>';
            }
        },
        auto: {
            play:true
        },
        prev: '#prevCarou',
        next: '#nextCarou'
    });

    // Carousel for text
    $('#carousel_text').carouFredSel({
    auto: false
    });

//Since this carousel shows 3 slides at a time, it likes to put the #2 slide in the center, with the #1 slide on the left of the screen -
//The best way I found to correct this is to put the last slide first in the #carousel_banner div, followed by the actual #1 slide. -->

$('.ll-steps-to-buy-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-1.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-2.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-3.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-4.jpg';";
});
$('.ll-steps-to-buy-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-5.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-6.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-7.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-8').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-8.jpg');";
});

$('.ll-steps-to-sell-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-1.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-2.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-3.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-4.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-5.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-6.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-7.jpg');";
});

$('.ll-steps-to-rent-in-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-1.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-2.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-3.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-4.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-5.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-6.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-7.jpg');";
});

$('.ll-steps-to-rent-out-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-1.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-2.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-3.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-4.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-5.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-6.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-7.jpg');";
});

$('.ll-steps-to-documents-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-1.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-2.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-3.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-4.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-5.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-6.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-7.jpg');";
});

}
// ---------------------------------------------------------------------------------------------------------------------
/**
 * Created by Николай on 21.09.2016.
 */


    function Init_input_range_slider(){

    var tracks = [
        '-webkit-slider-runnable-track',
    ];

    var thumbs = [
        '-webkit-slider-thumb',
    ];

    initSliders_range();





    }


    function initSliders_range(){
    var sliders = document.querySelectorAll('input[type=range]');
    var testAndWK = window.getComputedStyle(sliders[0],'::-webkit-slider-thumb').background;
    for (var i=0;i<sliders.length;i+=1) {
        if (!testAndWK) {
            sliders[i].style.WebkitAppearance = 'slider-horizontal';
        }


    }

    }


// Фильтрация ввода в поле. Разрешает вводить только числовые значения
function KeypressNumericFilter(event) {
    console.log('Запустилась функция KeypressNumericFilter');
        var key, keyChar;
        if(!event) var event = window.event;

        if (event.keyCode) key = event.keyCode;
        else if(event.which) key = event.which;

        if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
        keyChar=String.fromCharCode(key);

        if(!/\d/.test(keyChar))	return false;
}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор кнопки "Участок"
function LandClick () {
    console.log('Запустилась функция LandClick - Выбор кнопки "Участок"');

    $("#ll-search-form__estate-type_land").prop("checked", true);
    $('.ll-search-form__type-land').css('borderBottom','none');
    $('.ll-search-form__type-apartment').css('borderBottom','solid 1px #DDDDDD');
    $('.ll-search-form__type-household').css('borderBottom','solid 1px #DDDDDD');

    $('.llg-for-rent').hide();
    $('.llg-for-household').hide();
    $('.llg-for-appartment').hide();
    $('.llg-for-land').show();
    $.ajax(
        {
            url: 'api',
            type: 'GET',
            data: {
                type: 'search',
                format: 'plain',
                realestate_type: 'land'
            },
            success: function(data){
                var min=Infinity, max=-Infinity;
                var a=JSON.parse(data);
                a.forEach(function(land)
                {
                    if(land.cost&&land.cost<min) min=land.cost;
                    if(land.special_price&&land.special_price<min) min=land.special_price;
                    if(land.cost&&land.cost>max) max=land.cost;
                    if(land.special_price&&land.special_price>max) max=land.special_price;
                });
                $('.ll-search-form__cost .ll-search-form__min-value').val(min_cost);
                $('.ll-search-form__cost .ll-search-form__max-value').val(max_cost);
            }
        });
}
// ---------------------------------------------------------------------------------------------------------------------
// Изменение максимального значения слайдера
function MaxCostChange () {
	//от функции одни глюки
    console.log('Запустилась функция MaxCostChange');
	/*
    var value1=$(".ll-search-form__min-value").val();
    var value2=$(".ll-search-form__max-value").val();

    if (value2 > 1000) { value2 = 1000; $(".ll-search-form__max-value").val(1000)}

    if(parseInt(value1) > parseInt(value2)){
        value2 = value1;
        $(".ll-search-form__max-value").val(value2);
    }
    $(this).siblings(".ll-search-form__slider").slider("values",1,value2);
	*/
}
// ---------------------------------------------------------------------------------------------------------------------
//максимальное значение диапазона
function max_cost () {
    console.log('Запустилась функция max_cost');
    $.ajax(
        {
            url: 'api',
            type: 'GET',
            data: {
                // Обязательные параметры: [ type, format, realestate_type]
                type: 'search',
                format: 'plain',
                realestate_type: 'apartment'
            },
            success: function(data){
                window.max_cost = 60;
                var estate = JSON.parse(data);
                //console.log('Недвижимость:', estate);

                //for(var i=0;i<window.global_estate.length; i++)
                //{
                //if(window.global_estate[i].cost>window.max_cost)
                //{
                window.max_cost = 50;//window.global_estate[i].cost;
                //}
                //}
            }
        });
}
// ---------------------------------------------------------------------------------------------------------------------
// Изменение минимального значения слайдера
function MinCostChange () {
    console.log('Запустилась функция MinCostChange');
    var value1=$(".ll-search-form__min-value").val();
    var value2=$(".ll-search-form__max-value").val();

    if(parseInt(value1) > parseInt(value2)){
        value1 = value2;
        $(".ll-search-form__min-value").val(value1);
    }
    $(this).siblings(".ll-search-form__slider").slider("values",0,value1);
}
// ---------------------------------------------------------------------------------------------------------------------
function number_format(number, decimals, dec_point, thousands_sep) {
    console.log('Запустилась функция number_format');
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}
function paginationGrid() {
    console.log('Запустилась функция paginationGrid');
    PaginationStart('#script-pagination-grid', '#data-grid', '.ll-search-result-grid-data');
    star_color(); // Изменяем цвет звёздочки для избранного
}
// ---------------------------------------------------------------------------------------------------------------------
// Пагинация результатов поиска
function paginationList() {
    console.log('Запустилась функция paginationList');
    PaginationStart('#script-pagination', '#data-list', '.ll-search-result-list-data')
    star_color(); // Изменяем цвет звёздочки для избранного
}
// ---------------------------------------------------------------------------------------------------------------------
// Запускаем панинацию
// container - лок, в котором отрисовывается панинация
function PaginationStart(container, parent, child) {
    console.log('Запустилась функция PaginationStart для ', child);
    window.tp = new Pagination(container, {
        itemsCount: $(child).length, //количество видимых блоков
        onPageChange: function (paging) {
            var start = paging.pageSize * (paging.currentPage - 1),
                end = start + paging.pageSize,
                $rows = $(parent).find(child);
            $rows.hide();
            for (var i = start; i < end; i++) {
                $rows.eq(i).show();
            }
        }
    });
}
// ---------------------------------------------------------------------------------------------------------------------
function photo_placeholder()
{
    console.log('Запустилась функция photo_placeholder'); 
		var coversrc = document.getElementsByClassName('ll-data__cover-src');

	    for (i = 0; i < coversrc.length; i++) {

	        if (coversrc[i].src == ''){
				coversrc[i].src = 'http://localhost/landlord-site/public/images/placeholders/no-photo-estate.png';
			}

	    }
}
// Читаем значения из базы данных
function ReadField (field) {
    console.log('Запустилась функция ReadField для ', field);
    try {
        var dataitem = dataarray[index];
        var value= dataitem[field];
    }
    catch(error) {
        var value= '';
    }
    return value;
}
// ---------------------------------------------------------------------------------------------------------------------	
// Чтение справочников и заполнение форм
function ReadReferences (page)
{
    console.log('Запустилась функция ReadReferences для чтения справочников на странице ', page);

    // Выбираем какие справочники считывать в зависимости от страницы
    switch (page) {
        case 'home': // Заглавная страница
            var tables = 'type_cities;type_profile';
            break;
        default:
            var tables = 'all';
            break;
    }
    console.log('Справочники: ', tables);

    $.ajax({
        url:'SearchForm',
        data:
        {
            type: 'reference',
            tables: tables,
            format: 'plain'
        },
        type: "GET",
        success: function (data) {
            if(data)
            {
                window.global_data = JSON.parse(data);

                // Выбираем какие поля заполнять в зависимости от страницы
                // Формат FillOptions (data, 'Имя таблицы','Имя поля','селектор CSS куда будут подставляться значения');
                switch (page) {
                    case 'home': // Заглавная страница
                        console.log('Заполняем формы на заглавной странице : ');
                        $('select[name="ll-search-form__city"]').find('option').remove();
                        FillOptions (window.global_data, 'type_cities','city','.ll-search-form__city');
                        FillOptions (window.global_data, 'reference_profiles','profile','.ll-search-form__commercial-type');
                        break;
                    default: // Временно, потом нужно разнести по страницам для оптимизации
                        console.log('Заполняем формы на других страницах : ');
                        //Очистка полей----------------------------------------------------------------------------------------------
                        $('select[name="ll-search-form__city"]').find('option').remove();
                        // $('select[name="ll-search-form__city"]').append('<option disabled="" selected="" value="">'+'Город'+'</option>');
                        $('select[name="ll-search-form__district"]').find('option').remove();
                        $('select[name="ll-search-form__subdistrict"]').find('option').remove();
                        //----------------------------------------------------------------------------------------------Очистка полей
                        FillOptions (window.global_data, 'type_cities','city','.ll-search-form__city');
						//window.global_data.type_cities.forEach(function (item, i, arr){
						//	$(".ll-search-form__city").append("<option value="+item.city+">"+item.city+"</option>");
						//})
                        FillOptions (window.global_data, 'reference_profiles','profile','.ll-search-form__commercial-type');
                        FillOptions (window.global_data, 'type_cities','city','.ll-sell-form__city-select');
                        FillOptions (window.global_data, 'reference_type_apartments','type_apartment','.ll-search-form__apartment');
                        FillOptions (window.global_data, 'reference_state','state','.ll-search-form__state');
                        FillOptions (window.global_data, 'reference_wall_material_apartments','wall_material','.ll-search-form__walls-apartment');
                        FillOptions (window.global_data, 'reference_wall_material_households','wall_material','.ll-search-form__walls-households');
                        FillOptions (window.global_data, 'reference_entries','entry','.ll-search-form__entry');
                        FillOptions (window.global_data, 'reference_furniture','furniture','.ll-search-form__furniture');
                        break;
                }
// Обработка района - начало -------------------------------------------------------------------------------------------

                var city_id = $(".ll-search-form__city").val(); //Value из списка

                var list_district = [];
                var j = 0;

                for(var i=0;i<window.global_data.type_administrative_district.length; i++)
                {
                    var added = 0;

                    if(window.global_data.type_administrative_district[i].id_city==city_id)
                    {
                        for(var k=0;k<list_district.length; k++)//есть ли уже такое значение
                        {
                            if(window.global_data.type_administrative_district[i].administrative_district==list_district[k].administrative_district)
                            {
                                added = 1;
                            }
                        }

                        if(added==0)
                        {
                            list_district.push(window.global_data.type_administrative_district[i]);
                            j++;
                        }
                    }
                }

                //FillOptions (data, 'type_administrative_district','district','.ll-search-form__district');
                //FillOptions (data, 'type_subdistrict','subdistrict','.ll-search-form__subdistrict');
                if (list_district.length<2) {$('.ll-search-form__district').hide();}
                for (i in list_district)
                {
                    $('.ll-search-form__district').append('<option value="'+list_district[i].administrative_district+'">'+list_district[i].administrative_district+'</option>');
                }
// Обработка района - конец --------------------------------------------------------------------------------------------
            }
        }
    });


}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор кнопки "Аренда"
function RentClick () {
    console.log('Запустилась функция RentClick - Выбор кнопки "Аренда"');
    $("#ll-search-form__radio-rent").prop("checked", true);
    $('.llg-for-rent').show();
}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор кнопки "Продажа"
function SaleClick () {
    console.log('Запустилась функция SaleClick');
    $("#ll-search-form__radio-sale").prop( "checked", true );
    $('.llg-for-rent').hide();
}
// ---------------------------------------------------------------------------------------------------------------------
function SearchButtonClick ()
{
    console.log('Запустилась функция SearchButtonClick');

    var select_type = '&select_type=' + $("#ll-search-form-home__select-type").val();
    var city = '&city=' + $("#ll-search-form-home__select-city").val();
    var price = '&price=' + $("#ll-search-form-home__select-price").val();
    var rooms = '&rooms=';

    var deal_type = 'buy';
    var action = 'search';

    if(($("#ll-search-form-home__radio-rent").prop('checked')) || ($('#ll-search-form-home__radio-for-rent').prop('checked'))) {
        deal_type = 'rent';
    }
    if($("#ll-search-form-home__radio-sale").prop('checked')) {
        deal_type = 'sell';
    }
    if(($('#ll-search-form-home__radio-sale').prop('checked')) || ($('#ll-search-form-home__radio-for-rent').prop('checked'))) {
        action = 'sell';
    }

    if($("#ll-search-form-home__checkbox-1_room").prop('checked')) {
        rooms = rooms + 'one';
    }
    if($("#ll-search-form-home__checkbox-2_rooms").prop('checked')) {
        rooms = rooms + 'two';
    }
    if($("#ll-search-form-home__checkbox-3_rooms").prop('checked')) {
        rooms = rooms + 'three';
    }
    if($("#ll-search-form-home__checkbox-more_rooms").prop('checked')) {
        rooms = rooms + 'more';
    }

    if (rooms=='&rooms=') rooms='';
    if ((price=='&price=null') || (price=='&price=') || (price=='&price=0-') ) price='';
    if ((city=='&city=null') || (city=='&city=')) city='';
    if ((select_type=='&select_type=null') || (select_type=='&select_type=')) select_type='';

    location.href = action+'?deal_type='+deal_type+select_type+city+rooms+price;
}
// ---------------------------------------------------------------------------------------------------------------------
// Обработка района
function SearchFormCityChange()
{
    console.log('Запустилась функция SearchFormCityChange');

    $('select[name="ll-search-form__district"]').find('option').remove();
    $('select[name="ll-search-form__district"]').append('<option disabled="" selected="" value="">'+'Район'+'</option>');
    $('.ll-search-form__district').show();
    $('select[name="ll-search-form__subdistrict"]').find('option').remove();
    $('.ll-search-form__subdistrict').hide();
    //$('select[name="ll-search-form__subdistrict"]').append('<option disabled selected value="">'+'Ориентир'+'</option>');


    var data_district = window.global_data;

    var city_id = $(".ll-search-form__city").val(); //Value из списка

    var list_district = [];
    var j = 0;

    for(var i=0;i<data_district.type_administrative_district.length; i++)
    {
        var added = 0;

        if(data_district.type_administrative_district[i].id_city==city_id)
        {
            for(var k=0;k<list_district.length; k++)//есть ли уже такое значение
            {
                if(data_district.type_administrative_district[i].administrative_district==list_district[k].administrative_district)
                {
                    added = 1;
                }
            }

            if(added==0)
            {
                list_district.push(data_district.type_administrative_district[i]);
                j++;
            }
        }
    }

    //FillOptions (data, 'type_administrative_district','district','.ll-search-form__district');
    //FillOptions (data, 'type_subdistrict','subdistrict','.ll-search-form__subdistrict');
    if (list_district.length<2) {$('.ll-search-form__district').hide();}
    for (i in list_district)
    {
        $('.ll-search-form__district').append('<option value="'+list_district[i].administrative_district+'">'+list_district[i].administrative_district+'</option>');
    }
}
// ---------------------------------------------------------------------------------------------------------------------
// Обработка ориентира
function SearchFormDistrictChange()
{
    console.log('Запустилась функция SearchFormDistrictChange');

    $('select[name="ll-search-form__subdistrict"]').find('option').remove();
    $('select[name="ll-search-form__subdistrict"]').append('<option disabled="" selected="" value="">'+'Ориентир'+'</option>');
    $('.ll-search-form__subdistrict').show();

    var data_subdistrict = window.global_data;

    var district= $(".ll-search-form__district").val(); //Value из списка
    var city_id = $(".ll-search-form__city").val(); //Value из списка

    var list_subdistrict = [];

    for(var i=0;i<data_subdistrict.type_administrative_district.length; i++)
    {
        if((data_subdistrict.type_administrative_district[i].administrative_district==district)&&(data_subdistrict.type_administrative_district[i].id_city==city_id))
        {
            list_subdistrict.push(data_subdistrict.type_administrative_district[i]);
        }
    }

    if (list_subdistrict.length==1) { $('.ll-search-form__subdistrict').hide(); }
    for (i in list_subdistrict)
    {
        $('.ll-search-form__subdistrict').append('<option value="'+list_subdistrict[i].subdistrict+'">'+list_subdistrict[i].subdistrict+'</option>');
    }
}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор типа недвижимости
function SearchFormEstateTypeChange () {
    console.log('Запустилась функция SearchFormEstateTypeChange для обработки типа недвижимости');

    var type = $('#ll-search-form-home__select-type').val();
    switch (type) {
        case 'commercial':
            $('.ll-search-form-home__commercial-type').show();

            $('.ll-search-form-home__rooms').hide();
            $('.ll-search-form-home__rooms-text').hide();
            break;
        case 'land':
            $('.ll-search-form-home__rooms').hide();
            $('.ll-search-form-home__rooms-text').hide();
            $('.ll-search-form-home__commercial-type').hide();
            break;
        default:
            $('.ll-search-form-home__rooms').show();
            $('.ll-search-form-home__rooms-text').show();

            $('.ll-search-form-home__commercial-type').hide();
            break;
    }
}
// ---------------------------------------------------------------------------------------------------------------------
// Подготовка критериев запроса к API
function sendForm(){
    console.log('Запустилась функция sendForm');
    // Запуск поиска
	
	// [option] - имя переменной , [essence] - класс формы, по к-му фильтр
	/*
	var option_first='',option_last='';
	if ($(".essence .ll-search-form__min-value").val() != '') 
		option_first='&option_first='+ $(".essence .ll-search-form__min-value").val();
	if ($(".essence .ll-search-form__max-value").val() != '') 
		option_last='&option_last='+ $(".essence .ll-search-form__max-value").val();
	*/
	
	//универсальные параметры
    // [id_city] - id города из справочника type_cities.
    var id_city = '';
    if ($(".ll-search-form__city").val() != null) {
        id_city = '&id_city='+$(".ll-search-form__city").val();
    }

    //[district] - полное название района города
    var district = '';
    if ($(".ll-search-form__district").val() != null) {
        district = '&district='+ $(".ll-search-form__district").val();
    }

    // [subdistrict] - полное название подрайона города из справочника type_district.
    var subdistrict = '';
    if ($(".ll-search-form__subdistrict").val() != null) {
        subdistrict = '&subdistrict='+ $(".ll-search-form__subdistrict").val();
    }

    var street = '';
    if ($(".ll-search-form__street").val() != null) {
        street = '&street='+$(".ll-search-form__street").val();
    }

	// [price] - имя переменной , [ll-search-form__cost] - класс формы, по к-му фильтр
	var price_first='',price_last='';
	if ($(".ll-search-form__cost .ll-search-form__min-value").val() != '') 
		price_first='&price_first='+ $(".ll-search-form__cost .ll-search-form__min-value").val();
	if ($(".ll-search-form__cost .ll-search-form__max-value").val() != '') 
		price_last='&price_last='+ $(".ll-search-form__cost .ll-search-form__max-value").val();
	
	//[sort] - поле сортировки. Поддерживаемые значения: [sort] - date_create / total_area / price / price_per_meter
    var sort = '&sort='+$(".ll-search-results-sorting select").val().split('|')[0];

    //[order] - тип сортировки. Поддерживаемые значения: [order] - asc / desc
    var order='&order='+ $(".ll-search-results-sorting select").val().split('|')[1];
	
	
	// [realestate_type] - тип недвижимости. Возможные значения: apartment, rent, households, rent, land
    var realestate_type = '', criteria='';

    if (($("#ll-search-form__estate-type_apartment").prop("checked"))&&($("#ll-search-form__radio-sale").prop("checked"))) realestate_type = 'apartment';
    if (($("#ll-search-form__estate-type_apartment").prop("checked"))&&($("#ll-search-form__radio-rent").prop("checked"))) realestate_type = 'rent';
    if ($("#ll-search-form__estate-type_household").prop("checked")) realestate_type = 'households';
    if ($("#ll-search-form__estate-type_land").prop("checked")) realestate_type = 'land';
	//специальные параметры
	
	switch(realestate_type)
	{
		case 'apartment':
			// [id_type_apartment] - id типа квартиры из справочника type_apartment.
			var id_type_apartment = '';
			if ($(".ll-search-form__apartment").val() != null) {
				id_type_apartment = '&id_type_apartment='+$(".ll-search-form__apartment").val();
			}
		
			// [id_type_state] - id типа состояния квартиры из справочника type_realestate_state.
			var id_type_state = '';
			if ($(".ll-search-form__state").val() != null) {
				id_type_state = '&id_type_state='+$(".ll-search-form__state").val();
			}
		
			// [floor] - этаж. Значения могут быть перечислены через ';' или ':'
			var floor_first='',floor_last='';
			if ($(".ll-search-form__floor .ll-search-form__min-value").val() != '') 
				floor_first='&floor_first='+ $(".ll-search-form__floor .ll-search-form__min-value").val();
			if ($(".ll-search-form__floor .ll-search-form__max-value").val() != '') 
				floor_last='&floor_last='+ $(".ll-search-form__floor .ll-search-form__max-value").val();

			var no_first_floor='';
			if($("#ll-search-form__checkbox-not-first").prop("checked")) 
				no_first_floor="&no_first_floor=1";
			var no_last_floor='';
			if($("#ll-search-form__checkbox-not-last").prop("checked")) 
				no_last_floor="&no_last_floor=1";
			
			// [total_area] - общая площадь.	
			var total_area_first='',total_area_last='';
			if ($(".ll-search-form__total-area .ll-search-form__min-value").val() != '') 
				total_area_first='&total_area_first='+ $(".ll-search-form__total-area .ll-search-form__min-value").val();
			if ($(".ll-search-form__total-area .ll-search-form__max-value").val() != '') 
				total_area_last='&total_area_last='+ $(".ll-search-form__total-area .ll-search-form__max-value").val();
		
			var only_new_appartment='';
			if($("#ll-search-form__checkbox-new-appartment").prop("checked")) 
				only_new_appartment="&only_new_apartment=1";
			
			console.log("realestate_type="+realestate_type);
			realestate_type = 'realestate_type='+realestate_type;
			criteria = realestate_type+id_city+district+subdistrict+street+price_first+price_last+floor_first+floor_last+no_first_floor+no_last_floor+total_area_first+total_area_last+id_type_apartment+id_type_state+only_new_appartment+sort+order;
		break;
		case 'rent':
			// [id_type_apartment] - id типа квартиры из справочника type_apartment.
			var id_type_apartment = '';
			if ($(".ll-search-form__apartment").val() != null) {
				id_type_apartment = '&id_type_apartment='+$(".ll-search-form__apartment").val();
			}
		
			// [id_type_state] - id типа состояния квартиры из справочника type_realestate_state.
			var id_type_state = '';
			if ($(".ll-search-form__state").val() != null) {
				id_type_state = '&id_type_state='+$(".ll-search-form__state").val();
			}
			
			// [id_type_furniture] - id мебелировки из  type_realestate_furniture.
			var id_type_furniture = '';
			if ($(".ll-search-form__furniture").val() != null) {
				id_type_furniture = '&id_type_furniture='+$(".ll-search-form__furniture").val();
			}
		
			// [floor] - этаж. Значения могут быть перечислены через ';' или ':'
			var floor_first='',floor_last='';
			if ($(".ll-search-form__floor .ll-search-form__min-value").val() != '') 
				floor_first='&floor_first='+ $(".ll-search-form__floor .ll-search-form__min-value").val();
			if ($(".ll-search-form__floor .ll-search-form__max-value").val() != '') 
				floor_last='&floor_last='+ $(".ll-search-form__floor .ll-search-form__max-value").val();

			var no_first_floor='';
			if($("#ll-search-form__checkbox-not-first").prop("checked")) 
				no_first_floor="&no_first_floor=1";
			var no_last_floor='';
			if($("#ll-search-form__checkbox-not-last").prop("checked")) 
				no_last_floor="&no_last_floor=1";
			
			// [total_area] - общая площадь.	
			var total_area_first='',total_area_last='';
			if ($(".ll-search-form__total-area .ll-search-form__min-value").val() != '') 
				total_area_first='&total_area_first='+ $(".ll-search-form__total-area .ll-search-form__min-value").val();
			if ($(".ll-search-form__total-area .ll-search-form__max-value").val() != '') 
				total_area_last='&total_area_last='+ $(".ll-search-form__total-area .ll-search-form__max-value").val();
		
			var only_new_appartment='';
			if($("#ll-search-form__checkbox-new-appartment").prop("checked")) 
				only_new_appartment="&only_new_appartment=1";
			
			realestate_type = 'realestate_type='+realestate_type;
			criteria = realestate_type+id_city+district+subdistrict+street+price_first+price_last+floor_first+floor_last+no_first_floor+no_last_floor+total_area_first+total_area_last+id_type_apartment+id_type_state+id_type_furniture+only_new_appartment+sort+order;
		break;
		case 'households':
			// [stead] - количество соток земли
			var stead_first='',stead_last='';
			if ($(".ll-search-form__sotki .ll-search-form__min-value").val() != '') 
				stead_first='&stead_first='+ $(".ll-search-form__sotki .ll-search-form__min-value").val();
			if ($(".ll-search-form__sotki .ll-search-form__max-value").val() != '') 
				stead_last='&stead_last='+ $(".ll-search-form__sotki .ll-search-form__max-value").val();

			// [facade] - фасад
			var facade_first='',facade_last='';
			if ($(".ll-search-form__fasad .ll-search-form__min-value").val() != '') 
				facade_first='&facade_first='+ $(".ll-search-form__fasad .ll-search-form__min-value").val();
			if ($(".ll-search-form__fasad .ll-search-form__max-value").val() != '') 
				facade_last='&facade_last='+ $(".ll-search-form__fasad .ll-search-form__max-value").val();

			// [id_wall] - id типа стены
			var id_wall = '';
			if ($(".ll-search-form__walls-households").val() != null) {
				id_wall = '&id_wall='+$(".ll-search-form__walls-households").val();
			}

			// [id_entry] - id типа въезда
			var id_entry = '';
			if ($(".ll-search-form__entry").val() != null) {
				id_entry = '&id_entry='+$(".ll-search-form__entry").val();
			}
			
			var id_yard = $('.llg-for-household input[name=yard]:checked').val();
			if(id_yard) id_yard= '&id_yard='+id_yard;

			realestate_type = '&realestate_type='+realestate_type;
			criteria=realestate_type+id_city+district+subdistrict+street+price_first+price_last+sort+order+stead_first+stead_last+facade_first+facade_last+id_wall+id_entry+id_yard;
		break;
		case 'land':
			// [stead] - количество соток земли
			var stead_first='',stead_last='';
			if ($(".ll-search-form__sotki .ll-search-form__min-value").val() != '') 
				stead_first='&stead_first='+ $(".ll-search-form__sotki .ll-search-form__min-value").val();
			if ($(".ll-search-form__sotki .ll-search-form__max-value").val() != '') 
				stead_last='&stead_last='+ $(".ll-search-form__sotki .ll-search-form__max-value").val();

			// [facade] - фасад
			var facade_first='',facade_last='';
			if ($(".ll-search-form__fasad .ll-search-form__min-value").val() != '') 
				facade_first='&facade_first='+ $(".ll-search-form__fasad .ll-search-form__min-value").val();
			if ($(".ll-search-form__fasad .ll-search-form__max-value").val() != '')
				facade_last= '&facade_last=' + $(".ll-search-form__fasad .ll-search-form__max-value").val();	
			
			var communications='';
			if($("#ll-search-form__checkbox-communications").prop("checked")) 
				communications="&communications=1";
			
			var id_corner ='';
			if($('.llg-for-land input[name=corner]:checked').val())
				id_corner= '&id_corner='+$('.llg-for-land input[name=corner]:checked').val();
			
			realestate_type = '&realestate_type='+realestate_type;
			criteria=realestate_type+id_city+district+subdistrict+street+price_first+price_last+sort+order+stead_first+stead_last+facade_first+facade_last+communications+id_corner;
	}

	AjaxRequest(criteria); // Запрос к API для получения списка

    // Обязательные параметры: [ type, format, realestate_type ]
    // [format] - формат API-запроса. Возможные значения: plain, json
    
    //realestate_type = 'realestate_type='+realestate_type;

    // Обязательные параметры: [type, format, realestate_type]
    //var criteria = 'type=search&format=plain'+realestate_type+id_city+district+subdistrict+street+price_first+price_last+floor_first+floor_last+total_area_first+total_area_last+id_type_apartment+id_type_state+only_new_appartment+sort+order;
}
// ---------------------------------------------------------------------------------------------------------------------

// Цвет звездочки
function star_color()
{
    console.log('Запустилась функция star_color');
    var order=$.cookie('favorites'); //получаем куки
    order ? order=JSON.parse(order): order=[]; //если заказ есть, то куки переделываем в массив с объектами
    var like;

    if(order.length>0)
    {
        $("#ll-favorites-star").removeClass("fa-star-o").addClass("fa-star  yellow-star");
    } else
    {
        $("#ll-favorites-star").removeClass("fa-star yellow-star").addClass("fa-star-o");
    }

    var ListIDs = [];
    $("#data-list").find(".add-to-favorites").each(function(){ ListIDs.push(this.id); });

    for(var i=0; i<ListIDs.length; i++){

        for(var j=0; j<order.length; j++) //перебираем массив в поисках наличия товара в корзине
        {
            if(order[j].item_id==ListIDs[i])
            {
                $("#fa-star-o"+ListIDs[i]).removeClass("fa-star-o").addClass("fa-star   yellow-star");
                $("#grid-fa-star-o"+ListIDs[i]).removeClass("fa-star-o").addClass("fa-star   yellow-star");
            }
        }
    }

    if (window.location.href.search('info') != -1) { //Звездочка на странице INFO

        for(var j=0; j<order.length; j++) //перебираем массив в поисках наличия товара в корзине
        {
            if(order[j].item_id==$(".add-to-favorites-info").attr('id'))
            {
                $("#info-fa-star-o"+$(".add-to-favorites-info").attr('id')).removeClass("fa-star-o").addClass("fa-star   yellow-star");
            }
        }
    }
}
// ---------------------------------------------------------------------------------------------------------------------

//# sourceMappingURL=landlord-functions.js.map
