-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 21, 2016 at 10:05 AM
-- Server version: 5.5.47
-- PHP Version: 5.6.22

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `landlord_new_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `site_pages`
--

CREATE TABLE IF NOT EXISTS `site_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `site_pages`
--

INSERT INTO `site_pages` (`id`, `url`, `title`, `keywords`, `description`) VALUES
(1, '/', 'Главная страница', '', ''),
(3, '/user-page', 'Личный кабинет', '', ''),
(4, '/buy-advices', 'Советы ЛЕНДЛОРД при покупке квартиры', '', ''),
(5, '/steps-to-buy', '8 шагов к покупке недвижимости', '', ''),
(6, '/why-buy-with-landlord', 'Зачем покупать с ЛЕНДЛОРД?', '', ''),
(7, '/mobile-application', 'Мобильное приложение', '', ''),
(8, '/analytics', 'Аналитика', '', ''),
(9, '/sell-advices', 'Советы ЛЕНДЛОРД при продаже квартиры', '', ''),
(10, '/steps-to-sell', '8 шагов к продаже недвижимости', '', ''),
(11, '/why-sell-with-landlord', 'Зачем продавать с ЛЕНДЛОРД?', '', ''),
(12, '/services', 'Услуги', '', ''),
(13, '/services-for-buyer', 'Услуги для покупателя', '', ''),
(14, '/services-for-seller', 'Услуги для продавца', '', ''),
(15, '/services-for-tenant', 'Услуги для арендатора', '', ''),
(16, '/services-for-owner', 'Услуги для арендодателя', '', ''),
(17, '/about', 'О нас', '', ''),
(18, '/news', 'Новости', '', ''),
(19, '/awards', 'Награды', '', ''),
(20, '/history', 'История компании', '', ''),
(21, '/structure', 'Структура', '', ''),
(22, '/leaders', 'Лидеры', '', ''),
(23, '/clients', 'Отзывы клиентов', '', ''),
(24, '/career', 'Карьера', '', ''),
(25, '/education', 'Учебный центр', '', ''),
(26, '/vacancies', 'Вакансии', '', ''),
(27, '/agents', 'Агенты', '', ''),
(28, '/offices', 'Офисы', '', ''),
(30, '/search', 'Поиск недвижимости', '', ''),
(31, '/sell', 'Разместить объявление', '', ''),
(32, '/favorites', 'Избранное', '', ''),
(33, '/services/tariffs', 'Тарифы на услуги', '', ''),
(34, '/services/privatization', 'Приватизация', '', ''),
(35, '/services/alienation', 'Отчуждение недвижимого имущества', '', ''),
(36, '/services/titling', 'Оформление прав собственности', '', ''),
(37, '/services/inheritance', 'Вступление в права наследования', '', ''),
(38, '/services/dacha-amnesty', 'Оформление по дачной амнистии', '', ''),
(39, '/services/transfer-to-non-residential', 'Перевод в нежилой фонд жилых помещений', '', ''),
(40, '/services/transfer-to-residential', 'Перевод в жилой фонд нежилых помещений', '', ''),
(41, '/services/split-of-land', 'Раздел земельных участков', '', ''),
(42, '/services/replanning-legalization', 'Узаконение перепланировок', '', ''),
(43, '/services/moving-between-categories', 'Перевод земельных участков', '', ''),
(44, '/services/connecting-to-communications', 'Подключение объектов к коммуникациям', '', ''),
(45, '/agent-info', 'Информация об агенте', '', ''),
(46, '/vacancy-realtor', 'Вакансия агента по продаже недвижимости', '', ''),
(47, '/vacancy-secretary', 'Вакансии офис-менеджера и секретаря', '', ''),
(48, '/vacancy-realtor-commercial', 'Вакансия агента по коммерческой недвижимости', '', ''),
(49, '/mortgage/for-house', 'Ипотечный кредит на дом или земельный участок', '', ''),
(50, '/mortgage/for-flat', 'Ипотека на квартиру', '', ''),
(51, '/mortgage/for-new-building', 'Ипотека на новостройку', '', ''),
(52, '/mortgage/documents', 'Перечень документов для рассмотрения заявления на получение ипотечного кредита', '', ''),
(53, '/mortgage/banks-partners', 'Банки-партнеры', '', ''),
(54, '/mortgage/additionals-two-documents', 'Ипотека по двум документам без подтверждения дохода предложение для индивидуальных предпринимателей', '', ''),
(55, '/mortgage/additionals-for-young', 'Ипотека для молодых семей', '', ''),
(56, '/mortgage/additionals-for-military', 'Экспресс-ипотека для военных', '', ''),
(57, '/mortgage/additionals-for-business', 'Ипотека для бизнеса', '', ''),
(58, '/steps-to-documents', 'Шаги к оформлению документов', '', ''),
(59, '/steps-to-rent-in', 'Шаги к аренде недвижимости', '', ''),
(60, '/steps-to-rent-out', 'Шаги к сдаче недвижимости в аренду', '', ''),
(61, '/why-rent-in-with-landlord', 'Зачем арендовать с ЛЕНДЛОРД?', '', ''),
(62, '/why-rent-out-with-landlord', 'Зачем сдавать в аренду с ЛЕНДЛОРД?', '', ''),
(63, '/why-documents-with-landlord', 'Зачем оформлять документы с ЛЕНДЛОРД', '', ''),
(64, '/office-info', 'Информация об офисе', '', ''),
(65, '/services-additional', 'Юридические услуги', '', '');
