<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;

class InfoController extends Controller 
{
	public function index($id)
	{
		$rit=[
		'App\\\\Models\\\\Realestate\\\\Realestate_Apartment',
		'App\\\\Models\\\\Realestate\\\\Realestate_Rent',
		'App\\\\Models\\\\Realestate\\\\Realestate_Households',
		'App\\\\Models\\\\Realestate\\\\Realestate_Land'];
		$rin=[
		'realestate_apartments',
		'realestate_rent',
		'realestate_households',
		'realestate_lands'];
		$id=(int)$id;
		$r_type=mb_strtolower(explode('_',DB::select("select realestate_instance_type from realestate where id=?",[$id])[0]->realestate_instance_type,2)[1]);
		switch($r_type){
			case 'apartment':
				$query_string="SELECT '$r_type' as r_type, id_employee_assigned as employee,
				realestate.id, cover_src, price, floor, floor_all, 
				locality, 
				dependent_locality, 
				district, 
				thoroughfare,
				latitude,
				longitude,
				reference_type_apartments.type_apartment, reference_type_apartments.room_count,
				realestate.created_at,
				
				wall_material,
				fund,
				area_general,area_living,area_kitchen,
				is_under_construction,
				year_build, date_putting,
				has_concierge,
				is_backup_power,
				floor_apartments_count,
				is_free_planing,
				has_around_territory,
				is_guarded,
				is_video_monitoring,
				is_control_access
				
				FROM realestate
				INNER JOIN $rin[0] ON realestate.realestate_instance_id=$rin[0].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=$rin[0].id_reference_type_apartment
				
				INNER JOIN reference_wall_material_apartments ON $rin[0].id_reference_wall_material=reference_wall_material_apartments.id
				INNER JOIN reference_funds ON $rin[0].id_reference_fund=reference_funds.id
				WHERE realestate_instance_type='$rit[0]' AND realestate.id=?";
				
				$estate=DB::select($query_string,[$id]);
				$employee=DB::select("SELECT phone_mob, src from employes WHERE employes.id=?",[$estate[0]->employee]);
				return view("layouts.info", ['estate'=>$estate[0], 'employee'=>$employee[0], 'similars'=>InfoController::like($estate[0]->id)]);
				break;
			
			case 'rent':
				$query_string="select '$r_type'as r_type, realestate.id, cover_src, price, floor, floor_all, id_employee_assigned as employee,
				locality, 
				dependent_locality, 
				district, 
				thoroughfare, 
				latitude,
				longitude,
				reference_type_apartments.type_apartment, reference_type_apartments.room_count,
				realestate.created_at,
				
				wall_material,
				fund,
				area_general,area_living,area_kitchen,
				is_under_construction,
				year_build, date_putting,
				has_concierge,
				is_backup_power,
				floor_apartments_count,
				is_free_planing,
				has_around_territory,
				is_guarded,
				is_video_monitoring,
				is_control_access
				
				from realestate 
				INNER JOIN $rin[1] ON realestate.realestate_instance_id=$rin[1].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=$rin[1].id_reference_type_apartment
				
				INNER JOIN reference_wall_material_apartments ON $rin[1].id_reference_wall_material=reference_wall_material_apartments.id
				INNER JOIN reference_funds ON $rin[1].id_reference_fund=reference_funds.id
				WHERE realestate_instance_type='$rit[1]' AND realestate.id=?";
				
				$estate=DB::select($query_string,[$id]);
				$employee=DB::select("SELECT phone_mob, src from employes WHERE employes.id=?",[$estate[0]->employee]);
				return view("layouts.info", ['estate'=>$estate[0], 'employee'=>$employee[0], 'similars'=>InfoController::like($estate[0]->id)]);
				break;
			case 'households':
				$query_string="select '$r_type' as r_type, realestate.id, cover_src, price, id_employee_assigned as employee,
				locality, 
				dependent_locality, 
				district, 
				thoroughfare, 
				latitude,
				longitude,
				realestate.created_at, 
				
				wall_material,
				stead,
				yard,
				area_general,area_living,area_kitchen,
				is_under_construction,
				year_build,  date_putting,
				comfort,
				has_garage,
				bathroom,
				pavement,
				state,
				heating,
				placement,
				room_count,
				hot_water,
				ceilings,
				has_around_territory,
				output_to_water,
				location,
				GROUP_CONCAT(engineer_system SEPARATOR ', ') as engineer_systems,
				$rin[2].id as h_id
				
				from realestate 
				INNER JOIN $rin[2] ON realestate.realestate_instance_id=$rin[2].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				
				LEFT JOIN reference_wall_material_households ON $rin[2].id_reference_wall_material=reference_wall_material_households.id
				LEFT JOIN reference_yard_households ON $rin[2].id_reference_yard=reference_yard_households.id
				LEFT JOIN reference_comforts ON $rin[2].id_reference_comfort=reference_comforts.id
				LEFT JOIN reference_bathroom ON $rin[2].id_reference_bathroom=reference_bathroom.id
				LEFT JOIN reference_pavement_land ON $rin[2].id_reference_pavement_land=reference_pavement_land.id
				LEFT JOIN reference_state_households ON $rin[2].id_reference_state=reference_state_households.id
				LEFT JOIN reference_heating ON $rin[2].id_reference_heating=reference_heating.id
				LEFT JOIN reference_placement_lands ON $rin[2].id_reference_placement_land=reference_placement_lands.id
				LEFT JOIN reference_hot_water ON $rin[2].id_reference_hot_water=reference_hot_water.id
				LEFT JOIN reference_outputs_to_water ON $rin[2].id_reference_output_to_water=reference_outputs_to_water.id
				LEFT JOIN reference_household_location ON $rin[2].id_reference_household_location=reference_household_location.id
				LEFT JOIN pivot_realestate_households_reference_engineer_systems ON $rin[2].id=pivot_realestate_households_reference_engineer_systems.id_realestate_household
				LEFT JOIN reference_engineer_systems ON pivot_realestate_households_reference_engineer_systems.id_reference_engineer_system = reference_engineer_systems.id
				
				WHERE realestate_instance_type='$rit[2]' AND realestate.id=?";
				
				$estate=DB::select($query_string,[$id]);
				$com=DB::select("SELECT CONCAT( communication,': ', GROUP_CONCAT( subcommunication
SEPARATOR ', ' ) ) as `com`
FROM pivot_realestate_households_reference_communications
INNER JOIN pivot_reference_communications_reference_subcommunications ON pivot_realestate_households_reference_communications.id_pivot_communication = pivot_reference_communications_reference_subcommunications.id
INNER JOIN reference_communications ON pivot_reference_communications_reference_subcommunications.id_reference_communication = reference_communications.id
INNER JOIN reference_subcommunications ON pivot_reference_communications_reference_subcommunications.id_reference_subcommunication = reference_subcommunications.id
WHERE `id_realestate_household` =?
GROUP BY communication", [$estate[0]->h_id]);
				$com_mut=DB::select("SELECT `communication` AS `com_mut`
FROM pivot_realestate_households_reference_communications_mutual
INNER JOIN reference_communications ON `id_reference_communication` = reference_communications.id
WHERE `id_realestate_household` =?", [$estate[0]->h_id]);
				
				$employee=DB::select("SELECT phone_mob, src from employes WHERE employes.id=?",[$estate[0]->employee]);
				return view("layouts.info", ['estate'=>$estate[0], 'employee'=>$employee[0], 'com'=>$com, 'com_mut'=>$com_mut, 'similars'=>InfoController::like($estate[0]->id)]);
				break;
			case 'land':
				
				$query_string="select '$r_type' as r_type, realestate.id, cover_src, price, id_employee_assigned as employee,
				locality, 
				dependent_locality, 
				district, 
				thoroughfare,
				latitude,
				longitude,
				realestate.created_at,
				
				type_land,
				stead,
				stead_x, stead_y,
				pavement,
				location,
				land_relief,
				facade,
				output_to_water,
				has_around_territory,
				land_location,
				$rin[3].id as l_id
				
				from realestate 
				INNER JOIN $rin[3] ON realestate.realestate_instance_id=$rin[3].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				LEFT JOIN pivot_realestate_lands_reference_communications ON $rin[3].id=pivot_realestate_lands_reference_communications.id_realestate_land
				LEFT JOIN reference_pavement_land ON $rin[3].id_reference_pavement_land=reference_pavement_land.id
				LEFT JOIN reference_locations ON $rin[3].id_reference_location=reference_locations.id
				LEFT JOIN reference_land_reliefs ON $rin[3].id_reference_land_relief=reference_land_reliefs.id
				LEFT JOIN reference_outputs_to_water ON $rin[3].id_reference_output_to_water=reference_outputs_to_water.id
				LEFT JOIN reference_land_locations ON $rin[3].id_reference_land_location=reference_land_locations.id
				
				LEFT JOIN reference_type_lands ON $rin[3].id_reference_type_land=reference_type_lands.id
				WHERE realestate_instance_type='$rit[3]' AND realestate.id=?";
				
				$estate=DB::select($query_string,[$id]);
				$com=DB::select("SELECT CONCAT( communication,': ', GROUP_CONCAT( subcommunication
SEPARATOR ', ' ) ) as `com`
FROM pivot_realestate_lands_reference_communications
INNER JOIN pivot_reference_communications_reference_subcommunications ON pivot_realestate_lands_reference_communications.id_pivot_communication = pivot_reference_communications_reference_subcommunications.id
INNER JOIN reference_communications ON pivot_reference_communications_reference_subcommunications.id_reference_communication = reference_communications.id
INNER JOIN reference_subcommunications ON pivot_reference_communications_reference_subcommunications.id_reference_subcommunication = reference_subcommunications.id
WHERE `id_realestate_land` =?
GROUP BY communication", [$estate[0]->l_id]);
				$com_mut=DB::select("SELECT `communication` AS `com_mut`
FROM pivot_realestate_lands_reference_communications_mutual
INNER JOIN reference_communications ON `id_reference_communication` = reference_communications.id
WHERE `id_realestate_land` =?", [$estate[0]->l_id]);
				$employee=DB::select("SELECT phone_mob, src from employes WHERE employes.id=?",[$estate[0]->employee]);
				return view("layouts.info", ['estate'=>$estate[0], 'com'=>$com, 'com_mut'=>$com_mut, 'employee'=>$employee[0], 'similars'=>InfoController::like($estate[0]->id)]);
				break;
		}
	}
	public function like($id)
	{
		$rit=[
		'App\\\\Models\\\\Realestate\\\\Realestate_Apartment',
		'App\\\\Models\\\\Realestate\\\\Realestate_Rent',
		'App\\\\Models\\\\Realestate\\\\Realestate_Households',
		'App\\\\Models\\\\Realestate\\\\Realestate_Land'];
		define('price_weight', 1);
		define('area_general_weight', 1);
		define('locality_weight', 1);
		define('type_apartment_weight',5);
		define('floor_weight', 0.5);
		define('floor_all_weight', 0.5);
		
		function compare_numeric($standard, $compared, $weight){
			if(!$standard) return 0;
			$num=1-abs(($compared-$standard)/$standard);
			if($num<=0) return 0;
			return $weight*$num;
		}
		function compare_bool($standard, $compared, $weight){
			return $standard==$compared?$weight:0;
		}
		
		function sort_similar($standard, $similars_raw){
			$similars=[];
			foreach($similars_raw as $s_raw){
				$weight=0;
				if(isset($standard->price)) $weight+=compare_numeric($standard->price, $s_raw->price, price_weight);
				if(isset($standard->area_general)) $weight+=compare_numeric($standard->area_general, $s_raw->area_general, area_general_weight);
				
				$similars[]=[$s_raw, $weight];
			}
			usort($similars, function($a,$b){
				if ($a[1] == $b[1]) {
					return 0;
				}
				return ($a[1] < $b[1]) ? 1 : -1;
			});
			return $similars;
		}
		//
		$id=(int)$id;
		$r_type=mb_strtolower(explode('_',DB::select("select realestate_instance_type from realestate where id=?",[$id])[0]->realestate_instance_type,2)[1]);
		switch($r_type)
		{
			case "apartment":
				$standard=DB::select("select price, area_general, dependent_locality, locality, id_reference_type_apartment, floor, floor_all 
				from realestate 
				INNER JOIN realestate_apartments ON realestate.realestate_instance_id=realestate_apartments.id
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=realestate_apartments.id_reference_type_apartment
				WHERE realestate.id=?",[$id])[0];
				$similars_raw=DB::select("select realestate.id, price, area_general, dependent_locality, locality, id_reference_type_apartment, floor, floor_all,
				type_apartment, cover_src, '$r_type' as 'r_type'
				from realestate 
				INNER JOIN realestate_apartments ON realestate.realestate_instance_id=realestate_apartments.id
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=realestate_apartments.id_reference_type_apartment
				WHERE realestate_instance_type='$rit[3]' AND locality=? AND realestate.id!=? 
				", [$standard->locality, $id]);
				echo "<br><br>\n\n";
				$similars=sort_similar($standard, $similars_raw);
				/*foreach($similars as $sim)
				{
					echo "price: ".$sim[0]->price." area_general: ".$sim[0]->area_general."<br>\n";
					echo "weight: ".$sim[1]."<br>\n";
					echo "<br>\n";
				}*/
				return array_slice($similars,0,4);
			break;
			case 'rent':
				$standard=DB::select("select price, area_general, dependent_locality, locality, id_reference_type_apartment, floor, floor_all 
				from realestate 
				INNER JOIN realestate_rent ON realestate.realestate_instance_id=realestate_rent.id
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=realestate_rent.id_reference_type_apartment
				WHERE realestate.id=?",[$id])[0];
				$similars_raw=DB::select("select realestate.id, price, area_general, dependent_locality, locality, id_reference_type_apartment, floor, floor_all, 
				type_apartment, cover_src, '$r_type' as 'r_type'
				from realestate 
				INNER JOIN realestate_rent ON realestate.realestate_instance_id=realestate_rent.id
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=realestate_rent.id_reference_type_apartment
				WHERE realestate_instance_type='$rit[3]' AND locality=? AND realestate.id!=? 
				", [$standard->locality, $id]);
				echo "<br><br>\n\n";
				$similars=sort_similar($standard, $similars_raw);
				return array_slice($similars,0,4);
			break;
			case 'households':
				$standard=DB::select("select realestate.id, price, area_general, dependent_locality, locality,
				cover_src
				from realestate 
				INNER JOIN realestate_households ON realestate.realestate_instance_id=realestate_households.id
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate.id=?",[$id])[0];
				$similars_raw=DB::select("select realestate.id, price, area_general, dependent_locality, locality, 
				'$r_type' as 'r_type', cover_src
				from realestate 
				INNER JOIN realestate_households ON realestate.realestate_instance_id=realestate_households.id
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate_instance_type='$rit[3]' AND locality=? AND realestate.id!=? 
				", [$standard->locality, $id]);
				echo "<br><br>\n\n";
				$similars=sort_similar($standard, $similars_raw);
				return array_slice($similars,0,4);
			break;
			case 'land':
				$standard=DB::select("select price, stead, dependent_locality, locality,
				cover_src
				from realestate 
				INNER JOIN realestate_lands ON realestate.realestate_instance_id=realestate_lands.id
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate.id=?",[$id])[0];
				$similars_raw=DB::select("select realestate.id, '$r_type' as 'r_type', price, stead, dependent_locality, locality, 
				cover_src
				from realestate 
				INNER JOIN realestate_lands ON realestate.realestate_instance_id=realestate_lands.id
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate_instance_type='$rit[3]' AND locality=? AND realestate.id!=? 
				", [$standard->locality, $id]);
				echo "<br><br>\n\n";
				$similars=sort_similar($standard, $similars_raw);
				return array_slice($similars,0,4);
		}
	}
}
