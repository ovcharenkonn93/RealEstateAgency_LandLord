<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Illuminate\Http\Request;

class OfficesController extends Controller 
{
	public function index(Request $request)
	{
			$page = \DB::table('site_pages')->where('url', '/offices')->get();		
			if (!$page) {					
				return \View::make('errors.404');					
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}				
			
		$cities=DB::select("SELECT id, city FROM type_cities");	
		$query_string="SELECT offices.id as id, address, telephone, skype, email, work_time, city FROM offices
		INNER JOIN type_cities ON type_cities.id=offices.id_type_city";
		$offices=DB::select($query_string);
		return view("layouts.offices", ['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords,'cities'=>$cities, 'offices'=>[]]);
	}
	public function get(Request $request)
	{
		$query_string="SELECT offices.id as id, address, telephone, skype, email,  work_time, city FROM offices
		INNER JOIN type_cities ON type_cities.id=offices.id_type_city";
		$param=[];
		$id_city=$request->input('id_city');
		if($id_city){
			$query_string.=" WHERE id_type_city=?";
			$param[]=(int)$id_city;
		}
		$offices=DB::select($query_string, $param);
		return view("layouts.blocks.ll-offices-list", ['offices'=>$offices]);
	}
	public function getmap(Request $request)
	{
		$id_city=(int)$request->input("id_city");
		$coord=DB::select("SELECT id, latitude, longitude FROM offices WHERE id_type_city=?", [$id_city]);
		return json_encode($coord);
	}
	public function select(Request $request)
	{
		$id=(int)$request->input("id");
		$offices=DB::select("SELECT address, telephone, 'skype_login' as skype, email,  'Пн-Пт: 09:00-19:00' as work_time, id_type_city, city FROM offices
		INNER JOIN type_cities ON type_cities.id=offices.id_type_city WHERE offices.id=?", [$id]);
		return json_encode($offices[0]);
	}
}
