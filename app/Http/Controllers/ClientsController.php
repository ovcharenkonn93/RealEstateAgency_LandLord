<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class ClientsController extends Controller 
{
	public function index()
	{
		$reviews=DB::select("select id, name, avatar, date, text FROM reviews ORDER BY date DESC");
		return view('layouts.clients', ['reviews'=>$reviews]);
	}
}
