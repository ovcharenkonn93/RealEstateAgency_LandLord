<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class Site_pages extends Controller {
	function __construct()
	{
		define("additional_id", 64);
	}
	public function index($page_name) //Страныцы без разделов
		{		
			$page = \DB::table('site_pages')->where('url', '/'.$page_name)->get();		
			if (!$page) {					
				return \View::make('errors.404');					
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}				
			return \View::make('layouts.'.$page_name,['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords]);		
		}

	public function services() //раздел Услуги
		{		
			$page = \DB::table('site_pages')->where('url', '/services')->get();		
			if (!$page) {
				return \View::make('errors.404');			
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}	
			$site_services = \DB::table('site_services')->get();
			$additional_services= \DB::table('site_secondary_services')->where('parent_id', additional_id)->get();
			return \View::make('layouts.services',['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'site_services'=>$site_services, 'additional_services'=>$additional_services]);		
		}
	
	public function services_for_buyer() // Услуги для покпателя
		{		
			$page = \DB::table('site_pages')->where('url', '/services-for-buyer')->get();		
			if (!$page) {
				return \View::make('errors.404');			
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}		
			$site_services = \DB::table('site_services')->get();
			$additional_services= \DB::table('site_secondary_services')->where('parent_id', additional_id)->get();
			return \View::make('layouts.services-for-buyer',['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'site_services'=>$site_services, 'additional_services'=>$additional_services]);		
		}
	
	public function services_for_seller() // Услуги для продавца
		{		
			$page = \DB::table('site_pages')->where('url', '/services-for-seller')->get();		
			if (!$page) {
				return \View::make('errors.404');			
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}		
			$site_services = \DB::table('site_services')->get();
			$additional_services= \DB::table('site_secondary_services')->where('parent_id', additional_id)->get();
			return \View::make('layouts.services-for-seller',['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'site_services'=>$site_services, 'additional_services'=>$additional_services]);		
		}
	
	public function services_for_tenant() //Услуги арендатору
		{		
			$page = \DB::table('site_pages')->where('url', '/services-for-tenant')->get();		
			if (!$page) {
				return \View::make('errors.404');			
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}		
			$site_services = \DB::table('site_services')->get();
			$additional_services= \DB::table('site_secondary_services')->where('parent_id', additional_id)->get();
			return \View::make('layouts.services-for-tenant',['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'site_services'=>$site_services, 'additional_services'=>$additional_services]);		
		}
		
	public function services_for_owner() //Услуги арендодателю
		{		
			$page = \DB::table('site_pages')->where('url', '/services-for-owner')->get();		
			if (!$page) {
				return \View::make('errors.404');			
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}		
			$site_services = \DB::table('site_services')->get();
			$additional_services= \DB::table('site_secondary_services')->where('parent_id', additional_id)->get();
			return \View::make('layouts.services-for-owner',['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'site_services'=>$site_services, 'additional_services'=>$additional_services]);		
		}
	
	public function services_additional() //Услуги
		{		
			$page = \DB::table('site_pages')->where('url', '/services-additional')->get();		
			if (!$page) {
				return \View::make('errors.404');			
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}		
			$site_services = \DB::table('site_services')->get();
			$additional_services= \DB::table('site_secondary_services')->where('parent_id', additional_id)->get();
			$site_secondary_services=[];
			foreach ($site_services as $s) {
				$site_secondary_services[$s->id]= \DB::table('site_secondary_services')->where('parent_id', $s->id)->get();
			}
			return \View::make('layouts.services-additional',['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'site_services'=>$site_services, 'additional_services'=>$additional_services, 'site_secondary_services'=>$site_secondary_services]);		
		}	


	public function index_services($page_name) //Страныцы раздела Услуги
		{			
			$page = \DB::table('site_pages')->where('url', '/services/'.$page_name)->get();			
			if (!$page) {
				return \View::make('errors.404');			
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}			
			$page_name = str_replace("/", ".", $page_name);			
			return \View::make('layouts.services.'.$page_name,['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords]);			
		}	
	
	public function index_mortgage($page_name) //Страныцы раздела Ипотека
		{			
			$page = \DB::table('site_pages')->where('url', '/mortgage/'.$page_name)->get();			
			if (!$page) {
				return \View::make('errors.404');			
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->description;
				  $meta_keywords=$p->keywords;		  
			}			
			$page_name = str_replace("/", ".", $page_name);			
			return \View::make('layouts.mortgage.'.$page_name,['title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords]);			
		}	

}
