<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Illuminate\Http\Request;

class SearchController extends Controller {
	public function index()
	{
		return;
	}
	public function get_estate_from_id(Request $request)
	{
		$rit=[
		'App\\\\Models\\\\Realestate\\\\Realestate_Apartment',
		'App\\\\Models\\\\Realestate\\\\Realestate_Rent',
		'App\\\\Models\\\\Realestate\\\\Realestate_Households',
		'App\\\\Models\\\\Realestate\\\\Realestate_Land'];
		$rin=[
		'realestate_apartments',
		'realestate_rent',
		'realestate_households',
		'realestate_lands'];
		$id=(int)$request->input("id");
		$r_type=mb_strtolower(explode('_',DB::select("select realestate_instance_type from realestate where id=?",[$id])[0]->realestate_instance_type,2)[1]);
		switch($r_type){
			case 'apartment':
				$query_string="SELECT realestate.id, cover_src, price, floor, floor_all, area_general, 
				locality as city, 
				dependent_locality as administrative_district, 
				district, 
				thoroughfare as street,
				reference_type_apartments.type_apartment, reference_type_apartments.room_count
				FROM realestate
				INNER JOIN $rin[0] ON realestate.realestate_instance_id=$rin[0].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=$rin[0].id_reference_type_apartment
				WHERE realestate_instance_type='$rit[0]' AND realestate.id=$id";
				
				$estate=DB::select($query_string);
				for($i=0;$i<count($estate);$i++){
					echo view("layouts.blocks.ll-search-sb-result-list",['id'=>$i,'estate'=>$estate[$i]]);
				}
				return;
				break;
			
			case 'rent':
				$query_string="select realestate.id, cover_src, price, floor, floor_all, area_general, 
				locality as city, 
				dependent_locality as administrative_district, 
				district, 
				thoroughfare as street, 
				reference_type_apartments.type_apartment, reference_type_apartments.room_count
				from realestate 
				INNER JOIN $rin[1] ON realestate.realestate_instance_id=$rin[1].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=$rin[1].id_reference_type_apartment
				WHERE realestate_instance_type='$rit[1]' AND realestate.id=$id";
				
				$estate=DB::select($query_string);
				for($i=0;$i<count($estate);$i++){
					echo view("layouts.blocks.ll-search-sb-result-list",['id'=>$i,'estate'=>$estate[$i]]);
				}
				return;
				break;
			case 'households':
				$query_string="select realestate.id, cover_src, price, area_general, 
				locality as city, 
				dependent_locality as administrative_district, 
				district, 
				thoroughfare as street, 
				room_count, floors
				from realestate 
				INNER JOIN $rin[2] ON realestate.realestate_instance_id=$rin[2].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate_instance_type='$rit[2]' AND realestate.id=$id";
				
				$estate=DB::select($query_string);
				for($i=0;$i<count($estate);$i++){
					echo view("layouts.blocks.ll-search-sb-result-households-list",['id'=>$i,'estate'=>$estate[$i]]);
				}
				return;
				break;
			case 'land':
				
				$query_string="select realestate.id, cover_src, price, stead, 
				locality as city, 
				dependent_locality as administrative_district, 
				district, 
				thoroughfare as street
				from realestate 
				INNER JOIN $rin[3] ON realestate.realestate_instance_id=$rin[3].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				LEFT JOIN pivot_realestate_lands_reference_communications ON $rin[3].id=pivot_realestate_lands_reference_communications.id_realestate_land
				WHERE realestate_instance_type='$rit[3]' WHERE realestate.id=$id";
				
				$estate=DB::select($query_string);
				for($i=0;$i<count($estate);$i++){
					//var_dump($estate[$i]);
					//echo "<br><br>\n\n";
					echo view("layouts.blocks.ll-search-sb-result-lands-list",['id'=>$i,'estate'=>$estate[$i]]);
				}
				return;
				break;
		}
	}
	public function get_coordinates(Request $request)
	{
		$rit=[
		'App\\\\Models\\\\Realestate\\\\Realestate_Apartment',
		'App\\\\Models\\\\Realestate\\\\Realestate_Rent',
		'App\\\\Models\\\\Realestate\\\\Realestate_Households',
		'App\\\\Models\\\\Realestate\\\\Realestate_Land'];
		$r_type=$request->input("realestate_type");
		$rin=[
		'realestate_apartments',
		'realestate_rent',
		'realestate_households',
		'realestate_lands'];
		switch($r_type){
			case 'apartment':
				$query_string="select realestate.id, addresses.longitude+RAND()*0.01-0.005 as longitude, addresses.latitude+RAND()*0.01-0.005 as latitude
				from realestate 
				INNER JOIN $rin[0] ON realestate.realestate_instance_id=$rin[0].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate_instance_type='$rit[0]' ";
				$param=[];
				
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND locality=(SELECT city from type_cities WHERE id=?)";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND dependent_locality=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND thoroughfare=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND price>=?";
					$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND price<=?";
					$param[]=(int)$price_last;
				}
				
				$floor_first=$request->input('floor_first');
				if($floor_first) {
					$query_string.=" AND floor>=?";
					$param[]=$floor_first;
				}
				$floor_last=$request->input('floor_last');
				if($floor_last) {
					$query_string.=" AND floor<=?";
					$param[]=(int)$floor_last;
				}
				
				$total_area_first=$request->input('total_area_first');
				if($total_area_first){
					$query_string.=" AND area_general>=?";
					$param[]=(int)$total_area_first;
				}
				
				$total_area_last=$request->input('total_area_last');
				if($total_area_last){
					$query_string.=" AND area_general<=?";
					$param[]=$total_area_last;
				}
				
				$id_type_apartment=$request->input('id_type_apartment');
				if($id_type_apartment){
					$query_string.=" AND id_reference_type_apartment=?";
					$param[]=$id_type_apartment;
				}
				
				$id_state=$request->input('id_type_state');
				if($id_state){
					$query_string.=" AND id_reference_state=?";
					$param[]=$id_state;
				}
				
				$only_new_apartment=$request->input('only_new_apartment');
				if($only_new_apartment) {
					$query_string.=" AND id_reference_fund=1";
				}
				
				$estate=DB::select($query_string, $param);
				echo json_encode($estate);
				return;
				break;
			
			case 'rent':
				$query_string="select realestate.id, addresses.longitude+RAND()*0.01-0.005 as longitude, addresses.latitude+RAND()*0.01-0.005 as latitude
				from realestate 
				INNER JOIN $rin[1] ON realestate.realestate_instance_id=$rin[1].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate_instance_type='$rit[1]' ";
				$param=[];
				
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND locality=(SELECT city from type_cities WHERE id=?)";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND dependent_locality=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND thoroughfare=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND `price`>?";
					$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND `price`<=?";
					$param[]=(int)$price_last;
				}
				
				$floor_first=$request->input('floor_first');
				if($floor_first) {
					$query_string.=" AND floor>=?";
					$param[]=(int)$floor_first;
				}
				$floor_last=$request->input('floor_last');
				if($floor_last) {
					$query_string.=" AND floor<=?";
					$param[]=(int)$floor_last;
				}
				
				$total_area_first=$request->input('total_area_first');
				if($total_area_first){
					$query_string.=" AND area_general>=?";
					$param[]=$total_area_first;
				}
				$total_area_last=$request->input('total_area_last');
				if($total_area_last){
					$query_string.=" AND area_general<=?";
					$param[]=$total_area_last;
				}
				
				$id_type_apartment=$request->input('id_type_apartment');
				if($id_type_apartment){
					$query_string.=" AND id_reference_type_apartment=?";
					$param[]=$id_type_apartment;
				}
				
				$id_state=$request->input('id_type_state');
				if($id_state){
					$query_string.=" AND id_reference_state=?";
					$param[]=$id_state;
				}
				
				$id_furniture=$request->input('id_type_furniture');
				if($id_furniture){
					$query_string.=" AND id_reference_furniture=?";
					$param[]=$id_furniture;
				}
				
				$only_new_apartment=$request->input('only_new_apartment');
				if($only_new_apartment) {
					$query_string.=" AND id_reference_fund=1";
				}
				
				$estate=DB::select($query_string, $param);
				echo json_encode($estate);
				return;
				break;
			case 'households':
				$query_string="select realestate.id,  addresses.longitude+RAND()*0.01-0.005 as longitude, addresses.latitude+RAND()*0.01-0.005 as latitude
				from realestate 
				INNER JOIN $rin[2] ON realestate.realestate_instance_id=$rin[2].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate_instance_type='$rit[2]' ";
				$param=[];
				
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND locality=(SELECT city from type_cities WHERE id=?)";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND dependent_locality=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND thoroughfare=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND price>=?";
					$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND price<=?";
					$param[]=(int)$price_last;
				}
				
				$stead_first=$request->input('stead_first');
				if($stead_first) {
					$query_string.=" AND `stead`>=?";
					$param[]=(int)$stead_first;
				}
				$stead_last=$request->input('stead_last');
				if($stead_last) {
					$query_string.=" AND `stead`<=?";
					$param[]=(int)$stead_last;
				}
				
				$facade_first=$request->input('facade_first');
				if($facade_first) {
					$query_string.=" AND `facade`>=?";
					$param[]=(int)$facade_first;
				}
				$facade_last=$request->input('facade_last');
				if($facade_last) {
					$query_string.=" AND `facade`<=?";
					$param[]=(int)$facade_last;
				}
				
				$id_wall=$request->input('id_wall');
				if($id_wall) {
					$query_string.=" AND `id_reference_wall_material`=?";
					$param[]=(int)$id_wall;
				}
			
				$id_entry=$request->input('id_entry');
				if($id_entry) {
					$query_string.=" AND `id_reference_entry`=?";
					$param[]=(int)$id_entry;
					//если въезд не установлен и id_entry=3, т.е. нет въезда
					//тогда фильтрация пройдена, если id_entry!=3, то не пройдена
				}
			
				$id_yard=$request->input('id_yard');
				if($id_yard) {
					$query_string.=" AND `id_reference_yard`=?";
					$param[]=(int)$id_yard;
				}
				
				$estate=DB::select($query_string, $param);
				echo json_encode($estate);
				return;
				break;
			case 'land':
				$com_join="";
				$communications=$request->input('communications');
				if($communications) {
					$com_join="INNER";
				}
				else $com_join="LEFT";
				
				$query_string="select realestate.id, addresses.longitude+RAND()*0.01-0.005 as longitude, addresses.latitude+RAND()*0.01-0.005 as latitude
				from realestate 
				INNER JOIN $rin[3] ON realestate.realestate_instance_id=$rin[3].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				$com_join JOIN pivot_realestate_lands_reference_communications ON $rin[3].id=pivot_realestate_lands_reference_communications.id_realestate_land
				WHERE realestate_instance_type='$rit[3]' ";
				$param=[];
				
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND locality=(SELECT city from type_cities WHERE id=?)";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND dependent_locality=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND thoroughfare=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND price>=?";
					$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND price<=?";
					$param[]=(int)$price_last;
				}
				
				$stead_first=$request->input('stead_first');
				if($stead_first) {
					$query_string.=" AND `stead`>=?";
					$param[]=(int)$stead_first;
				}
				$stead_last=$request->input('stead_last');
				if($stead_last) {
					$query_string.=" AND `stead`<=?";
					$param[]=(int)$stead_last;
				}
				
				$facade_first=$request->input('facade_first');
				if($facade_first) {
					$query_string.=" AND `facade`>=?";
					$param[]=(int)$facade_first;
				}
				$facade_last=$request->input('facade_last');
				if($facade_last) {
					$query_string.=" AND `facade`<=?";
					$param[]=(int)$facade_last;
				}
				
				$estate=DB::select($query_string, $param);
				echo json_encode($estate);
				return;
				break;
		}
	}
	public function get(Request $request)
	{
		/*echo $_SERVER['DOCUMENT_ROOT'];
		return;*/
		$rit=[
		'App\\\\Models\\\\Realestate\\\\Realestate_Apartment',
		'App\\\\Models\\\\Realestate\\\\Realestate_Rent',
		'App\\\\Models\\\\Realestate\\\\Realestate_Households',
		'App\\\\Models\\\\Realestate\\\\Realestate_Land'];
		$r_type=$request->input("realestate_type");
		$rin=[
		'realestate_apartments',
		'realestate_rent',
		'realestate_households',
		'realestate_lands'];
		if(isset($_COOKIE[md5(json_encode($request))]))
			$percent_sell=(int)$_COOKIE[md5(json_encode($request))];
		else
			$percent_sell=25+rand(-3,7);
			echo "<script>console.log(\"\$.cookie с \$percent_sell\");
			$.cookie(\"".md5(json_encode($request))."\", \"".$percent_sell."\", {expires: 1});</script>";
		switch($r_type){
			case 'apartment':
				/*$query_string="select realestate.id, cover_src, price, floor, floor_all, 
				area_general, type_cities.city as city, 
				type_administrative_district.administrative_district, type_district.district,
				address.street, reference_type_apartments.type_apartment, 
				reference_type_apartments.room_count
				from realestate 
				INNER JOIN $rin[0] ON realestate.realestate_instance_id=$rin[0].id 
				INNER JOIN address ON realestate.id_address=address.id
				INNER JOIN type_district ON address.id_district=type_district.id
				INNER JOIN type_cities ON type_district.id_city=type_cities.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=$rin[0].id_reference_type_apartment
				LEFT JOIN type_administrative_district ON address.id_administrative_district=type_administrative_district.id
				WHERE realestate_instance_type='$rit[0]' ";*/
				$query_string="SELECT realestate.id, cover_src, price, floor, floor_all, area_general, 
				locality as city, 
				dependent_locality as administrative_district, 
				district, 
				thoroughfare as street,
				reference_type_apartments.type_apartment, reference_type_apartments.room_count
				FROM realestate
				INNER JOIN $rin[0] ON realestate.realestate_instance_id=$rin[0].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=$rin[0].id_reference_type_apartment
				WHERE realestate_instance_type='$rit[0]' ";
				$param=[];
				
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND locality=(SELECT city from type_cities WHERE id=?)";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND dependent_locality=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND thoroughfare=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND price>=?";
					$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND price<=?";
					$param[]=(int)$price_last;
				}
				
				$floor_first=$request->input('floor_first');
				if($floor_first) {
					$query_string.=" AND floor>=?";
					$param[]=$floor_first;
				}
				$floor_last=$request->input('floor_last');
				if($floor_last) {
					$query_string.=" AND floor<=?";
					$param[]=(int)$floor_last;
				}
				$no_first_floor=$request->input('no_first_floor');
				if($no_first_floor){
					$query_string.=" AND floor!=1";
				}
				$no_last_floor=$request->input('no_last_floor');
				if($no_last_floor){
					$query_string.=" AND floor!=floor_all";
				}
				
				$total_area_first=$request->input('total_area_first');
				if($total_area_first){
					$query_string.=" AND area_general>=?";
					$param[]=(int)$total_area_first;
				}
				
				$total_area_last=$request->input('total_area_last');
				if($total_area_last){
					$query_string.=" AND area_general<=?";
					$param[]=$total_area_last;
				}
				
				$id_type_apartment=$request->input('id_type_apartment');
				if($id_type_apartment){
					$query_string.=" AND id_reference_type_apartment=?";
					$param[]=$id_type_apartment;
				}
				
				$id_state=$request->input('id_type_state');
				if($id_state){
					$query_string.=" AND id_reference_state=?";
					$param[]=$id_state;
				}
				
				$only_new_apartment=$request->input('only_new_apartment');
				if($only_new_apartment) {
					$query_string.=" AND id_reference_fund=1";
				}
				
				$sort=$request->input("sort");
				$order=$request->input("order");
				if($sort&&$order){
					switch($sort){
						case 'date_create':
						$query_string.=" ORDER BY realestate.created_at ".($order=="desc"?"DESC":"ASC");
						break;
						case 'price':
						$query_string.=" ORDER BY price ".($order=="desc"?"DESC":"ASC");
						break;
						case 'total_area':
						$query_string.=" ORDER BY area_general ".($order=="desc"?"DESC":"ASC");
						break;
					}
				}
				
				$estate=DB::select($query_string, $param);
				
				$details=[];
				if($id_type_apartment) $details[]=DB::select("select type_apartment from reference_type_apartments WHERE id=?",[$id_type_apartment])[0]->type_apartment;
				if($id_state) $details[]=DB::select("select state from reference_state WHERE id=?",[$id_state])[0]->state;
				if($price_first||$price_last) {
					$temp="";
					if($price_first) $temp.=" от ".(int)$price_first;
					if($price_last) $temp.=" до ".(int)$price_last;
					$temp.=" руб.";
					$details[]=$temp;
				}
				if($floor_first||$floor_last) {
					$temp="";
					if($floor_first) $temp.=" от ".(int)$floor_first;
					if($floor_last) $temp.=" до ".(int)$floor_last;
					$temp.=" этажа";
					$details[]=$temp;
				}
				if($total_area_first||$total_area_last) {
					$temp="";
					if($total_area_first) $temp.=" от ".(int)$total_area_first;
					if($total_area_last) $temp.=" до ".(int)$total_area_last;
					$temp.=" м<sup>2</sup>";
					$details[]=$temp;
				}
				if($only_new_apartment) 
				{
					$temp="только новостройки";
					$details[]=$temp;
				}
				
				$location=[];
				if($id_city) $location[]=DB::select("SELECT city from type_cities WHERE id=?",[$id_city])[0]->city;
				if($district) $location[]=$district;
				if($street) $location[]=$street;
				
				echo view("layouts.blocks.ll-search-result-count", ['percent_sell'=>$percent_sell, 'number_records'=>count($estate),'details'=>$details, 'location'=>$location]);
				
				
				for($i=0;$i<count($estate);$i++){
					echo view("layouts.blocks.ll-search-sb-result-list",['id'=>$i,'estate'=>$estate[$i], 'location'=>$location]);
				}
				return;
				break;
			
			case 'rent':
				$query_string="select realestate.id, cover_src, price, floor, floor_all, area_general, 
				locality as city, 
				dependent_locality as administrative_district, 
				district, 
				thoroughfare as street, 
				reference_type_apartments.type_apartment, reference_type_apartments.room_count
				from realestate 
				INNER JOIN $rin[1] ON realestate.realestate_instance_id=$rin[1].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				INNER JOIN reference_type_apartments ON reference_type_apartments.id=$rin[1].id_reference_type_apartment
				WHERE realestate_instance_type='$rit[1]' ";
				$param=[];
				
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND locality=(SELECT city from type_cities WHERE id=?)";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND dependent_locality=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND thoroughfare=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND `price`>?";
					$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND `price`<=?";
					$param[]=(int)$price_last;
				}
				
				$floor_first=$request->input('floor_first');
				if($floor_first) {
					$query_string.=" AND floor>=?";
					$param[]=(int)$floor_first;
				}
				$floor_last=$request->input('floor_last');
				if($floor_last) {
					$query_string.=" AND floor<=?";
					$param[]=(int)$floor_last;
				}
				$no_first_floor=$request->input('no_first_floor');
				if($no_first_floor){
					$query_string.=" AND floor!=1";
				}
				$no_last_floor=$request->input('no_last_floor');
				if($no_last_floor){
					$query_string.=" AND floor!=floor_all";
				}
				
				$total_area_first=$request->input('total_area_first');
				if($total_area_first){
					$query_string.=" AND area_general>=?";
					$param[]=$total_area_first;
				}
				$total_area_last=$request->input('total_area_last');
				if($total_area_last){
					$query_string.=" AND area_general<=?";
					$param[]=$total_area_last;
				}
				
				$id_type_apartment=$request->input('id_type_apartment');
				if($id_type_apartment){
					$query_string.=" AND id_reference_type_apartment=?";
					$param[]=$id_type_apartment;
				}
				
				$id_state=$request->input('id_type_state');
				if($id_state){
					$query_string.=" AND id_reference_state=?";
					$param[]=$id_state;
				}
				
				$id_furniture=$request->input('id_type_furniture');
				if($id_furniture){
					$query_string.=" AND id_reference_furniture=?";
					$param[]=$id_furniture;
				}
				
				$only_new_apartment=$request->input('only_new_apartment');
				if($only_new_apartment) {
					$query_string.=" AND id_reference_fund=1";
				}
				
				$sort=$request->input("sort");
				$order=$request->input("order");
				if($sort&&$order){
					switch($sort){
						case 'date_create':
						$query_string.=" ORDER BY realestate.created_at ".($order=="desc"?"DESC":"ASC");
						break;
						case 'price':
						$query_string.=" ORDER BY price ".($order=="desc"?"DESC":"ASC");
						break;
						case 'total_area':
						$query_string.=" ORDER BY area_general ".($order=="desc"?"DESC":"ASC");
						break;
					}
				}
				
				$estate=DB::select($query_string, $param);
				
				$details=[];
				if($id_type_apartment) $details[]=DB::select("select type_apartment from reference_type_apartments WHERE id=?",[$id_type_apartment])[0]->type_apartment;
				if($id_state) $details[]=DB::select("select state from reference_state WHERE id=?",[$id_state])[0]->state;
				if($price_first||$price_last) {
					$temp="";
					if($price_first) $temp.=" от ".(int)$price_first;
					if($price_last) $temp.=" до ".(int)$price_last;
					$temp.=" руб.";
					$details[]=$temp;
				}
				if($floor_first||$floor_last) {
					$temp="";
					if($floor_first) $temp.=" от ".(int)$floor_first;
					if($floor_last) $temp.=" до ".(int)$floor_last;
					$temp.=" этажа";
					$details[]=$temp;
				}
				if($total_area_first||$total_area_last) {
					$temp="";
					if($total_area_first) $temp.=" от ".(int)$total_area_first;
					if($total_area_last) $temp.=" до ".(int)$total_area_last;
					$temp.=" м<sup>2</sup>";
					$details[]=$temp;
				}
				if($only_new_apartment) 
				{
					$temp="только новостройки";
					$details[]=$temp;
				}
				
				$location=[];
				if($id_city) $location[]=DB::select("SELECT city from type_cities WHERE id=?",[$id_city])[0]->city;
				if($district) $location[]=$district;
				if($street) $location[]=$street;
				
				echo view("layouts.blocks.ll-search-result-count", ['percent_sell'=>$percent_sell, 'number_records'=>count($estate),'details'=>$details, 'location'=>$location]);
				
				for($i=0;$i<count($estate);$i++){
					echo view("layouts.blocks.ll-search-sb-result-list",['id'=>$i,'estate'=>$estate[$i]]);
				}
				return;
				break;
			case 'households':
				$query_string="select realestate.id, cover_src, price, area_general, 
				locality as city, 
				dependent_locality as administrative_district, 
				district, 
				thoroughfare as street, 
				room_count, floors
				from realestate 
				INNER JOIN $rin[2] ON realestate.realestate_instance_id=$rin[2].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				WHERE realestate_instance_type='$rit[2]' ";
				$param=[];
				
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND locality=(SELECT city from type_cities WHERE id=?)";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND dependent_locality=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND thoroughfare=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND price>=?";
					$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND price<=?";
					$param[]=(int)$price_last;
				}
				
				$stead_first=$request->input('stead_first');
				if($stead_first) {
					$query_string.=" AND `stead`>=?";
					$param[]=(int)$stead_first;
				}
				$stead_last=$request->input('stead_last');
				if($stead_last) {
					$query_string.=" AND `stead`<=?";
					$param[]=(int)$stead_last;
				}
				
				$facade_first=$request->input('facade_first');
				if($facade_first) {
					$query_string.=" AND `facade`>=?";
					$param[]=(int)$facade_first;
				}
				$facade_last=$request->input('facade_last');
				if($facade_last) {
					$query_string.=" AND `facade`<=?";
					$param[]=(int)$facade_last;
				}
				
				$id_wall=$request->input('id_wall');
				if($id_wall) {
					$query_string.=" AND `id_reference_wall_material`=?";
					$param[]=(int)$id_wall;
				}
			
				$id_entry=$request->input('id_entry');
				if($id_entry) {
					$query_string.=" AND `id_reference_entry`=?";
					$param[]=(int)$id_entry;
					//если въезд не установлен и id_entry=3, т.е. нет въезда
					//тогда фильтрация пройдена, если id_entry!=3, то не пройдена
				}
			
				$id_yard=$request->input('id_yard');
				if($id_yard) {
					$query_string.=" AND `id_reference_yard`=?";
					$param[]=(int)$id_yard;
				}
				
				$sort=$request->input("sort");
				$order=$request->input("order");
				if($sort&&$order){
					switch($sort){
						case 'date_create':
						$query_string.=" ORDER BY realestate.created_at ".($order=="desc"?"DESC":"ASC");
						break;
						case 'price':
						$query_string.=" ORDER BY price ".($order=="desc"?"DESC":"ASC");
						break;
						case 'total_area':
						$query_string.=" ORDER BY stead ".($order=="desc"?"DESC":"ASC");
						break;
					}
				}
				$estate=DB::select($query_string, $param);
				
				$details=[];
				if($price_first||$price_last) {
					$temp="";
					if($price_first) $temp.=" от ".(int)$price_first;
					if($price_last) $temp.=" до ".(int)$price_last;
					$temp.=" руб.";
					$details[]=$temp;
				}
				if($stead_first||$stead_last) {
					$temp="";
					if($stead_first) $temp.=" от ".(int)$stead_first;
					if($stead_last) $temp.=" до ".(int)$stead_last;
					$temp.=" ар";
					$details[]=$temp;
				}
				if($facade_first||$facade_last) {
					$temp="фасад";
					if($facade_first) $temp.=" от ".(int)$facade_first;
					if($facade_last) $temp.=" до ".(int)$facade_last;
					$temp.=" метров";
					$details[]=$temp;
				}
				if($id_wall) $details[]="стены: ".DB::select("select wall_material from reference_wall_material_households WHERE id=?",[$id_wall])[0]->wall_material;
				if($id_entry) $details[]="выезд: ".DB::select("select entry from reference_entries WHERE id=?",[$id_entry])[0]->entry;
				if($id_yard) $details[]="двор: ".DB::select("select yard from reference_yard_households  WHERE id=?",[$id_yard])[0]->yard;
				
				$location=[];
				if($id_city) $location[]=DB::select("SELECT city from type_cities WHERE id=?",[$id_city])[0]->city;
				if($district) $location[]=$district;
				if($street) $location[]=$street;
				
				echo view("layouts.blocks.ll-search-result-count", ['percent_sell'=>$percent_sell, 'number_records'=>count($estate),'details'=>$details, 'location'=>$location]);
				
				for($i=0;$i<count($estate);$i++){
					echo view("layouts.blocks.ll-search-sb-result-households-list",['id'=>$i,'estate'=>$estate[$i]]);
				}
				return;
				break;
			case 'land':
				$com_join="";
				$communications=$request->input('communications');
				if($communications) {
					$com_join="INNER";
				}
				else $com_join="LEFT";
				
				$query_string="select realestate.id, cover_src, price, stead, 
				locality as city, 
				dependent_locality as administrative_district, 
				district, 
				thoroughfare as street
				from realestate 
				INNER JOIN $rin[3] ON realestate.realestate_instance_id=$rin[3].id 
				INNER JOIN addresses ON realestate.id_address=addresses.id
				$com_join JOIN pivot_realestate_lands_reference_communications ON $rin[3].id=pivot_realestate_lands_reference_communications.id_realestate_land
				WHERE realestate_instance_type='$rit[3]' ";
				$param=[];
				
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND locality=(SELECT city from type_cities WHERE id=?)";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND dependent_locality=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND thoroughfare=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND price>=?";
					$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND price<=?";
					$param[]=(int)$price_last;
				}
				
				$stead_first=$request->input('stead_first');
				if($stead_first) {
					$query_string.=" AND `stead`>=?";
					$param[]=(int)$stead_first;
				}
				$stead_last=$request->input('stead_last');
				if($stead_last) {
					$query_string.=" AND `stead`<=?";
					$param[]=(int)$stead_last;
				}
				
				$facade_first=$request->input('facade_first');
				if($facade_first) {
					$query_string.=" AND `facade`>=?";
					$param[]=(int)$facade_first;
				}
				$facade_last=$request->input('facade_last');
				if($facade_last) {
					$query_string.=" AND `facade`<=?";
					$param[]=(int)$facade_last;
				}
				
				$sort=$request->input("sort");
				$order=$request->input("order");
				if($sort&&$order){
					switch($sort){
						case 'date_create':
						$query_string.=" ORDER BY realestate.created_at ".($order=="desc"?"DESC":"ASC");
						break;
						case 'price':
						$query_string.=" ORDER BY price ".($order=="desc"?"DESC":"ASC");
						break;
						case 'total_area':
						$query_string.=" ORDER stead ".($order=="desc"?"DESC":"ASC");
						break;
					}
				}
				
				$estate=DB::select($query_string, $param);
				
				$details=[];
				if($price_first||$price_last) {
					$temp="";
					if($price_first) $temp.=" от ".(int)$price_first;
					if($price_last) $temp.=" до ".(int)$price_last;
					$temp.=" руб.";
					$details[]=$temp;
				}
				if($stead_first||$stead_last) {
					$temp="";
					if($stead_first) $temp.=" от ".(int)$stead_first;
					if($stead_last) $temp.=" до ".(int)$stead_last;
					$temp.=" ар";
					$details[]=$temp;
				}
				if($facade_first||$facade_last) {
					$temp="фасад";
					if($facade_first) $temp.=" от ".(int)$facade_first;
					if($facade_last) $temp.=" до ".(int)$facade_last;
					$temp.=" метров";
					$details[]=$temp;
				}
				if($communications){
					$details[]="с коммуникациями";
				}
				
				$location=[];
				if($id_city) $location[]=DB::select("SELECT city from type_cities WHERE id=?",[$id_city])[0]->city;
				if($district) $location[]=$district;
				if($street) $location[]=$street;
				
				echo view("layouts.blocks.ll-search-result-count", ['percent_sell'=>$percent_sell, 'number_records'=>count($estate),'details'=>$details, 'location'=>$location]);
				
				for($i=0;$i<count($estate);$i++){
					//var_dump($estate[$i]);
					//echo "<br><br>\n\n";
					echo view("layouts.blocks.ll-search-sb-result-lands-list",['id'=>$i,'estate'=>$estate[$i]]);
				}
				return;
				break;
			
				/*
			case 'land':
				$com_join="";
				$communications=$request->input('communications');
				if($communications) {
					$com_join="INNER";
				}
				else $com_join="LEFT";
				
				$query_string="select realestate.id, $rin[3].cost, $rin[3].special_price, $rin[3].stead, $rin[3].steadY, address.street, type_district.district, GROUP_CONCAT(type_communication.communication) as communication
				from realestate 
				INNER JOIN $rin[3] ON realestate.realestate_instance_id=$rin[3].id 
				INNER JOIN address ON realestate.id_address=address.id
				INNER JOIN type_district ON address.id_district=type_district.id
				LEFT JOIN type_administrative_district ON address.id_administrative_district=type_administrative_district.id
				$com_join JOIN realestate_land_communication ON realestate_land.id=realestate_land_communication.id_realestate_land
				$com_join JOIN type_communication ON type_communication.id = realestate_land_communication.id_communication 
				WHERE realestate_instance_type='$rit[3]' ";
				$param=[];
				$id_city=$request->input('id_city');
				if($id_city){
					$query_string.=" AND type_district.id_city=?";
					$param[]=(int)$id_city;
				}
				
				$district=$request->input('district');
				if($district){
					$query_string.=" AND administrative_district=?";
					$param[]=$district;
				}
				
				$street=$request->input('street');
				if($street){
					$query_string.=" AND street=?";
					$param[]=$street;
				}
				
				$price_first=$request->input('price_first');
				if($price_first) {
					$query_string.=" AND IF(`special_price`>0, `special_price`>?, `cost`>?)";
					$param[]=(int)$price_first;$param[]=(int)$price_first;
					
				}
				$price_last=$request->input('price_last');
				if($price_last) {
					$query_string.=" AND IF(`special_price`>0, `special_price`<?, `cost`<?)";
					$param[]=(int)$price_last;$param[]=(int)$price_last;
				}
				
				//
				
				$stead_first=$request->input('stead_first');
				if($stead_first) {
					$query_string.=" AND `stead`>=?";
					$param[]=(int)$stead_first;
				}
				$stead_last=$request->input('stead_last');
				if($stead_last) {
					$query_string.=" AND `stead`<=?";
					$param[]=(int)$stead_last;
				}
				
				$facade_first=$request->input('facade_first');
				if($facade_first) {
					$query_string.=" AND `steadY`>=?";
					$param[]=(int)$facade_first;
				}
				$facade_last=$request->input('facade_last');
				if($facade_last) {
					$query_string.=" AND `steadY`<=?";
					$param[]=(int)$facade_last;
				}
				
				$query_string.=" GROUP BY realestate.id";
				
				$sort=$request->input("sort");
				$order=$request->input("order");
				if($sort&&$order){
					switch($sort){
						case 'date_create':
						$query_string.=" ORDER BY date_create ".($order=="desc"?"DESC":"ASC");
						break;
						case 'price':
						$query_string.=" ORDER BY IF(special_price>0, special_price, cost) ".($order=="desc"?"DESC":"ASC");
						break;
						case 'total_area':
						$query_string.=" ORDER BY stead ".($order=="desc"?"DESC":"ASC");
						break;
					}
				}
				
				//echo $query_string;
				$lands=DB::select($query_string,$param);
				echo "<table border=\"1\"><tr><th>id</th><th>Район</th><th>Улица</th><th>Цена</th><th>Акционная цена</th><th>Площадь, ар</th><th>Фасад</th><th>Коммуникации</th></tr>";
				foreach($lands as $land){
					echo "<tr><td>".$land->id."</td><td>".$land->district."</td><td>".$land->street."</td><td>".$land->cost."</td><td>".$land->special_price."</td><td>".$land->stead."</td><td>".$land->steadY."</td><td>".$land->communication."</td></tr>";
				}
				echo "</table>";
				*/
		}
	}
}
