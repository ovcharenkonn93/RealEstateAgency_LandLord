<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Illuminate\Http\Request;

class SearchFormController extends Controller 
{
	public function index(Request $request){
		$permitted=["type_cities","reference_profiles","reference_type_apartments","reference_state",
		"reference_wall_material_apartments","reference_wall_material_households","reference_entries",
		"reference_furniture","type_administrative_district"];
		$ret=[];
		if($request->input("tables")=="all")
		{
			$query=$permitted;
		}
		else{
			$query=explode(';',$request->input("tables"),100);
		}	
		foreach($query as $reference){
			if(in_array($reference,$permitted)){
				try{
					$ret[$reference]=DB::select("select * from $reference");
				}
				catch(\Illuminate\Database\QueryException $e){
					$ret[$reference]=[];
				}
			}
		}
		return json_encode($ret);
	}
}
