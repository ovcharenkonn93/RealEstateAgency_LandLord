<?php namespace App\Http\Controllers;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{ 
		$news = \DB::table('news')->orderBy('created_at', 'DESC')->take(3)->get();		
        
		$list = [];

        foreach($news as $new) {
            $list['news'][] = (object) ['id' => $new->id, 'name' => $new->name, 'description' => $new->description,
                       'content' => $new->content, 'created_at' => date('d.m.Y', strtotime($new->created_at))];
        }				
			return \View::make('welcome',['news'=>$list]);	
	}

}
