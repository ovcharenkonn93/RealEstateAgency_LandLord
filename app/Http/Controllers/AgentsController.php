<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;

class AgentsController extends Controller 
{
	public function index(Request $request)
	{
		$query_string="SELECT employes.id, CONCAT_WS( ' ', surname, name, patronymic ) AS fullname, email, phone_mob, phone_home, 'loginskype' as skype, src FROM employes";
		$param=[];
		
		/*$special=$request->input("special");
		if($special){
			switch($special){
				case 1:
					$query_string.=" INNER JOIN departments ON departments.id=?";
					$param[]=4;
			}
			
		}*/
		$agents=DB::select($query_string, $param);
		//var_dump($agents);
		return view("layouts.agents", ['agents' => $agents]);
		//return view('layouts.blocks.ll-agents-grid',['i'=>0, 'agent'=>$agents[0]]);
	}
}
