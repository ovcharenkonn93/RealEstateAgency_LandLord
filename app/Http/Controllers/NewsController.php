<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;

class NewsController extends Controller 
{
	public function index($id)
	{
		$news=DB::select("SELECT * FROM news WHERE id=?", [$id]);
		return view('layouts.company-news-page',['news'=>isset($news[0])?$news[0]:NULL]);
	}
}
