<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/search-sb', 'SearchController@index');
Route::get('/search-sb/get', 'SearchController@get');
Route::get('/search-sb/get_coordinates','SearchController@get_coordinates');
Route::get('/search-sb/get_estate_from_id','SearchController@get_estate_from_id');
Route::get('/search-sb2', 'SearchController2@index');
Route::get('/search-sb2/get', 'SearchController2@get');

Route::get("/info/like/{id}", 'InfoController@like');
Route::get("/info/{id}", 'InfoController@index');

Route::get('/agents', 'AgentsController@index');

Route::get('/offices', 'OfficesController@index');
Route::get('/offices/select', 'OfficesController@select');
Route::get('/offices/get', 'OfficesController@get');
Route::get('/offices/getmap', 'OfficesController@getmap');

Route::get('/SearchForm', 'SearchFormController@index');

Route::get('/clients', 'ClientsController@index');

Route::get('/', 'WelcomeController@index');

Route::get('index', 'WelcomeController@index');

Route::get('home', 'HomeController@index');


Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');



// Вызов страниц
Route::get('services', 'Site_pages@services');
Route::get('services-for-buyer', 'Site_pages@services_for_buyer');
Route::get('services-for-seller', 'Site_pages@services_for_seller');
Route::get('services-for-tenant', 'Site_pages@services_for_tenant');
Route::get('services-for-owner', 'Site_pages@services_for_owner');
Route::get('services-additional', 'Site_pages@services_additional');

Route::get('services/{page_name}', 'Site_pages@index_services');

Route::get('{page_name}', 'Site_pages@index');

Route::get('mortgage/{page_name}', 'Site_pages@index_mortgage');

		
Route::get('favorites', function(){
			 if(isset($_COOKIE['favorites'])) // проверяем, есть ли заказы
			{
			  $favorites = $_COOKIE['favorites'];
			  $favorites=json_decode($favorites); //перекодируем строку из куки в массив с объектами
			}
			else
			{
			   $favorites=[];
			}
				return View::make('layouts.favorites',['favorites'=>$favorites]);
			}
		);
		
		
Route::get('search', function(){

			$limit=10;
				if (Request::ajax()) {
				$limit=$_POST['limit']; 
				}
				
				return View::make('layouts.search',['limit'=>$limit]);
			}
		);

		
//Не работает

//Route::post('search', array('before'=>'csrf-ajax', 'as'=>'test', 'uses'=>'FooController@foo'));




// Подробная информация о квартире на отдельной странице
Route::get('info/{infoId}', function($infoId){
		return View::make('layouts.info',['infoId'=>$infoId]);
    }
);

// Подробная информация о квартире через параметр
Route::get('info&id={infoId}', function($infoId){
        return View::make('layouts.info',['infoId'=>$infoId]);
    }
);

// Для тестирования на отдельной странице
Route::get('test-api/{infoId}', function($infoId){
		return View::make('layouts.test-api',['infoId'=>$infoId]);
    }
);

// Для тестирования через параметр
Route::get('test-api&id={infoId}', function($infoId){
        return View::make('layouts.test-api',['infoId'=>$infoId]);
    }
);



// Страница одной новости 
Route::get('news/{id}', 'NewsController@index');
		//function($newsId){
	
		//return View::make('layouts.company-news-page',['newsId'=>$newsId]);
    //}
//);



// Персональная страница агента
Route::get('agent-info/{agentInfoId}', function($agentInfoId){
		return View::make('layouts.agent-info',['agentInfoId'=>$agentInfoId]);
    }
);


//API
Route::match(['get', 'post'], 'api', 'ApiController@index');




// Фильтр для ajax
Route::filter('csrf-ajax', function()
{
    if (Session::token() != Request::header('x-csrf-token'))
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});