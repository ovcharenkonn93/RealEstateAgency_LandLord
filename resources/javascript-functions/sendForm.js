// Подготовка критериев запроса к API
function sendForm(){
    console.log('Запустилась функция sendForm');
    // Запуск поиска
	
	// [option] - имя переменной , [essence] - класс формы, по к-му фильтр
	/*
	var option_first='',option_last='';
	if ($(".essence .ll-search-form__min-value").val() != '') 
		option_first='&option_first='+ $(".essence .ll-search-form__min-value").val();
	if ($(".essence .ll-search-form__max-value").val() != '') 
		option_last='&option_last='+ $(".essence .ll-search-form__max-value").val();
	*/
	
	//универсальные параметры
    // [id_city] - id города из справочника type_cities.
    var id_city = '';
    if ($(".ll-search-form__city").val() != null) {
        id_city = '&id_city='+$(".ll-search-form__city").val();
    }

    //[district] - полное название района города
    var district = '';
    if ($(".ll-search-form__district").val() != null) {
        district = '&district='+ $(".ll-search-form__district").val();
    }

    // [subdistrict] - полное название подрайона города из справочника type_district.
    var subdistrict = '';
    if ($(".ll-search-form__subdistrict").val() != null) {
        subdistrict = '&subdistrict='+ $(".ll-search-form__subdistrict").val();
    }

    var street = '';
    if ($(".ll-search-form__street").val() != null) {
        street = '&street='+$(".ll-search-form__street").val();
    }

	// [price] - имя переменной , [ll-search-form__cost] - класс формы, по к-му фильтр
	var price_first='',price_last='';
	if ($(".ll-search-form__cost .ll-search-form__min-value").val() != '') 
		price_first='&price_first='+ $(".ll-search-form__cost .ll-search-form__min-value").val();
	if ($(".ll-search-form__cost .ll-search-form__max-value").val() != '') 
		price_last='&price_last='+ $(".ll-search-form__cost .ll-search-form__max-value").val();
	
	//[sort] - поле сортировки. Поддерживаемые значения: [sort] - date_create / total_area / price / price_per_meter
    var sort = '&sort='+$(".ll-search-results-sorting select").val().split('|')[0];

    //[order] - тип сортировки. Поддерживаемые значения: [order] - asc / desc
    var order='&order='+ $(".ll-search-results-sorting select").val().split('|')[1];
	
	
	// [realestate_type] - тип недвижимости. Возможные значения: apartment, rent, households, rent, land
    var realestate_type = '', criteria='';

    if (($("#ll-search-form__estate-type_apartment").prop("checked"))&&($("#ll-search-form__radio-sale").prop("checked"))) realestate_type = 'apartment';
    if (($("#ll-search-form__estate-type_apartment").prop("checked"))&&($("#ll-search-form__radio-rent").prop("checked"))) realestate_type = 'rent';
    if ($("#ll-search-form__estate-type_household").prop("checked")) realestate_type = 'households';
    if ($("#ll-search-form__estate-type_land").prop("checked")) realestate_type = 'land';
	//специальные параметры
	
	switch(realestate_type)
	{
		case 'apartment':
			// [id_type_apartment] - id типа квартиры из справочника type_apartment.
			var id_type_apartment = '';
			if ($(".ll-search-form__apartment").val() != null) {
				id_type_apartment = '&id_type_apartment='+$(".ll-search-form__apartment").val();
			}
		
			// [id_type_state] - id типа состояния квартиры из справочника type_realestate_state.
			var id_type_state = '';
			if ($(".ll-search-form__state").val() != null) {
				id_type_state = '&id_type_state='+$(".ll-search-form__state").val();
			}
		
			// [floor] - этаж. Значения могут быть перечислены через ';' или ':'
			var floor_first='',floor_last='';
			if ($(".ll-search-form__floor .ll-search-form__min-value").val() != '') 
				floor_first='&floor_first='+ $(".ll-search-form__floor .ll-search-form__min-value").val();
			if ($(".ll-search-form__floor .ll-search-form__max-value").val() != '') 
				floor_last='&floor_last='+ $(".ll-search-form__floor .ll-search-form__max-value").val();

			var no_first_floor='';
			if($("#ll-search-form__checkbox-not-first").prop("checked")) 
				no_first_floor="&no_first_floor=1";
			var no_last_floor='';
			if($("#ll-search-form__checkbox-not-last").prop("checked")) 
				no_last_floor="&no_last_floor=1";
			
			// [total_area] - общая площадь.	
			var total_area_first='',total_area_last='';
			if ($(".ll-search-form__total-area .ll-search-form__min-value").val() != '') 
				total_area_first='&total_area_first='+ $(".ll-search-form__total-area .ll-search-form__min-value").val();
			if ($(".ll-search-form__total-area .ll-search-form__max-value").val() != '') 
				total_area_last='&total_area_last='+ $(".ll-search-form__total-area .ll-search-form__max-value").val();
		
			var only_new_appartment='';
			if($("#ll-search-form__checkbox-new-appartment").prop("checked")) 
				only_new_appartment="&only_new_apartment=1";
			
			console.log("realestate_type="+realestate_type);
			realestate_type = 'realestate_type='+realestate_type;
			criteria = realestate_type+id_city+district+subdistrict+street+price_first+price_last+floor_first+floor_last+no_first_floor+no_last_floor+total_area_first+total_area_last+id_type_apartment+id_type_state+only_new_appartment+sort+order;
		break;
		case 'rent':
			// [id_type_apartment] - id типа квартиры из справочника type_apartment.
			var id_type_apartment = '';
			if ($(".ll-search-form__apartment").val() != null) {
				id_type_apartment = '&id_type_apartment='+$(".ll-search-form__apartment").val();
			}
		
			// [id_type_state] - id типа состояния квартиры из справочника type_realestate_state.
			var id_type_state = '';
			if ($(".ll-search-form__state").val() != null) {
				id_type_state = '&id_type_state='+$(".ll-search-form__state").val();
			}
			
			// [id_type_furniture] - id мебелировки из  type_realestate_furniture.
			var id_type_furniture = '';
			if ($(".ll-search-form__furniture").val() != null) {
				id_type_furniture = '&id_type_furniture='+$(".ll-search-form__furniture").val();
			}
		
			// [floor] - этаж. Значения могут быть перечислены через ';' или ':'
			var floor_first='',floor_last='';
			if ($(".ll-search-form__floor .ll-search-form__min-value").val() != '') 
				floor_first='&floor_first='+ $(".ll-search-form__floor .ll-search-form__min-value").val();
			if ($(".ll-search-form__floor .ll-search-form__max-value").val() != '') 
				floor_last='&floor_last='+ $(".ll-search-form__floor .ll-search-form__max-value").val();

			var no_first_floor='';
			if($("#ll-search-form__checkbox-not-first").prop("checked")) 
				no_first_floor="&no_first_floor=1";
			var no_last_floor='';
			if($("#ll-search-form__checkbox-not-last").prop("checked")) 
				no_last_floor="&no_last_floor=1";
			
			// [total_area] - общая площадь.	
			var total_area_first='',total_area_last='';
			if ($(".ll-search-form__total-area .ll-search-form__min-value").val() != '') 
				total_area_first='&total_area_first='+ $(".ll-search-form__total-area .ll-search-form__min-value").val();
			if ($(".ll-search-form__total-area .ll-search-form__max-value").val() != '') 
				total_area_last='&total_area_last='+ $(".ll-search-form__total-area .ll-search-form__max-value").val();
		
			var only_new_appartment='';
			if($("#ll-search-form__checkbox-new-appartment").prop("checked")) 
				only_new_appartment="&only_new_appartment=1";
			
			realestate_type = 'realestate_type='+realestate_type;
			criteria = realestate_type+id_city+district+subdistrict+street+price_first+price_last+floor_first+floor_last+no_first_floor+no_last_floor+total_area_first+total_area_last+id_type_apartment+id_type_state+id_type_furniture+only_new_appartment+sort+order;
		break;
		case 'households':
			// [stead] - количество соток земли
			var stead_first='',stead_last='';
			if ($(".ll-search-form__sotki .ll-search-form__min-value").val() != '') 
				stead_first='&stead_first='+ $(".ll-search-form__sotki .ll-search-form__min-value").val();
			if ($(".ll-search-form__sotki .ll-search-form__max-value").val() != '') 
				stead_last='&stead_last='+ $(".ll-search-form__sotki .ll-search-form__max-value").val();

			// [facade] - фасад
			var facade_first='',facade_last='';
			if ($(".ll-search-form__fasad .ll-search-form__min-value").val() != '') 
				facade_first='&facade_first='+ $(".ll-search-form__fasad .ll-search-form__min-value").val();
			if ($(".ll-search-form__fasad .ll-search-form__max-value").val() != '') 
				facade_last='&facade_last='+ $(".ll-search-form__fasad .ll-search-form__max-value").val();

			// [id_wall] - id типа стены
			var id_wall = '';
			if ($(".ll-search-form__walls-households").val() != null) {
				id_wall = '&id_wall='+$(".ll-search-form__walls-households").val();
			}

			// [id_entry] - id типа въезда
			var id_entry = '';
			if ($(".ll-search-form__entry").val() != null) {
				id_entry = '&id_entry='+$(".ll-search-form__entry").val();
			}
			
			var id_yard = $('.llg-for-household input[name=yard]:checked').val();
			if(id_yard) id_yard= '&id_yard='+id_yard;

			realestate_type = '&realestate_type='+realestate_type;
			criteria=realestate_type+id_city+district+subdistrict+street+price_first+price_last+sort+order+stead_first+stead_last+facade_first+facade_last+id_wall+id_entry+id_yard;
		break;
		case 'land':
			// [stead] - количество соток земли
			var stead_first='',stead_last='';
			if ($(".ll-search-form__sotki .ll-search-form__min-value").val() != '') 
				stead_first='&stead_first='+ $(".ll-search-form__sotki .ll-search-form__min-value").val();
			if ($(".ll-search-form__sotki .ll-search-form__max-value").val() != '') 
				stead_last='&stead_last='+ $(".ll-search-form__sotki .ll-search-form__max-value").val();

			// [facade] - фасад
			var facade_first='',facade_last='';
			if ($(".ll-search-form__fasad .ll-search-form__min-value").val() != '') 
				facade_first='&facade_first='+ $(".ll-search-form__fasad .ll-search-form__min-value").val();
			if ($(".ll-search-form__fasad .ll-search-form__max-value").val() != '')
				facade_last= '&facade_last=' + $(".ll-search-form__fasad .ll-search-form__max-value").val();	
			
			var communications='';
			if($("#ll-search-form__checkbox-communications").prop("checked")) 
				communications="&communications=1";
			
			var id_corner ='';
			if($('.llg-for-land input[name=corner]:checked').val())
				id_corner= '&id_corner='+$('.llg-for-land input[name=corner]:checked').val();
			
			realestate_type = '&realestate_type='+realestate_type;
			criteria=realestate_type+id_city+district+subdistrict+street+price_first+price_last+sort+order+stead_first+stead_last+facade_first+facade_last+communications+id_corner;
	}

	AjaxRequest(criteria); // Запрос к API для получения списка

    // Обязательные параметры: [ type, format, realestate_type ]
    // [format] - формат API-запроса. Возможные значения: plain, json
    
    //realestate_type = 'realestate_type='+realestate_type;

    // Обязательные параметры: [type, format, realestate_type]
    //var criteria = 'type=search&format=plain'+realestate_type+id_city+district+subdistrict+street+price_first+price_last+floor_first+floor_last+total_area_first+total_area_last+id_type_apartment+id_type_state+only_new_appartment+sort+order;
}
// ---------------------------------------------------------------------------------------------------------------------
