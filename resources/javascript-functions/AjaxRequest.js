// Запрос к API для получения списка
function AjaxRequest(criteria) {
console.log('Запустилась функция AjaxRequest');
if(!this.no_first_launch){
	this.no_first_launch=1;
	criteria+="&update=1";
}
console.log('Параметры запроса к API:', criteria);
	$.ajax({
		url: 'search-sb/get_coordinates',
		type: 'GET',
		data: criteria,
		success: function(response){
			data=JSON.parse(response);
			marks.forEach(function(mark){
				mark.setMap(null);
			})
			marks.length=0;
			point_flag=true;
			if(map_flag){
				for(var i=0;i<marks.length;i++)
				{
					marks[i].setMap(null);
				}
				marks.length=0;
				data.forEach(function(coord){
					var mark=new google.maps.Marker({position: {lat: coord.latitude, lng: coord.longitude}, map: map});
					mark.addListener("click", function(){
						$.ajax({
							url: 'search-sb/get_estate_from_id',
							type: 'GET',
							data: 'id='+coord.id,
							success: function(response){
								/*var infowindow= new google.maps.InfoWindow({
									content: response
								});*/
								infowindow.setContent(response);
								infowindow.open(map, mark);
							}
						})
					});
					//mark.addListener("click", view_map_estate(mark,coord.id));
					marks.push(mark);
					latlngbounds.extend(mark.position);
				})
				map.setCenter( latlngbounds.getCenter(), map.fitBounds(latlngbounds));
			}
			else{
				data.forEach(function(coord){
				var mark=new google.maps.Marker({position: {lat: coord.latitude, lng: coord.longitude}});
				marks.push(mark);
				})
			}
		}
	});
    $.ajax(
        {
            url: 'search-sb/get',
            type: 'GET',
            data: criteria,
            success: function(data){
                $('#data-list').html(data);
                //console.log('Результат запроса к API:', data);
                $('.data-list').show();
                $('.data-grid').show();
                $('.search-result_empty').hide();
                $('.ll-pagination').show();
                $('.ll-search-results-limit').show();
                $('.ll-search-results-sorting').show();               

                if (data.length==0){
                    $('.data-list').hide();
                    $('.data-grid').hide();
                    $('.search-result_empty').show();
                    $('.ll-pagination').hide();
                    $('.ll-search-results-limit').hide();
                    $('.ll-search-results-sorting').hide();
                }

                var min_cost=Infinity, max_cost=-Infinity,
                    min_floor=Infinity, max_floor=-Infinity,
                    min_total_area=Infinity, max_total_area=-Infinity;
                var a=JSON.parse(data);
                var i=0;
                a.forEach(function(apartment)
                {
                    if(apartment.cost&&apartment.cost<min_cost) min_cost=apartment.cost;
                    if(apartment.special_price&&apartment.special_price<min_cost) min_cost=apartment.special_price;
                    if(apartment.cost&&apartment.cost>max_cost) max_cost=apartment.cost;
                    if(apartment.special_price&&apartment.special_price>max_cost) max_cost=apartment.special_price;

                    if(apartment.floor<min_floor) min_floor=apartment.floor;
                    if(apartment.floor>max_floor) max_floor=apartment.floor;

                    if(apartment.total_area<min_total_area) min_total_area=apartment.total_area;
                    if(apartment.total_area>max_total_area) max_total_area=apartment.total_area;
                });
                $('.ll-search-form__cost .ll-search-form__min-value').val(min_cost);
                $('.ll-search-form__cost .ll-search-form__max-value').val(max_cost);

                $('.ll-search-form__floor .ll-search-form__min-value').val(min_floor);
                $('.ll-search-form__floor .ll-search-form__max-value').val(max_floor);

                $('.ll-search-form__total-area .ll-search-form__min-value').val(min_total_area);
                $('.ll-search-form__total-area .ll-search-form__max-value').val(max_total_area);
            }
        });

    if (window.location.href.search('search') != -1) {
        setTimeout(paginationList, 3000);
        setTimeout(paginationGrid, 3000);
    }
}
// ---------------------------------------------------------------------------------------------------------------------
