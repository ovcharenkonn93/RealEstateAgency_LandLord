// Построение графика
function Chartist_Line() {
    console.log('Запустилась функция Chartist_Line');
    new Chartist.Line('#ct-chart-line', {
        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
        series: [
            [12, 9, 7, 9, 5],
            [2, 1, 3.5, 7, 3],
            [1, 3, 4, 5, 6]
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 40
        }
    });
}
// ---------------------------------------------------------------------------------------------------------------------