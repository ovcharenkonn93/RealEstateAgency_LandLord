// Обработка ориентира
function SearchFormDistrictChange()
{
    console.log('Запустилась функция SearchFormDistrictChange');

    $('select[name="ll-search-form__subdistrict"]').find('option').remove();
    $('select[name="ll-search-form__subdistrict"]').append('<option disabled="" selected="" value="">'+'Ориентир'+'</option>');
    $('.ll-search-form__subdistrict').show();

    var data_subdistrict = window.global_data;

    var district= $(".ll-search-form__district").val(); //Value из списка
    var city_id = $(".ll-search-form__city").val(); //Value из списка

    var list_subdistrict = [];

    for(var i=0;i<data_subdistrict.type_administrative_district.length; i++)
    {
        if((data_subdistrict.type_administrative_district[i].administrative_district==district)&&(data_subdistrict.type_administrative_district[i].id_city==city_id))
        {
            list_subdistrict.push(data_subdistrict.type_administrative_district[i]);
        }
    }

    if (list_subdistrict.length==1) { $('.ll-search-form__subdistrict').hide(); }
    for (i in list_subdistrict)
    {
        $('.ll-search-form__subdistrict').append('<option value="'+list_subdistrict[i].subdistrict+'">'+list_subdistrict[i].subdistrict+'</option>');
    }
}
// ---------------------------------------------------------------------------------------------------------------------