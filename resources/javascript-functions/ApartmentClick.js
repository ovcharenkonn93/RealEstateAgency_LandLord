// Выбор кнопки "Квартира"
function ApartmentClick () {
    console.log('Запустилась функция ApartmentClick - Выбор кнопки "Квартира"');

    $("#ll-search-form__estate-type_apartment").prop("checked", true);
    $('.ll-search-form__type-apartment').css('borderBottom','none');
    //$('.ll-search-form__type-apartment').css("border-bottom","none");
    $('.ll-search-form__type-land').css('borderBottom','solid 1px #DDDDDD');
    $('.ll-search-form__type-household').css('borderBottom','solid 1px #DDDDDD');

    $('.llg-for-rent').hide();
    $('.llg-for-land').hide();
    $('.llg-for-household').hide();
    $('.llg-for-appartment').show();
}
// ---------------------------------------------------------------------------------------------------------------------