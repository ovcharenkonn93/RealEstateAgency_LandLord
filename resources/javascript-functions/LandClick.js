// Выбор кнопки "Участок"
function LandClick () {
    console.log('Запустилась функция LandClick - Выбор кнопки "Участок"');

    $("#ll-search-form__estate-type_land").prop("checked", true);
    $('.ll-search-form__type-land').css('borderBottom','none');
    $('.ll-search-form__type-apartment').css('borderBottom','solid 1px #DDDDDD');
    $('.ll-search-form__type-household').css('borderBottom','solid 1px #DDDDDD');

    $('.llg-for-rent').hide();
    $('.llg-for-household').hide();
    $('.llg-for-appartment').hide();
    $('.llg-for-land').show();
    $.ajax(
        {
            url: 'api',
            type: 'GET',
            data: {
                type: 'search',
                format: 'plain',
                realestate_type: 'land'
            },
            success: function(data){
                var min=Infinity, max=-Infinity;
                var a=JSON.parse(data);
                a.forEach(function(land)
                {
                    if(land.cost&&land.cost<min) min=land.cost;
                    if(land.special_price&&land.special_price<min) min=land.special_price;
                    if(land.cost&&land.cost>max) max=land.cost;
                    if(land.special_price&&land.special_price>max) max=land.special_price;
                });
                $('.ll-search-form__cost .ll-search-form__min-value').val(min_cost);
                $('.ll-search-form__cost .ll-search-form__max-value').val(max_cost);
            }
        });
}
// ---------------------------------------------------------------------------------------------------------------------