// Чтение справочников и заполнение форм
function ReadReferences (page)
{
    console.log('Запустилась функция ReadReferences для чтения справочников на странице ', page);

    // Выбираем какие справочники считывать в зависимости от страницы
    switch (page) {
        case 'home': // Заглавная страница
            var tables = 'type_cities;type_profile';
            break;
        default:
            var tables = 'all';
            break;
    }
    console.log('Справочники: ', tables);

    $.ajax({
        url:'SearchForm',
        data:
        {
            type: 'reference',
            tables: tables,
            format: 'plain'
        },
        type: "GET",
        success: function (data) {
            if(data)
            {
                window.global_data = JSON.parse(data);

                // Выбираем какие поля заполнять в зависимости от страницы
                // Формат FillOptions (data, 'Имя таблицы','Имя поля','селектор CSS куда будут подставляться значения');
                switch (page) {
                    case 'home': // Заглавная страница
                        console.log('Заполняем формы на заглавной странице : ');
                        $('select[name="ll-search-form__city"]').find('option').remove();
                        FillOptions (window.global_data, 'type_cities','city','.ll-search-form__city');
                        FillOptions (window.global_data, 'reference_profiles','profile','.ll-search-form__commercial-type');
                        break;
                    default: // Временно, потом нужно разнести по страницам для оптимизации
                        console.log('Заполняем формы на других страницах : ');
                        //Очистка полей----------------------------------------------------------------------------------------------
                        $('select[name="ll-search-form__city"]').find('option').remove();
                        // $('select[name="ll-search-form__city"]').append('<option disabled="" selected="" value="">'+'Город'+'</option>');
                        $('select[name="ll-search-form__district"]').find('option').remove();
                        $('select[name="ll-search-form__subdistrict"]').find('option').remove();
                        //----------------------------------------------------------------------------------------------Очистка полей
                        FillOptions (window.global_data, 'type_cities','city','.ll-search-form__city');
						//window.global_data.type_cities.forEach(function (item, i, arr){
						//	$(".ll-search-form__city").append("<option value="+item.city+">"+item.city+"</option>");
						//})
                        FillOptions (window.global_data, 'reference_profiles','profile','.ll-search-form__commercial-type');
                        FillOptions (window.global_data, 'type_cities','city','.ll-sell-form__city-select');
                        FillOptions (window.global_data, 'reference_type_apartments','type_apartment','.ll-search-form__apartment');
                        FillOptions (window.global_data, 'reference_state','state','.ll-search-form__state');
                        FillOptions (window.global_data, 'reference_wall_material_apartments','wall_material','.ll-search-form__walls-apartment');
                        FillOptions (window.global_data, 'reference_wall_material_households','wall_material','.ll-search-form__walls-households');
                        FillOptions (window.global_data, 'reference_entries','entry','.ll-search-form__entry');
                        FillOptions (window.global_data, 'reference_furniture','furniture','.ll-search-form__furniture');
                        break;
                }
// Обработка района - начало -------------------------------------------------------------------------------------------

                var city_id = $(".ll-search-form__city").val(); //Value из списка

                var list_district = [];
                var j = 0;

                for(var i=0;i<window.global_data.type_administrative_district.length; i++)
                {
                    var added = 0;

                    if(window.global_data.type_administrative_district[i].id_city==city_id)
                    {
                        for(var k=0;k<list_district.length; k++)//есть ли уже такое значение
                        {
                            if(window.global_data.type_administrative_district[i].administrative_district==list_district[k].administrative_district)
                            {
                                added = 1;
                            }
                        }

                        if(added==0)
                        {
                            list_district.push(window.global_data.type_administrative_district[i]);
                            j++;
                        }
                    }
                }

                //FillOptions (data, 'type_administrative_district','district','.ll-search-form__district');
                //FillOptions (data, 'type_subdistrict','subdistrict','.ll-search-form__subdistrict');
                if (list_district.length<2) {$('.ll-search-form__district').hide();}
                for (i in list_district)
                {
                    $('.ll-search-form__district').append('<option value="'+list_district[i].administrative_district+'">'+list_district[i].administrative_district+'</option>');
                }
// Обработка района - конец --------------------------------------------------------------------------------------------
            }
        }
    });


}
// ---------------------------------------------------------------------------------------------------------------------