// Выбор кнопки "Дом"
function HouseholdClick () {
    console.log('Запустилась функция HouseholdClick - Выбор кнопки "Дом"');

    $("#ll-search-form__estate-type_household").prop("checked", true);
    $('.ll-search-form__type-household').css('borderBottom','none');
    $('.ll-search-form__type-apartment').css('borderBottom','solid 1px #DDDDDD');
    $('.ll-search-form__type-land').css('borderBottom','solid 1px #DDDDDD');
    $('.llg-for-rent').hide();
    $('.llg-for-land').hide();
    $('.llg-for-appartment').hide();
    $('.llg-for-household').show();
    $.ajax(
        {
            url: 'api',
            type: 'GET',
            data: {
                type: 'search',
                format: 'plain',
                realestate_type: 'households'
            },
            success: function(data){
                var min=Infinity, max=-Infinity;
                var a=JSON.parse(data);
                a.forEach(function(households)
                {
                    if(households.cost&&households.cost<min) min=households.cost;
                    if(households.special_price&&households.special_price<min) min=households.special_price;
                    if(households.cost&&households.cost>max) max=households.cost;
                    if(households.special_price&&households.special_price>max) max=households.special_price;
                });
                $('.ll-search-form__cost .ll-search-form__min-value').val(min_cost);
                $('.ll-search-form__cost .ll-search-form__max-value').val(max_cost);
            }
        });
}
// ---------------------------------------------------------------------------------------------------------------------