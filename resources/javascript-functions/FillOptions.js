// Заполняем select значениями из справочника
// FillOptions (data, 'Имя таблицы','Имя поля','селектор CSS куда будут подставляться значения')
function FillOptions (base, table, field, selector) {
    console.log('Запустилась функция FillOptions для заполнения ', selector, ' значениями из справочника');
    var element = base[table];
    $.each(element, function(index, value){
        var elementvalue = element[index];
        var datatokens='';
        if (selector=='.ll-search-form__city') {
            datatokens=' data-tokens="'+elementvalue[field]+'" ';
        }
        $(selector).append("<option value="+elementvalue["id"]+datatokens+">"+elementvalue[field]+"</option>");
    });
}
// ---------------------------------------------------------------------------------------------------------------------