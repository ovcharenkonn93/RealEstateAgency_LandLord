// Заполняем результаты поиска
// FillSearchResult (массив с объектами, селектор блока внутри которого будут вставляться данные, номер элемента массива)
function FillSearchResult (dataarray, selector) {
    console.log('Запустилась функция FillSearchResult для', selector);
    $(selector).show();

// Для универсальности выносим значения на верхний уровень
    try {dataarray.street = dataarray.address.street;} catch(error) {}
    try {dataarray.city = dataarray.address.type_district.city.city;} catch(error) {}
    try {dataarray.city = dataarray.address.type_administrative_district.city.city;} catch(error) {}
    try {dataarray.type_apartment = dataarray.type_apartment.type_apartment;} catch(error) {}
    try {dataarray.phone_mob = dataarray.employee_assigned.phone_mob;} catch(error) {}
    try {dataarray.employee_assigned_id = dataarray.employee_assigned.id;} catch(error) {}
    try {dataarray.employee_assigned_src = dataarray.employee_assigned.src;} catch(error) {}

// Заполняем стандартные текстовые блоки
    var textfields = ['name', 'surname', 'patronymic', 'city', 'district', 'district_admin', 'street', 	'floor',
        'floor_all', 'total_area', 'living_area', 'photo_count', 'text_website', 'phone_mob', 'type_apartment',
        'created_at', 'email', 'photo_count'];
    $.each(textfields, function(index, value){
        $(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).text(dataarray[value]);
		console.log(dataarray[value]);
    });

// Заполняем стандартные блоки изображений
    var host = 'http://ll1.dego1n.ru/';
    var imagefields = ['cover_src', 'employee_assigned_src', 'src'];
    $.each(imagefields, function(index, value){
        $(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).attr('src',host+dataarray[value]);
    });

// Заполняем стандартные блоки ссылок
    var linkfields = ['', ''];
    $.each(linkfields, function(index, value){
        $(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).attr("href",dataarray[value]);
    });

// Заполняем нестандартные блоки
    if ((dataarray.special_price == '') ||
        (dataarray.special_price == null) ||
        (dataarray.special_price == 0)) {
        $(selector).find('.ll-data__new-price').text(number_format(dataarray['cost'],0,'',' '));
        $(selector).find('.ll-data__old-price').text('');
        $(selector).find('.ll-estate-info__old-price').hide ();
        $(selector).find('.ll-search-result-list__old-price').hide ();
        $(selector).find('.ll-search-result-grid__old-price').hide ();
    } else {
        $(selector).find('.ll-data__new-price').text(number_format(dataarray['special_price'],0,'',' '));
        $(selector).find('.ll-data__old-price').text(number_format(dataarray['cost'],0,'',' '));
    }

    if ((dataarray.text_website == '') || (dataarray.text_website == null)) {
        $('.ll-estate-info__description').hide ();
    }

    $(selector).find('.ll-data__link').attr("href",'info/'+dataarray['id']);
    $(selector).find('.ll-data__agent-link').attr("href",'../agent-info/'+dataarray['employee_assigned_id']);

    $(selector).find('.add-to-favorites').attr("id",dataarray['id']);
    $(selector).find('.list-fa-star-o').attr("id",'fa-star-o'+dataarray['id']);
    $(selector).find('.grid-fa-star-o').attr("id",'grid-fa-star-o'+dataarray['id']);
    //star_color(); // Изменяем цвет звёздочки для избранного
//--------------Инициализация карты
    var latitude = 0;
    var longitude = 0;
    try {latitude = dataarray.address.latitude;} catch(error) {}
    try {longitude = dataarray.address.longitude;} catch(error) {}
    //if (latitude != 0) and (longitude != 0) {
    //console.log('Широта:',latitude);
    //console.log('Долгота:',longitude);
    //var myMap ;
    //ymaps.ready(init);
    //function init () {
    //myMap=new ymaps.Map("map", {
    //center: [latitude, longitude],
    //zoom: 7,
    //type: "yandex#map",
    // Карта будет создана без
    // элементов управления.
    //controls: []
    //});
    /* }, {
     searchControlProvider: 'yandex#search'
     });*/
    //}
    //}

// Ещё нужно заполнить такие блоки

//ll-estate-info__title
//ll-data__adress
//ll-data__city
//ll-estate-info__date-of-publication
$('#SearchRooms').selectpicker('refresh');
}
// ---------------------------------------------------------------------------------------------------------------------