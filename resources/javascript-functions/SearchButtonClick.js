﻿function SearchButtonClick ()
{
    console.log('Запустилась функция SearchButtonClick');

    var select_type = '&select_type=' + $("#ll-search-form-home__select-type").val();
    var city = '&city=' + $("#ll-search-form-home__select-city").val();
    var price = '&price=' + $("#ll-search-form-home__select-price").val();
    var rooms = '&rooms=';

    var deal_type = 'buy';
    var action = 'search';

    if(($("#ll-search-form-home__radio-rent").prop('checked')) || ($('#ll-search-form-home__radio-for-rent').prop('checked'))) {
        deal_type = 'rent';
    }
    if($("#ll-search-form-home__radio-sale").prop('checked')) {
        deal_type = 'sell';
    }
    if(($('#ll-search-form-home__radio-sale').prop('checked')) || ($('#ll-search-form-home__radio-for-rent').prop('checked'))) {
        action = 'sell';
    }

    if($("#ll-search-form-home__checkbox-1_room").prop('checked')) {
        rooms = rooms + 'one';
    }
    if($("#ll-search-form-home__checkbox-2_rooms").prop('checked')) {
        rooms = rooms + 'two';
    }
    if($("#ll-search-form-home__checkbox-3_rooms").prop('checked')) {
        rooms = rooms + 'three';
    }
    if($("#ll-search-form-home__checkbox-more_rooms").prop('checked')) {
        rooms = rooms + 'more';
    }

    if (rooms=='&rooms=') rooms='';
    if ((price=='&price=null') || (price=='&price=') || (price=='&price=0-') ) price='';
    if ((city=='&city=null') || (city=='&city=')) city='';
    if ((select_type=='&select_type=null') || (select_type=='&select_type=')) select_type='';

    location.href = action+'?deal_type='+deal_type+select_type+city+rooms+price;
}
// ---------------------------------------------------------------------------------------------------------------------