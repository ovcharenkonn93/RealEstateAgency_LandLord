// Получаем параметры из адресной строки и подставляем в форму
function GetParametersFromURL () {
    console.log('Запустилась функция GetParametersFromURL для обработки параметров адресной строки и изменения формы');

    $.urlParam = function(name){ //получить параметры из адресной строки
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }

    var rooms = '';
    var realestate_type_value = 'apartment';

    if (window.location.href.search('deal_type') != -1) { //если параметры поиска переданы
        if (window.location.href.search('city') != -1) {
            console.log('Меняем город. city =', $.urlParam('city'));
            $('#ll-search-form__city').get(0).selectedIndex = $.urlParam('city');
            $("#ll-search-form__city [value='"+$.urlParam('city')+"']").attr("selected", "selected");
        }
        if (window.location.href.search('price') != -1) {
            $('.ll-search-form__cost .ll-search-form__min-value').val(0);
            $('.ll-search-form__cost .ll-search-form__max-value').val($.urlParam('price'));
        }

        console.log('Нужно дописать обработку комнат - сделать мультиселект');
        if (window.location.href.search('rooms') != -1) {
            rooms = '&id_type_apartment='+$.urlParam('rooms');
            rooms = rooms.replace("one","5;6;7;");
            rooms = rooms.replace("two","8;9;");
            rooms = rooms.replace("three","10;");
            rooms = rooms.replace("more","11;12;");
        }

        if (window.location.href.search('select_type') != -1) {
            switch ($.urlParam('select_type')) {
                case '':
                    console.log('select_type не передавался');
                    break;
                case 'households':
                    HouseholdClick ();
                    break;
                case 'land':
                    LandClick ();
                    break;
                default:
                    ApartmentClick ();
                    break;
            }
        } else ApartmentClick ();

        if (window.location.href.search('deal_type') != -1) {
            switch ($.urlParam('deal_type')) {
                case '':
                    console.log('deal_type не передавался');
                    break;
                case 'rent':
                    RentClick ();
                    break;
                default:
                    SaleClick ();
                    break;
            }
        } else SaleClick ();
    } else // Если параметры поиска не переданы
    {
        ApartmentClick ();
        SaleClick ();
    }
}
// ---------------------------------------------------------------------------------------------------------------------