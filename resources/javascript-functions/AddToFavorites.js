// Добавление (удаление) в избранное
function AddToFavorites(id) {
    console.log('Запустилась функция AddToFavorites('+id+')');
    var favorites=JSON.parse($.cookie("favorites"));
	if(!favorites){
		$.cookie("favorites",JSON.stringify([id]));
		$("#fa-star-o"+id.toString()).removeClass("fa-star-o").addClass("fa-star  yellow-star");
	}
	else {
		pos=favorites.indexOf(id);
		if(pos==-1){
			favorites.push(id);
			$("#fa-star-o"+id.toString()).removeClass("fa-star-o").addClass("fa-star  yellow-star");
			$.cookie("favorites",JSON.stringify(favorites));
		}
		else {
			favorites.splice(pos,1);
			$("#fa-star-o"+id.toString()).removeClass("fa-star  yellow-star").addClass("fa-star-o");
			$.cookie("favorites",JSON.stringify(favorites));
		}
	}
}
// ---------------------------------------------------------------------------------------------------------------------