// Запуск заглавной страницы
function InitHomePage () {
    console.log('Запустилась функция InitHomePage');
    $('#ll-search-form-home__select-type').change(SearchFormEstateTypeChange);
    $(".llg-button-search").click (SearchButtonClick);

    //$('#ll-search-form-home__radio-sale').click (SwitchToSell);
    //$('#ll-search-form-home__radio-for-rent').click (SwitchToSell);
    //$('#ll-search-form-home__radio-buy').click (SwitchToSearch);
    //$('#ll-search-form-home__radio-rent').click (SwitchToSearch);
    //$('.llg-news-variant-1').click (NewsVariant1);
    //$('.llg-news-variant-2').click (NewsVariant2);
    //$('.llg-news-variant-3').click (NewsVariant3);
    //$('.llg-news-variant-4').click (NewsVariant4);

    ReadReferences ('home'); // Чтение справочников и заполнение форм
    ChangeVisualOnHomePage (); // Смена изображений на главной
}
// ---------------------------------------------------------------------------------------------------------------------