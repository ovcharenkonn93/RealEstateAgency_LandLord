// Запуск страницы шагов
function InitStepsPage () {
    console.log('Запустилась функция InitStepsPage');
    $('#carousel_banner').carouFredSel({
        synchronise: '#carousel_text',
        width: '100%',
        items: {
            visible: 3
        },
        scroll: {
            items: 1,
            duration: 1100,
            timeoutDuration: 7500
        },
        pagination: {
            container: '#carousel_pagination',
            anchorBuilder: function(nr) {
                return '<a id="step_'+nr+'" href="#"></a>';
            }
        },
        auto: {
            play:true
        },
        prev: '#prevCarou',
        next: '#nextCarou'
    });

    // Carousel for text
    $('#carousel_text').carouFredSel({
    auto: false
    });

//Since this carousel shows 3 slides at a time, it likes to put the #2 slide in the center, with the #1 slide on the left of the screen -
//The best way I found to correct this is to put the last slide first in the #carousel_banner div, followed by the actual #1 slide. -->

$('.ll-steps-to-buy-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-1.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-2.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-3.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-4.jpg';";
});
$('.ll-steps-to-buy-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-5.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-6.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-7.jpg');";
});
$('.ll-steps-to-buy-toggle-tab-8').bind('click', function (e) {
    $('#ll-steps-to-buy-toggle').style= "background-image: url('../images/visuals/steps-to-buy-8.jpg');";
});

$('.ll-steps-to-sell-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-1.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-2.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-3.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-4.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-5.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-6.jpg');";
});
$('.ll-steps-to-sell-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-sell-toggle').style= "background-image: url('../images/visuals/steps-to-sell-7.jpg');";
});

$('.ll-steps-to-rent-in-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-1.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-2.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-3.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-4.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-5.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-6.jpg');";
});
$('.ll-steps-to-rent-in-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-rent-in-toggle').style= "background-image: url('../images/visuals/steps-to-rent-in-7.jpg');";
});

$('.ll-steps-to-rent-out-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-1.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-2.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-3.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-4.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-5.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-6.jpg');";
});
$('.ll-steps-to-rent-out-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-rent-out-toggle').style= "background-image: url('../images/visuals/steps-to-rent-out-7.jpg');";
});

$('.ll-steps-to-documents-toggle-tab-1').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-1.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-2').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-2.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-3').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-3.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-4').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-4.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-5').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-5.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-6').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-6.jpg');";
});
$('.ll-steps-to-documents-toggle-tab-7').bind('click', function (e) {
    $('#ll-steps-to-documents-toggle').style= "background-image: url('../images/visuals/steps-to-documents-7.jpg');";
});

}
// ---------------------------------------------------------------------------------------------------------------------