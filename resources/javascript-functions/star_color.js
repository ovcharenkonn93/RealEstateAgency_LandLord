// Цвет звездочки
function star_color()
{
    console.log('Запустилась функция star_color');
    var order=$.cookie('favorites'); //получаем куки
    order ? order=JSON.parse(order): order=[]; //если заказ есть, то куки переделываем в массив с объектами
    var like;

    if(order.length>0)
    {
        $("#ll-favorites-star").removeClass("fa-star-o").addClass("fa-star  yellow-star");
    } else
    {
        $("#ll-favorites-star").removeClass("fa-star yellow-star").addClass("fa-star-o");
    }

    var ListIDs = [];
    $("#data-list").find(".add-to-favorites").each(function(){ ListIDs.push(this.id); });

    for(var i=0; i<ListIDs.length; i++){

        for(var j=0; j<order.length; j++) //перебираем массив в поисках наличия товара в корзине
        {
            if(order[j].item_id==ListIDs[i])
            {
                $("#fa-star-o"+ListIDs[i]).removeClass("fa-star-o").addClass("fa-star   yellow-star");
                $("#grid-fa-star-o"+ListIDs[i]).removeClass("fa-star-o").addClass("fa-star   yellow-star");
            }
        }
    }

    if (window.location.href.search('info') != -1) { //Звездочка на странице INFO

        for(var j=0; j<order.length; j++) //перебираем массив в поисках наличия товара в корзине
        {
            if(order[j].item_id==$(".add-to-favorites-info").attr('id'))
            {
                $("#info-fa-star-o"+$(".add-to-favorites-info").attr('id')).removeClass("fa-star-o").addClass("fa-star   yellow-star");
            }
        }
    }
}
// ---------------------------------------------------------------------------------------------------------------------
