// Запуск страницы аналитики
function InitAnalyticsPage () {
    console.log('Запустилась функция InitAnalyticsPage');
    ReadReferences ('analytics'); // Чтение справочников и заполнение форм
    Chartist_Line();
    Chartist_Pie();
    Chartist_Bar();
}
// ---------------------------------------------------------------------------------------------------------------------