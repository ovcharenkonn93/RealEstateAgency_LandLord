// Выбор типа недвижимости
function SearchFormEstateTypeChange () {
    console.log('Запустилась функция SearchFormEstateTypeChange для обработки типа недвижимости');

    var type = $('#ll-search-form-home__select-type').val();
    switch (type) {
        case 'commercial':
            $('.ll-search-form-home__commercial-type').show();

            $('.ll-search-form-home__rooms').hide();
            $('.ll-search-form-home__rooms-text').hide();
            break;
        case 'land':
            $('.ll-search-form-home__rooms').hide();
            $('.ll-search-form-home__rooms-text').hide();
            $('.ll-search-form-home__commercial-type').hide();
            break;
        default:
            $('.ll-search-form-home__rooms').show();
            $('.ll-search-form-home__rooms-text').show();

            $('.ll-search-form-home__commercial-type').hide();
            break;
    }
}
// ---------------------------------------------------------------------------------------------------------------------