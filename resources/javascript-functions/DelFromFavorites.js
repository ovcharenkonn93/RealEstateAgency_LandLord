// Удаление со страницы favorites
function DelFromFavorites(atf)
{
    console.log('Запустилась функция DelFromFavorites');
    string=$(this).parent();// выбираем всю строку в таблице
    item_id=parseInt(atf.id);  //получаем id товара

    string.remove();// удаляем строку

    order=JSON.parse($.cookie('favorites'));//получаем массив с объектами из куки
    for(var i=0;i<order.length; i++)
    {
        if(order[i].item_id==item_id)
        {
            order.splice(i,1); //удаляем из массива объект
        }
    }

    $.cookie('favorites', JSON.stringify(order), { expires: 7, path: '/'} );

    star_color();
}
// ---------------------------------------------------------------------------------------------------------------------