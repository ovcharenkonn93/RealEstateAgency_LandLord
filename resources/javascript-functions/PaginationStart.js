// Запускаем панинацию
// container - лок, в котором отрисовывается панинация
function PaginationStart(container, parent, child) {
    console.log('Запустилась функция PaginationStart для ', child);
    window.tp = new Pagination(container, {
        itemsCount: $(child).length, //количество видимых блоков
        onPageChange: function (paging) {
            var start = paging.pageSize * (paging.currentPage - 1),
                end = start + paging.pageSize,
                $rows = $(parent).find(child);
            $rows.hide();
            for (var i = start; i < end; i++) {
                $rows.eq(i).show();
            }
        }
    });
}
// ---------------------------------------------------------------------------------------------------------------------