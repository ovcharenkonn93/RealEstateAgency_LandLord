// Изменение минимального значения слайдера
function MinCostChange () {
    console.log('Запустилась функция MinCostChange');
    var value1=$(".ll-search-form__min-value").val();
    var value2=$(".ll-search-form__max-value").val();

    if(parseInt(value1) > parseInt(value2)){
        value1 = value2;
        $(".ll-search-form__min-value").val(value1);
    }
    $(this).siblings(".ll-search-form__slider").slider("values",0,value1);
}
// ---------------------------------------------------------------------------------------------------------------------