// Обработка района
function SearchFormCityChange()
{
    console.log('Запустилась функция SearchFormCityChange');

    $('select[name="ll-search-form__district"]').find('option').remove();
    $('select[name="ll-search-form__district"]').append('<option disabled="" selected="" value="">'+'Район'+'</option>');
    $('.ll-search-form__district').show();
    $('select[name="ll-search-form__subdistrict"]').find('option').remove();
    $('.ll-search-form__subdistrict').hide();
    //$('select[name="ll-search-form__subdistrict"]').append('<option disabled selected value="">'+'Ориентир'+'</option>');


    var data_district = window.global_data;

    var city_id = $(".ll-search-form__city").val(); //Value из списка

    var list_district = [];
    var j = 0;

    for(var i=0;i<data_district.type_administrative_district.length; i++)
    {
        var added = 0;

        if(data_district.type_administrative_district[i].id_city==city_id)
        {
            for(var k=0;k<list_district.length; k++)//есть ли уже такое значение
            {
                if(data_district.type_administrative_district[i].administrative_district==list_district[k].administrative_district)
                {
                    added = 1;
                }
            }

            if(added==0)
            {
                list_district.push(data_district.type_administrative_district[i]);
                j++;
            }
        }
    }

    //FillOptions (data, 'type_administrative_district','district','.ll-search-form__district');
    //FillOptions (data, 'type_subdistrict','subdistrict','.ll-search-form__subdistrict');
    if (list_district.length<2) {$('.ll-search-form__district').hide();}
    for (i in list_district)
    {
        $('.ll-search-form__district').append('<option value="'+list_district[i].administrative_district+'">'+list_district[i].administrative_district+'</option>');
    }
}
// ---------------------------------------------------------------------------------------------------------------------