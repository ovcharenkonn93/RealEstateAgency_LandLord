<div class="row">

	@if (Auth::guest())
		
				@yield('content')
					
					@else
						
					<div class="col-md-4 col-md-offset-4 text-center">
							
						<table class="formtable" class="text-center">

						   <tr>
							<td align="right" valign="top">Вы уже вошли в систему как <a href="#" >{{ Auth::user()->name }}</a></td>
						   </tr>

						   <tr>
							<td align="right" valign="top"><a href="auth/logout">Выход</a></td>
						   </tr>

						</table>
			
					</div>

					@endif


</div>


