@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__mobile-application">

        @include('layouts.blocks.ll-visual-in-header',['Page'=>'mobile-application','Header'=>'Удобное приложение  для поиска недвижимости!'])

    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-mobile-application')
        </div>
		<div class="row">
            @include('layouts.blocks.ll-mobile-application-features')
        </div>
    </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')