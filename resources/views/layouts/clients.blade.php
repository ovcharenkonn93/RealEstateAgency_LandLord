@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__clients">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'clients','Header'=>'Отзывы клиентов'])

    <div class="container">
    			<div class="row">
    					<div class="col-md-12" >
    						@include('layouts.blocks.ll-search-results-limit')
    					</div>
    			</div>

        <div class="row" id="data-list-clients">
            @include('layouts.blocks.ll-clients',['reviews'=>$reviews])
        </div>
    </div>
	<div class="row text-center">
		@include('layouts.blocks.ll-pagination')
	</div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')