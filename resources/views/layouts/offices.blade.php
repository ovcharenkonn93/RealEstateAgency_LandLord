@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="ll-offices container">
	<h1 class="text-center">Поиск офиса</h1>
	<div class="col-md-4 margin-top-20px">
		@include('layouts.blocks.ll-offices-left_col')
	</div>
	<script type="text/javascript">
	function select_office(id){
		$.ajax({
			url: "/offices/select",
			type: "GET",
			data: {id, id},
			success: function(response){
				data=JSON.parse(response);
				if(data.address){
					$(".ll-offices-left_col .address").html(data.address);
					$(".ll-offices-left_col .address-st").prop("hidden", false);
				}
				else{
					$(".ll-offices-left_col .address").html("");
					$(".ll-offices-left_col .address-st").prop("hidden", true);
				}
				if(data.telephone){
					$(".ll-offices-left_col .telephone").html(data.telephone);
					$(".ll-offices-left_col .telephone-st").prop("hidden", false);
				}
				else {
					$(".ll-offices-left_col .telephone").html("");
					$(".ll-offices-left_col .telephone-st").prop("hidden", true);
				}
				if(data.work_time){
					$(".ll-offices-left_col .work_time").html(data.work_time);
					$(".ll-offices-left_col .work_time-st").prop("hidden", false);
				}
				else {
					$(".ll-offices-left_col .work_time").html("");
					$(".ll-offices-left_col .work_time-st").prop("hidden", true);
				}
			}
		});
	}
	$("select.city").change(function(){
		$.ajax({
			url: "/offices/get",
			type: "GET",
			data: "id_city="+this.value,
			success: function(response){
				$(".ll-offices-list").html(response);
			}
		});
		$.ajax({
			url: "/offices/getmap",
			type: "GET",
			data: "id_city="+this.value,
			success: function(response){
				var data=JSON.parse(response);
				for(var i=0;i<marks.length;i++)
				{
					marks[i].setMap(null);
				}
				marks.length=0;
				data.forEach(function(coord){
					var mark=new google.maps.Marker({
					position: {lat: coord.latitude, lng: coord.longitude},
					map: map
					});
					mark.addListener("click", function(){
						select_office(coord.id);
					});
					latlngbounds.extend(mark.position);
					marks.push(mark);
				});
				map.setCenter( latlngbounds.getCenter(), map.fitBounds(latlngbounds));
			}
		})
	});
</script>
	<div class="col-md-8 margin-top-20px">
		@include('layouts.blocks.ll-offices-map')
	</div>
	<div class="col-md-12">
		@include('layouts.blocks.ll-offices-list')
	</div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')

