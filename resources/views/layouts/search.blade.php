@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-toggle llg-wrapper-toggle__search">
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-toggle')
        </div>
    </div>
</div>

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__search">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-5 col-xs-5 text-center " >
                 @include('layouts.blocks.ll-search-form')
            </div>
			
            <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7 text-center" >
				
				<div class="margin-top-30px">
					<div class="col-md-5" >
						@include('layouts.blocks.ll-search-results-limit')
					</div>
					<div class="col-md-7" >
						@include('layouts.blocks.ll-search-results-sorting')
					</div> 
				</div> 	
					

					@include('layouts.blocks.ll-search-result_empty')

				<div class="tab-content">
					<div id="llg-panel__result-list" class="tab-pane fade in active llg-panel">					
						<div class="data-list" id="data-list">						
						{{--@include('layouts.blocks.ll-search-result-list')--}}
						</div>

						<div class="row text-center">
							@include('layouts.blocks.ll-pagination')
						</div>
						
					</div>
					<div id="llg-panel__result-grid" class="tab-pane fade llg-panel">
						<div class="data-grid" id="data-grid">						
							@include('layouts.blocks.ll-search-result-grid')
						</div>
						
						<div class="row text-center">

						</div>
						
						<div class="ll-pagination" id="script-pagination-grid">
						</div>
						
					</div>
					<div id="llg-panel__result-map" class="tab-pane fade llg-panel">
						 @include('layouts.blocks.ll-search-result-map')
					</div>

				</div>
        </div>
		</div>
    </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')

	<script>
		$(document).ready(function(){
			/*left panel*/
			$("div.ll-search-form-panel_open").click(function(){
				$("div.ll-search-form").animate({left:'100%'},500);}, function() {
				$("div.ll-search-form").animate({left:0},500);
			});
		});
	</script>

  <script>
  	var searchElem = document.getElementById('ll-search-form');
    var searchSourceBottom = 200;//(searchElem.getBoundingClientRect().bottom) /3  + window.pageYOffset;

    window.onscroll = function() {
      if (searchElem.classList.contains('ll-search-form__fixed') && window.pageYOffset < searchSourceBottom) {
        searchElem.classList.remove('ll-search-form__fixed');
      } else if (window.pageYOffset > searchSourceBottom) {
        searchElem.classList.add('ll-search-form__fixed');
      }
    };
  </script>