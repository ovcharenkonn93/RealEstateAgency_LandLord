@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__steps-to-rent-in">

	<div class="ll-steps-to-rent-in-content">
		<div id="carousel">
			<div id="carousel_banner"><img alt="" src="{{ url('/images/visuals/steps-to-rent-in-8.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-rent-in-1.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-rent-in-2.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-rent-in-3.jpg') }}" />		         
				<img alt="" src="{{ url('/images/visuals/steps-to-rent-in-4.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-rent-in-5.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-rent-in-6.jpg') }}" />        
				<img alt="" src="{{ url('/images/visuals/steps-to-rent-in-7.jpg') }}" />
			</div>
		 
			<a title="Previous" id="prevCarou" href="#">&nbsp;</a>     
			<a title="Next" id="nextCarou" href="#">&nbsp;</a>
		</div>
		
		<!-- #carousel -->
		<div id="carousel_controls">
			<div id="carousel_overlay">
				<h1 class="red-text">8 шагов к аренде дома</h1>
				<div id="carousel_pagination">&nbsp;</div>
				<div class="clear_left">&nbsp;</div>
			</div>
		</div>
		<!-- #carousel_controls -->
		<div class="ll-steps-to-rent-in-content-item llg-columns-with-similar-height">
			<div class="ll-steps-to-rent-in-content-item_content">
				<div class="ll-steps-to-rent-in-content-main white-bg">
					@include('layouts.blocks.ll-steps-to-rent-in-kw-content')
				</div>
				
				<!-- main -->
				<div class="ll-steps-to-side llg-columns-with-similar-height">
					 @include('layouts.blocks.ll-steps-to-rent-in-buttons')
					 @include('layouts.blocks.ll-steps-why-rent-in-with-landlord')
					 @include('layouts.blocks.ll-steps-analitics')				 
				</div>

			</div>
		</div>
	</div>		

</div>

@include('layouts.footer')