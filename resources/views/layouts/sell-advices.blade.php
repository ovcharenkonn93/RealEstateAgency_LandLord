@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__sell-advices">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'sell-advices','Header'=>'Продажа Вами квартиры'])

    <div class="container">
        <div class="row">
            <div class="col-md-8 white-bg margin-top-20px">
                @include('layouts.blocks.ll-sell-advices')
            </div>
            <div class="col-md-4 margin-top-20px">
                    @include('layouts.blocks.ll-steps-to-sell-buttons')
                    @include('layouts.blocks.ll-steps-why-sell-with-landlord')
                    @include('layouts.blocks.ll-steps-analitics')
            </div>
        </div>
    </div>

</div>

@include('layouts.footer')