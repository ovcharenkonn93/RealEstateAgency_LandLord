@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__why-documents-with-landlord">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'why-documents-with-landlord','Header'=>'Зачем оформлять документы с ЛЕНДЛОРД?',
    'Description'=>'Мы помогаем покупателям оформить документы! Именно поэтому мы работаем с каждым клиентом индивидуально, принимая время на изучение их уникальный образ жизни, потребности и пожелания.'])

    <div class="container">
        <div class="row">
			<div class="col-md-8 margin-top-13px padding-left_right-0 margin-bottom-1em">
				@include('layouts.blocks.ll-why-documents-with-landlord')
			</div>
			<div class="col-md-4 margin-top-13px padding-right-0">
				@include('layouts.blocks.ll-steps-to-documents-buttons')
				@include('layouts.blocks.ll-why-documents-with-landlord-steps')
				{{--@include('layouts.blocks.ll-steps-analitics')--}}
			</div>
        </div>
		<div class="row">
            @include('layouts.blocks.ll-features')
        </div>
    </div>
</div>

@include('layouts.footer')