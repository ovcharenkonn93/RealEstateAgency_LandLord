@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__services">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'services','Header'=>'Услуги'])

    <div class="container">
        <div class="row">

            @include('layouts.blocks.ll-services')
        </div>
    </div>
</div>

@include('layouts.footer')