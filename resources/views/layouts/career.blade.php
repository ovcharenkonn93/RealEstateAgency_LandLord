@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__career">
    <div class="llg-career-variant-1">1</div>
    <div class="llg-career-variant-2">2</div>



    <script>
        function CareerVariant1 () {
            $('.ll-visual-in-header').removeClass('ll-visual-in-header__career-version-2');

        }
        function CareerVariant2 () {
            $('.ll-visual-in-header').addClass('ll-visual-in-header__career-version-2');

        }

        $('.llg-career-variant-1').click (CareerVariant1);
        $('.llg-career-variant-2').click (CareerVariant2);
    </script>


    @include('layouts.blocks.ll-visual-in-header',['Page'=>'career','Header'=>'Карьера',
    'Description'=>'Каждый, кто связывает с агентством недвижимости ЛЕНДЛОРД свои планы на будущее, определяет для себя, что такое ЛЕНДЛОРД лично для него.'])

	<div class="container">
		<div class="row">
			<div class="col-md-8 margin-top-13px">
					@include('layouts.blocks.ll-career-about')
			</div>
			<div class="col-md-4 margin-top-13px">
					@include('layouts.blocks.ll-career-button')
					@include('layouts.blocks.ll-about-awards')
			</div>
		</div>

		<div class="row ll-career">
			<div class="col-md-12">
					@include('layouts.blocks.ll-career-description')
			</div>
			<div class="col-md-12">
					@include('layouts.blocks.ll-vacancies_block')
		   </div>
		</div>
	</div>
</div>

@include('layouts.footer')