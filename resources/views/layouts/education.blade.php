@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__education">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'education','Header'=>'Учебный центр',
    'Description'=>'В нашей компании учатся все: от секретарей до топ-менеджеров. Это необходимо для того, чтобы стать настоящим профессионалом и добиться успеха.'])

	<div class="row">
		@include('layouts.blocks.ll-education-description')
	</div>
	{{--<div class="container">--}}
		{{--<div class="row">--}}
			{{--@include('layouts.blocks.ll-vacancies_block')--}}
		{{--</div>--}}
	{{--</div>   --}}
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')