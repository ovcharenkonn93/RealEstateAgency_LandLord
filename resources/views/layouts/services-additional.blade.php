@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__services-additional">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'services-additional','Header'=>'Юридические услуги'])

    <div class="container">
        <div class="row">
            <div class="col-md-8 white-bg margin-top-20px">
                @include('layouts.blocks.ll-services-select-list')
                @include('layouts.blocks.ll-services-additional-list')
                @include('layouts.blocks.ll-services-discounts')
                @include('layouts.blocks.ll-services-additional')
            </div>
            <div class="col-md-4 margin-top-20px">
                    @include('layouts.blocks.ll-services-buttons')
                    @include('layouts.blocks.ll-steps-why-documents-with-landlord')
                    @include('layouts.blocks.ll-steps-analitics')
            </div>
        </div>
    </div>

</div>

@include('layouts.footer')