<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ЛЕНДЛОРД — @yield('title')</title>

    <link href="{{ url('css/reset.css') }}" rel="stylesheet">
    <link href="{{ url('css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    {{--<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>--}}
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href="{{ url('css/animate.css') }}" rel="stylesheet">
    <link href="{{ url('css/landlord.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('css/bootstrap-select.min.css')}}">
	
    <!-- temporarily css style -->
    <link href="{{ url('css/style.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{{ url('js/jquery-3.0.0.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{{ url('js/markerclusterer.js') }}"></script>
    <script src="{{ url('js/bootstrap.js') }}"></script>
    <script src="{{ url('js/slider/highlight.pack.js') }}"></script>
    <script src="{{ url('js/slider/tabifier.js') }}"></script>
    <script src="{{ url('js/slider/js.js') }}"></script>
    <script src="{{ url('js/slider/jPages.js') }}"></script>
	<script src="{{ url('js/counter/jquery.knob.min.js') }}"></script>
    <script src="{{ url('js/jquery.cookie.js')}}"></script> <!-- Для работы с избранным -->
    <script src="{{ url('js/jquery-ui.js')}}"></script>
    <script src="{{ url('js/bootstrap-dropdown.js')}}"></script>
    <script src="{{ url('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="http://www.youtube.com/player_api"></script>  <!-- YouTube API, для остановки работы плееера -->
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>   <!-- Yandex Maps init  -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6-iNY-XFH_BePZQTxaga_Y8yEEaa3kfY&region=RU"></script>

	<script src="{{ url('js/landlord-functions.js')}}"></script>
	<script src="{{ url('js/landlord.js')}}"></script>
<!-- steps slider -->
    <script src="{{ url('js/steps/jquery.carouFredSel-6.2.1-packed.js') }}"></script>
    <script src="{{ url('js/steps/animatedcollapse.js') }}"></script>
<!-- steps slider -->	

<!-- pagination -->
	<script src="{{ url('js/pagination.js')}}"></script>
<!-- pagination -->

<!-- rotate_slider  -->
    <script src="{{ url('js/rotate_slider/app.js') }}"></script>
    <script src="{{ url('js/rotate_slider/jquery.rotateSlider.js') }}"></script>
<!-- End references for rotate_slider -->

<!-- analytics -->
<link href="{{ url('css/chartist.css') }}" rel="stylesheet">
<script src="{{ url('js/chartist.js')}}"></script>	
<!-- analytics -->
</head>

<body>

{{--@include('layouts.blocks.ll-callback-hunter')--}}
{{--<div class="llg-grey-red-skin">&nbsp;</div>--}}
{{--<div class="llg-brown-skin">&nbsp;</div>--}}

@include('layouts.blocks.ll-menu-panel-left')

<div class="llg-wrapper llg-wrapper-header">
    <div class="container">		
	@include('layouts.blocks.ll-header-user')
		<div class="col-md-12">
			<div class="row">
				@include('layouts.blocks.ll-header-links')
			</div>
			<div class="row">
				<div id="left_side-of-header_menu" class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
					@include('layouts.blocks.ll-logo')
				</div>

				<div id="right_side-of-header_menu" class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					@include('layouts.blocks.ll-header-menu')
				</div>
			</div>	
		</div>	

    </div>
</div>




  


