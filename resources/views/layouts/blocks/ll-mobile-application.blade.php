<div class="ll-mobile-application">
	<div class="ll-mobile-application__50-left">
		<h1 class="text-center red-text">Приложение ЛЕНДЛОРД</h1>
		<div class="row">
			<div id="lbl_left" class="ll-mobile-application__25-left"><h3 id="IPhone_lbl_top" class="text-center">Скачать для IPhone и IPad</h3></div>
			<div id="lbl_right" class="ll-mobile-application__25-right"><h3 class="text-center">Скачать для Android</h3></div>
		</div>
		<div class="row">
			<div class="lg_devices">
				<div class="ll-mobile-application__25-left"><a id="IOS_btn" href="#"><img src="{{ url('/images/interface/appStore.png') }}" alt="Скачать для IPhone и IPad" title="Скачать для IPhone и IPad"></a></div>
				<div class="ll-mobile-application__25-right"><a id="Android_btn" href="#"><img src="{{ url('/images/interface/google.png') }}" alt="Скачать для Android" title="Скачать для Android"></a></div>
			</div>

			<div class="mid_dev">
				<div class="ll-mobile-application__25-left"><a class="IPhone_btn_bottom" href="#"><img src="{{ url('/images/interface/app-store-small.png') }}" alt="Скачать для IPhone и IPad" title="Скачать для IPhone и IPad"></a></div>
				<div class="ll-mobile-application__25-right"><a href="#"><img src="{{ url('/images/interface/google-small.png') }}" alt="Скачать для Android" title="Скачать для Android"></a></div>
			</div>

			<div class="xs_dev">
				<a class="IPhone_btn_bottom" href="#"><img src="{{ url('/images/interface/app-store-small.png') }}" alt="Скачать для IPhone и IPad" title="Скачать для IPhone и IPad"></a>
				<a href="#"><img src="{{ url('/images/interface/google-small.png') }}" alt="Скачать для Android" title="Скачать для Android"></a>
			</div>
		</div>
	</div>
	<div class="ll-mobile-application__50-right">
			<p>	<i class="font-style-italic">«Я люблю приложение ЛЕНДЛОРД! Мне удалось
		 найти дом своей мечты недалеко от города во
		 время путешествия. ЛЕНДЛОРД также имеет
		 доступ ко всем домам на МЛС ... другие услуги
		 опробованы мною, предлагают только
		 минимальные прейскуранты.»</i>
		<div class="ll-mobile-application__name">Обозреватель ЛЕНДЛОРД приложения</div>
		<br><br><i class="font-style-italic">«Лучшее приложение в сфере недвижимости.
		 Я могу искать недвижимость по всей Ростовской
		 области. Удивительно! Приложение, которое
		 сразу показывает результаты без
		 необходимости нажимать много кнопок.»</i>
		<div class="ll-mobile-application__name">Обозреватель ЛЕНДЛОРД приложения</div>
	</div>
</div>