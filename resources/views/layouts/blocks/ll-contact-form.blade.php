<a class="llg-button llg-button-for-contact-form">{{ $ButtonName }}</a>

<div class="ll-agent-contact-form-overlay" >
	<div class="ll-agent-contact-form-table">
		<div class="ll-agent-contact-form-cell">
			<div class="ll-agent-contact-form-modal">
				<a href="#close" class="close-modal"></a>
				<h3 class="ll-agent-contact-form-h3 text-center red-text">Свяжитесь со мной {{ $ContactType }} {{ $ContactId }}</h3>
				<form action="sendmessage" method="POST">
				    @yield('services-select-list')
					<input name="name" class="ll-agent-contact-form__select-45-left" placeholder="Ваше имя" required=""/>
					<input name="email" class="ll-agent-contact-form__select-45-right" placeholder="Ваш e-mail" required=""/>
					<textarea name="message" class="ll-agent-contact-form__select-100" placeholder="Ваше сообщение" rows="10"></textarea>
					<input type="submit" value="Отправить">
				</form>
			</div>
		</div>
	</div>
</div>

<!--
<div class="ll-agent-contact-form-content">
	<h3 class="text-center red-text">Свяжитесь со мной {{ $ContactType }} {{ $ContactId }}</h3>

	<form action="" method="POST">
		<input name="name" class="ll-agent-contact-form__select-45-left" placeholder="Ваше имя" required=""/>
		<input name="email" class="ll-agent-contact-form__select-45-right" placeholder="Ваш e-mail" required=""/>
		<textarea name="message" class="ll-agent-contact-form__select-100" placeholder="Ваше сообщение" rows="10"></textarea>
		<input type="submit" value="Отправить">
	</form>
</div>
<div class="ll-agent-contact-form-overlay-black"></div>-->

<script>

</script>