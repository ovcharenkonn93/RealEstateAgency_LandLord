<div class="ll-vacancy">
	<div class="col-md-12 margin-top-50px ll-vacancy-first_block">
		<div class="ll-vacancy-header">	
			<h2 class="red-text">{{ $MainHeader }}</h2>
		</div>
		<div class="ll-vacancy-img">	
			<img src={{ url('images/visuals/') }}/{{ $ImgLink }} class="ll-history-rightimg" alt="">
		</div>	
		<div class="ll-vacancy-main_text">		
			<div>@yield('MainText')</div>
		</div>						
	</div>
	
	<div class="col-md-12 margin-top-20px ll-vacancy-second_block">
		<div class="col-md-12">		
			<div>@yield('Text2')</div>
		</div>
		<div class="col-md-6">		
			<div>@yield('Text3')</div>
		</div>
		<div class="col-md-6">		
			<div>@yield('Text4')</div>
		</div>	
		<div class="col-md-12 ll-vacancy-office_list">				
			<div>@yield('Text5')</div>
		</div>		
	</div>
</div>