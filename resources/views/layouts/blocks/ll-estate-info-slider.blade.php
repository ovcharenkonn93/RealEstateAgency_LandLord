<div class="ll-estate-info-slider">		

  <div id="content" class="defaults">

        <!-- big image ll-data__cover-src-->
        <div id="img" class="animated"><img class="ll-data__cover-src" src="<?php 
		try{
			$link=explode("/",$estate->cover_src,2)[1];
			if(file_exists($_SERVER['DOCUMENT_ROOT']."/".$link)){
				echo url($link);
			}
			else echo url('/images/placeholders/no-photo-estate.png');
		}
		catch(Exception $e){
			echo url('/images/placeholders/no-photo-estate.png');
		}
		?>"></div>

        <!-- item container -->
        <ul id="thumbs" class="thumbs ll-estate-info-slider-list">
			<li class="selected"><img src="" alt=""></li>
			<li><img src="" alt=""></li>
			<li><img src="" alt=""></li>
			<li><img src="" alt=""></li>
			<li><img src="" alt=""></li>
			<li><img src="" alt=""></li>
			<li><img src="" alt=""></li>
			<li><img src="" alt=""></li>
			<li><img src="" alt=""></li>
			<li><img src="" alt=""></li>
		</ul>

        <!-- navigation holder -->
        <div class="holder"></div>

        <!-- custom buttons -->
        <div id="btns">
            <span class="prev fa fa-angle-left fa-3x red-text"></span>
            <span class="next fa fa-angle-right fa-3x red-text"></span>
        </div>	

  </div> <!--! end of #content -->

</div>


  <script>
  /*
  $(function() {

  var carousel_img = 4;

	if (window.screen.width<980) { carousel_img = 1; }
	  
    /* initiate plugin */
    /*$("div.holder").jPages({
      containerID : "thumbs",
      perPage     : carousel_img,
      previous    : ".prev",
      next        : ".next",
      links       : "blank",
      direction   : "auto",
      animation   : "fadeInUp"
    });

    $("ul#thumbs li").click(function(){
      $(this).addClass("selected")
      .siblings()
      .removeClass("selected");

      var img = $(this).children().clone().addClass("animated fadeInDown");
      $("div#img").html( img );

    });

  });*/
  </script>

