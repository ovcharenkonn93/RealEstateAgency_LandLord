<div class="ll-visual-in-header ll-visual-in-header__{{ $Page }}">
    <div class="container">
        <div class="ll-visual-in-header__content">
            <h1>{{ $Header }}</h1>
            <p>{{ $Description or '' }}</p>
        </div>
    </div>
</div>