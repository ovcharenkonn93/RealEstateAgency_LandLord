<div class="ll-agent-card" id="ll-agent-card{{ $agentInfoId }}">
	<div class="ll-agent-card-photo">
		<img class="ll-data__src" src="{{ url('images/placeholders/no-photo-person.png') }}" alt="" title="">
	</div>
	
	<div class="ll-agent-card-header">
	
		<div class="ll-agent-card-name">
			<h4 class="text-center red-text"><span class="ll-data__name"><!-- Имя --></span> <span class="ll-data__patronymic"><!-- Фамилия --></span> <span class="ll-data__surname"><!-- Отчество --></span></h4>
		</div>

	 <!--
	 	<div class="ll-agent-card-social-buttons">
			<ul>
				<li><a href="#"><i class="fa fa-instagram"> </i></a></li>
				<li><a href="#"><i class="fa fa-vk"> </i></a></li>
				<li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter"> </i></a></li>
			</ul>
		</div>
-->
		
	</div>
	

	<div class="ll-agent-card-contact">
		<div class="ll-agent-card-links">
			<ul>
			<li><a href="#"><i class="fa fa-2x fa-whatsapp"> </i></a> <span class="ll-data__phone-office"><!-- Офисный телефон --></span></li>
			<li><a href="#"><i class="fa fa-2x fa-whatsapp"> </i></a> <span class="ll-data__phone-mob"><!-- Корпоративный сотовый телефон --></span></li>
			<li><a href="#"><i class="fa fa-2x fa-skype"> </i></a> <span class="ll-data__skype"><!-- Корпоративный skype --></span></li>
			<li><a href="#"><i class="fa fa-2x fa-envelope-o"> </i></a>   <span class="ll-data__email"><!-- Корпоративный электронный адрес --></span></li>
			</ul>
		</div>
		<hr>
		
		<div class="ll-agent-card-contact-me1">			
			@include('layouts.blocks.ll-contact-form',['ContactType'=>'agent','ContactId'=>$agentInfoId,'ButtonName'=>'Связаться со мной'])
		</div>	
	</div>
	
	<div class="ll-agent-card-info">
			Обо мне:
			Lorem ipsum dolor sit amet, consectetur adipiscing 
			elit,sed do eiusmod tempor incididunt ut labore et 
			dolore magnaaliqua. Ut enim ad minim veniam, quis 
			nostrud exercitation ullamco laboris nisi ut aliquip
			ex ea commodo consequat. 
			Duis aute irure dolor in reprehenderit in voluptate 
			velit esse cillum dolore eu fugiat nulla pariatur. 
			Excepteur sint occaecat cupidatat non proident,
			sunt in culpa qui officia deserunt mollit anim id 
			est laborum.
	</div>	
</div>
