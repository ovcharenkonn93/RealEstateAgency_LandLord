<div class="ll-ipoteka-calculator">
    <div class="ll-ipoteka-calculator__body">
        <div class="ll-ipoteka-calculator__title">Калькулятор ипотеки</div>

        <div class="ll-ipoteka-calculator__content">
            <div class="content_sm">
                <!--<select name="bank-select" id="" class="ll-ipoteka-calculator-select">

                    <option value="">
                        Выбор банка
                    </option>
                    <option value="">
                        ВТБ
                    </option>
                    <option value="">
                        СберБанк
                    </option>

                </select>-->
                <div class="content_sm-label">
                    <h3 class="content_sm-h3"> <span id="installment_per_month">0</span> РУБ</h3>
                    <h4 class="content_sm-h4">В месяц</h4>
                </div>
            </div>
            <div class="content_big">
				<label for="percent">Годовая ставка:   <span class="ll-res-input_percent" id="res-percent">0</span>%</label>
                <input type="range" id="percent" min="0" max="100" oninput="ch()">
				
                <label for="initial_installment">Первоначальный взнос:   <span class="ll-res-input_range" id="res-initial_installment">0</span> руб</label>
                <input type="range" id="initial_installment" min="0" max="{{$estate->price}}" oninput="ch()">

                <div class="content_big-range_block">
                    <label for="credit_term">Срок кредита:   <span class="ll-res-input_range" id="res-credit_term">0</span> </label>
                    <input type="range" id="credit_term" min="1" max="480" oninput="ch()">
                </div>
            </div>
        </div>
    </div>
    <div class="ll-ipoteka-calculator__button">
        <a href="#" class="llg-button">Отправить заявку</a>
    </div>
</div>
<script type="text/javascript">
	function calc(percent,initial_installment, term){
		var N=term;
		var P=percent/1200;
		var S={{$estate->price}}-initial_installment;
		//var S=100000;
		//var installment=S*(P+(P/((1+P)**N-1));
		var installment;
		if(P>0)
		{
			installment=(S*(P+(P/(Math.pow(1+P,N)-1)))).toFixed(2);
		}
		else
		{
			installment=S/N;
		}
		$("#installment_per_month").html(installment);	
	}
	function ch(){
		var percent=$("#percent").val();
		var initial_installment=$("#initial_installment").val();
		var credit_term=$("#credit_term").val();
		var year=Math.floor(credit_term/12);
		var month=credit_term%12;
		var str="";
		switch(year%10){
			case 0:
			break;
			case 1:
			str+="1 год ";
			break;
			case 2:
			case 3:
			case 4:
			str+=year+" года ";
			break;
			default:
			str+=year+" лет ";
			break;			
		}
		switch(month%12){
			case 0:
			break;
			case 1:
			str+="1 месяц";
			break;
			case 2:
			case 3:
			case 4:
			str+=month+" месяца";
			break;
			default:
			str+=month+" месяцев";
			break;			
		}
		$("#res-percent").html(percent);
		$("#res-initial_installment").html(initial_installment);
		$("#res-credit_term").html(str);
		calc(percent,initial_installment,credit_term);
	}
	ch();
</script>
