<div class="ll-structure">
	<h1 class="text-center">Отделы компании</h1>
	<hr>
	<p>Помимо тех отделов, которые занимаются непосредственно профильной деятельностью агентства, существуют еще и подразделения, без которых было бы немыслимо существование такой большой компании, как наша и которые обеспечивают бесперебойную работу агентства, а именно:</p>

	<div class="row">
	<div class="col-md-6 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-hr.png') }}" alt="" title="">
					</td>
					<td>
						<h3>Отдел кадров</h3>
						<p>Выполняет работу по комплектованию организации кадрами требуемых профессий, специальностей и квалификации. </p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div>  
	<div class="col-md-6 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-marketing-and-ads.png') }}" alt="" title="">
					</td>
					<td>
						<h3>Отдел маркетинга и рекламы</h3>
						<p>Определяет место компании на рынке, занимаясь поиском возможностей для достижения более выгодных позиций относительно конкурентов.  </p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div>
	</div>

	<div class="row">
	<div class="col-md-6 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-law-department.png') }}" alt="" title="">
					</td>
					<td>
						<h3>Юридический отдел</h3>
						<p>Соблюдение деятельности организации в соответствии с существующим законодательством.  </p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div> 
	<div class="col-md-6 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-accounting.png') }}" alt="" title="">
					</td>
					<td>
						<h3>Бухгалтерия</h3>
						<p>Сбор и обработка полной и достоверной информации о деятельности хозяйствующего субъекта. </p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div>
	</div>

	<div class="row">
	<div class="col-md-6 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-office-staff.png') }}" alt="" title="">
					</td>
					<td>
						<h3>Служба персонала</h3>
						<p>Специализированное подразделение организации, осуществляющее функции по управлению персоналом в организации.  </p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div> 
	<div class="col-md-6 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-it.png') }}" alt="" title="">
					</td>
					<td>
						<h3>IT-отдел</h3>
						<p>Обеспечение работоспособности информационных систем. </p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div>
	</div>

	<div class="row">
	<div class="col-md-6 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-secretariat.png') }}" alt="" title="">
					</td>
					<td>
						<h3>Секретариат</h3>
						<p>Подразделение компании, которое занимается общим администрированием, делопроизводством и документооборотом.</p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div> 
	<div class="col-md-6 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-quality.png') }}" alt="" title="">
					</td>
					<td>
						<h3>Служба контроля качества</h3>
						<p>Структура, включающая взаимодействующий управленческий персонал, реализующий функции управления качеством установленными методами.</p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div>
	</div>

	<div class="col-md-12 ll-structure-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-legal-registration.png') }}" alt="" title="">
					</td>
					<td>
						<h3>Служба юридического оформления</h3>
						<p>Юридические услуги по оформлению недвижимости и сопровождению сделок с недвижимым имуществом.</p>
						<p><a href="vacancies">Просмотреть вакансии </a>
					</td>
				</tr>
			</table>
	</div> 
    
</div>