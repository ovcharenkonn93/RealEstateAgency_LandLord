<div class="ll-steps-why-buy-with-landlord">
    <h2>Зачем покупать с ЛЕНДЛОРД?</h2>
    <img src="images/visuals/steps-why-buy-with-landlord.jpg" alt=""  title="">
    <p class="margin-top-20px">Мы помогаем покупателям найти дом своей мечты! Именно поэтому мы работаем с каждым клиентом индивидуально, принимая время на изучение их уникальный образ жизни, потребности и пожелания.
    <p><a href="why-buy-with-landlord" class="llg-button-detail">Подробнее</a>
</div>