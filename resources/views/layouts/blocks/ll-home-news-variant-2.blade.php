<div class="ll-home-news-variant-2 col-md-12">

@foreach($news['news'] as $new)
<a href="{{url("/news/$new->id")}}">
    <div class="col-md-4">
        <div class="ll-home-news-variant-2__block">
            <p class="ll-home-news-variant-2__caption"><strong>{{$new->name}}</strong></p>
            <div class="ll-home-news-variant-2__photo">
                <img src="{{ url('/images/placeholders') }}/news{{$new->id}}.jpg" alt="" title="" >
            </div>
            <div class="ll-home-news-variant-2__news-text">
                <p>{{$new->description}}</p>
            </div>
        </div>
    </div>
</a>
  <!--  <div class="col-md-4">
        <div class="ll-home-news-variant-2__block">
            <p class="ll-home-news-variant-2__caption"><strong>День отличника</strong></p>
            <div class="ll-home-news-variant-2__photo">
                <img src="{{ url('/images/placeholders') }}/news0.jpg" alt="" title="">
            </div>
            <div class="ll-home-news-variant-2__news-text">
                <p>	21 июня в нашем учебном центре прошел «День отличника» для детей сотрудников, посвященный успешному окончанию учебного года!
                21 июня в нашем учебном центре прошел «День отличника» для детей сотрудников, посвященный успешному окончанию учебного года! </p>
            </div>
            <a class="ll-home-news-variant-2__more" href="{{ url('/news') }}">Подробнее</a>
        </div>
    </div>

    <div class="col-md-4">
        <div class="ll-home-news-variant-2__block">
            <p class="ll-home-news-variant-2__caption"><strong>Tуры по новостройкам</strong></p>
            <div class="ll-home-news-variant-2__photo">
                <img src="{{ url('/images/placeholders') }}/news6.jpg" alt="" title="">
            </div>
            <div class="ll-home-news-variant-2__news-text">
                <p>	Как провести июньские праздники с максимальной пользой? Правильный ответ на этот вопрос нашли участники 12-го автобусного тура по новостройкам
                Как провести июньские праздники с максимальной пользой? Правильный ответ на этот вопрос нашли участники 12-го автобусного тура по новостройкам</p>
            </div>
            <a class="ll-home-news-variant-2__more" href="{{ url('/news') }}">Подробнее</a>
        </div>
    </div> -->
	
@endforeach	

</div>

