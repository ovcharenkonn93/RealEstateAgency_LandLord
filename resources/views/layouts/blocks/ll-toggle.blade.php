{{--В landlord.js нужно указать страницы на которых будет подсвечиваться первый пункт --}}
<div class="ll-toggle">
    <ul class="nav nav-tabs">
		<li class="llg-for__search"><a data-toggle="tab" href="#llg-panel__result-list" class="ll-toggle__tabs-list"><i class="fa fa-list-ul"></i> Список</a></li>
        <li class="llg-for__search"><a data-toggle="tab" href="#llg-panel__result-grid" class="ll-toggle_tabs-grid"><i class="fa fa-th"></i> Таблица</a></li>
        <li class="llg-for__search"><a data-toggle="tab" href="#llg-panel__result-map" class="ll-toggle__tabs-map"><i class="fa fa-map-marker"></i> Карта</a></li>
        <li class="llg-for__info"><a data-toggle="tab" href="#"><i class="fa fa-info"></i> Информация о квартире</a></li>
        <li class="llg-for__favorites llg-for__info"><a href="{{ url('/search') }}"><i class="fa fa-search"></i> Поиск квартиры</a></li>
        <li class="llg-for__agent-info"><a data-toggle="tab" href="#"><i class="fa fa-info"></i> Информация об агенте</a></li>
        <li class="llg-for__agent-info"><a href="{{ url('/agents') }}"><i class="fa fa-user"></i> Поиск агентов</a></li>
        <li class="llg-for__user-page"><a data-toggle="tab" href="#llg-panel__profile" class="ll-toggle__tabs-profile"><i class="fa fa-cog"></i> Профиль</a></li>
        <li class="llg-for__user-page"><a data-toggle="tab" href="#llg-panel__my-ads" class="ll-toggle__tabs-my-ads"><i class="fa fa-list-ul"></i> Мои объявления</a></li>
        <li class="llg-for__services-list"><a data-toggle="tab" href="#llg-panel__main-list" class="ll-toggle__tabs-main-list"><i class="fa fa-list-ul"></i> Оформление прав собственности на недвижимость</a></li>
        <li class="llg-for__services-list"><a data-toggle="tab" href="#llg-panel__documents-list" class="ll-toggle__tabs-documents-list"><i class="fa fa-list-ul"></i> Договорные услуги</a></li>
        <li class="llg-for__all"><a href="{{ url('/favorites') }}" class="ll-toggle__tabs-chosen"><i class="fa fa-star-o" id="ll-favorites-star"></i> Избранное</a></li>
    </ul>
</div>