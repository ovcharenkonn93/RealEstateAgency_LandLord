<div class="ll-vacancies_block">
	<h1 class="text-center">Наши вакансии</h1>
	<div class="col-md-12">
		<div class="col-md-4 text-center">
			<div class="ll-vacancies_block-img">
				<a href="{{ url('vacancy-realtor') }}"><img src="{{ url('images/visuals/vacancy-realtor.jpg') }}" alt=""></a>
			</div>
			<h4 class="text-center"><a href="{{ url('vacancy-realtor') }}">Агент по продаже недвижимости</a></h4>
		</div>
		<div class="col-md-4 text-center">
			<div class="ll-vacancies_block-img">
				<a href="{{ url('vacancy-secretary') }}"><img src="{{ url('images/visuals/vacancy-secretary.jpg') }}" alt=""></a>
			</div>
			<h4 class="text-center"><a href="{{ url('vacancy-secretary') }}">Офис-менеджер (секретарь)</a></h4>
		</div>
		<div class="col-md-4 text-center">
			<div class="ll-vacancies_block-img">
				<a href="{{ url('vacancy-realtor-commercial') }}"><img src="{{ url('images/visuals/vacancy-realtor-commercial.jpg') }}" alt=""></a>
			</div>
			<h4 class="text-center"><a href="{{ url('vacancy-realtor-commercial') }}">Агент по коммерческой недвижимости</a></h4>
		</div>
	</div>
	<div class="text-center">
		<a href="#" class="llg-button">Присоединиться к нам</a>
	</div>
</div>

