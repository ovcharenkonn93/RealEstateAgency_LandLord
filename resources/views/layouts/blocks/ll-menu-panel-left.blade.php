<div class="ll-menu-panel-left">
	<a class="ll-menu-panel-trigger" href="#"><i class="fa fa-reorder"></i></a>
	<div class="ll-menu-panel-left-user">
			@include('layouts.blocks.ll-header-user')
	</div>	
	<div class="ll-menu-panel-left-list">
		  <div class="list-group panel">
			<a href="#demo1" class="list-group-item list-group-item-success" data-toggle="collapse">Покупка</a>
			<div class="collapse" id="demo1">
				<a href="{{ url('/buy-advices') }}" class="list-group-item" data-parent="#SubMenu1">Советы при покупке</a>
				<a href="{{ url('/steps-to-buy') }}" class="list-group-item" data-parent="#SubMenu1">8 шагов к покупке недвижимости</a>				
				<a href="{{ url('/why-buy-with-landlord') }}" class="list-group-item" data-parent="#SubMenu1">Зачем покупать с ЛЕНДЛОРД?</a>
				<a href="{{ url('/mobile-application') }}" class="list-group-item" data-parent="#SubMenu1">Мобильное приложение</a>
				<a href="{{ url('/analytics') }}" class="list-group-item" data-parent="#SubMenu1">Исследование рынка</a>				
			</div>
			<a href="#demo2" class="list-group-item list-group-item-success" data-toggle="collapse">Продажа</a>
			<div class="collapse" id="demo2">
				<a href="{{ url('/sell-advices') }}" class="list-group-item" data-parent="#SubMenu1">Советы при продаже</a>
				<a href="{{ url('/steps-to-sell') }}" class="list-group-item" data-parent="#SubMenu1">8 шагов к продаже недвижимости</a>
				<a href="{{ url('/why-sell-with-landlord') }}" class="list-group-item" data-parent="#SubMenu1">Зачем продавать с ЛЕНДЛОРД?</a>
				<a href="{{ url('/mobile-application') }}" class="list-group-item" data-parent="#SubMenu1">Мобильное приложение</a>
				<a href="{{ url('/analytics') }}" class="list-group-item" data-parent="#SubMenu1">Исследование рынка</a>			
			</div>

			<a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse">Услуги</a>
			<div class="collapse" id="demo3">
				  <a href="{{ url('/services') }}" class="list-group-item" data-parent="#SubMenu1">Услуги</a>
				  {{--<a href="{{ url('/mortgage') }}" class="list-group-item" data-parent="#SubMenu1">Ипотека</a>--}}
                  <a href="{{ url('/services-for-buyer') }}" class="list-group-item" data-parent="#SubMenu1">Покупателю</a>
                  <a href="{{ url('/services-for-seller') }}" class="list-group-item" data-parent="#SubMenu1">Продавцу</a>
                  <a href="{{ url('/services-for-tenant') }}" class="list-group-item" data-parent="#SubMenu1">Арендатору</a>
                  <a href="{{ url('/services-for-owner') }}" class="list-group-item" data-parent="#SubMenu1">Арендодателю</a>

			</div>

			<a href="#demo4" class="list-group-item list-group-item-success" data-toggle="collapse">О нас</a>
			<div class="collapse" id="demo4">
			  <a href="{{ url('/about') }}" class="list-group-item">О нас</a>
			  <a href="{{ url('/news') }}" class="list-group-item">Новости</a>
			  <a href="{{ url('/awards') }}" class="list-group-item">Награды</a>
			  <a href="{{ url('/history') }}" class="list-group-item">История</a>
			  <a href="{{ url('/structure') }}" class="list-group-item">Структура компании</a>
			  <a href="{{ url('/leaders') }}" class="list-group-item">Лидеры компании</a>
			  <a href="{{ url('/clients') }}" class="list-group-item">Отзывы клиентов</a>
			</div>
			<a href="#demo5" class="list-group-item list-group-item-success" data-toggle="collapse">Карьера</a>
			<div class="collapse" id="demo5">
			  <a href="{{ url('/career') }}" class="list-group-item">Карьера</a>
			  <a href="{{ url('/education') }}" class="list-group-item">Обучение</a>
			  <a href="{{ url('/vacancies') }}" class="list-group-item">Вакансии</a>
			</div>
		  </div>
		</div>	
		
	<div class="ll-menu-panel-left-links">
		<a href="{{ url('/agents') }}" class="list-group-item"><i class="fa fa-user"></i> &nbsp;&nbsp;&nbsp;Найти агента</a>
		<a href="{{ url('/offices') }}" class="list-group-item"><i class="fa fa-map-marker"></i> &nbsp;&nbsp;&nbsp;Найти ближайший офис</a>
		@include('layouts.blocks.ll-contact-form',['ContactType'=>'sell','ContactId'=>1,'ButtonName'=>'&nbsp;&nbsp;&nbsp;Связаться со мной'])
	</div>
  
</div>
<a class="ll-menu-panel-trigger" href="#"><i class="fa fa-reorder"></i></a>

<script type="text/javascript">
$(document).ready(function(){
    $(".ll-menu-panel-trigger").click(function(){
        $(".ll-menu-panel-left").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
});

$(document).click( function(event){
	if( $(event.target).closest('.ll-menu-panel-left').length )
		return;
	$('.ll-menu-panel-left').fadeOut("normal");
	event.stopPropagation();
});

</script>