<div class="ll-search-result-count col-md-12">
	<div class="col-md-3 text-center">
	{{--<input type="text" value="75" class="dial" data-readOnly="true">--}}
		<p>{{$percent_sell}}% продано</p>
	</div>
	<div class="col-md-4 text-center  ll-search-result-count-object">
	{{--<p>Найдено 150 вариантов.</p>
	<p>1 ком. кв-ра, 5/а</p>--}}
		<p>Найдено {{$number_records}} вариантов</p>
		@foreach($details as $detail)
		<p><?php echo $detail;?></p>
		@endforeach
	</div>
	<div class="col-md-3 text-cente ll-search-result-count-object">
	{{--<p>Г. Ростов-на-Дону</p>
	<p>от 2000000</p>--}}
	@foreach($location as $l)
		<p>{{$l}}</p>
		@endforeach
	</div>
	<div class="col-md-2 text-center">
		<div class="ll-search-result-counter-circle">
			<i class="red-text">&#10004;</i>
		</div>	
		<p>Частый запрос</p>
	</div>
</div>

	<script>     
		 $(".dial").knob();
		 $({animatedVal: 0}).animate({animatedVal: 25}, {
			duration: 2000,
			easing: "swing",
			step: function() { 
				$(".dial").val(Math.ceil(this.animatedVal)).trigger("change"); 
				$(".dial").val($(".dial").val()+'%');
			}   
		 }); 
	</script>