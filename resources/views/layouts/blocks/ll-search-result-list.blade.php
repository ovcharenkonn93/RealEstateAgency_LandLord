<div class="ll-search-result-list ll-search-result-list-data" id="ll-search-result-list-item0">
    <div class="ll-search-result-list__photo">
        <a class="ll-data__link" href="#"><img class="ll-data__cover-src" src="{{ url('/images/placeholders/no-photo-estate.png') }}" alt="" title="" /></a>

        <div class="ll-search-result-list__new-price">
            <span class="ll-data__new-price"><!-- Новая цена --></span> <span class="ll-data__currency">руб.</span>
        </div>
        <div class="ll-search-result-list__old-price">
            <span class="ll-data__old-price"><!-- Старая цена --></span> <span class="ll-data__currency">руб.</span>
        </div>

        <div class="ll-search-result-list__photo-count">
            <span class="ll-data__photo-count"><!-- количество фотографий --></span> <i class="fa fa-camera"></i>
        </div>
    </div>

    <div class="ll-search-result-list__description">
        <h4><a class="ll-data__link" href="#"><span class="ll-data__street"><!-- Улица --></span> <span class="ll-data__district-admin"><!-- Ориентир --></span></a></h4>
        <div class="ll-search-result-list__city"><span class="ll-data__city"><!-- Город --></span> <span class="ll-data__district"><!-- Район --></span></div>

        <!--
        Вот сюда вставляешь данные из базы в таком формате
        <span class="ll-data__city">Город</span>
        -->


             <table class="ll-search-result-list__table1">
                  <tr class="ll-search-result-list__table1">
                    <td class="ll-search-result-list__table1"><span class="ll-data__type-apartment"><!-- Комнаты --></span></td>
                    <td class="ll-search-result-list__table1"><span class="ll-data__total-area"><!-- площадь --></span>&nbsp;м<sup>2</sup></td>
                    <td class="ll-search-result-list__table1">этаж: <span class="ll-data__floor"><!-- этаж --></span> / <span class="ll-data__floor-all"><!-- этажей всего --></span></td>
                  </tr>
            </table>
    </div>
    <table class="ll-search-result-list__table2">
          <tr>
            <td><a href="#"><i class="fa fa-map-marker"></i> На карте</a></td>
             <td><a href="#">Связаться с агентом</a></td>
             <td><a class="ll-data__link" href="#">Посмотреть</a></td>
             <td><a class="add-to-favorites" id="0" onclick="AddToFavorites(this)"><i class="fa fa-star-o list-fa-star-o" id="fa-star-o0"></i> В избранное</a></td>
          </tr>
    </table>
</div>

