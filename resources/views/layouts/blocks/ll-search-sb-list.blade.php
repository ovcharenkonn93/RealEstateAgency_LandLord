<div class="ll-search-result-list ll-search-result-list-data" id="{{$id}}">
    <div class="ll-search-result-list__photo">
        <a class="ll-data__link" href="#"><img class="ll-data__cover-src" src="<?php $t=explode("/",$estate->cover_src,2); if(count($t)>1) echo url($t[1]);?>" alt="" title="" /></a>

        <div class="ll-search-result-list__new-price">
            <span class="ll-data__new-price"><!-- Новая цена --><?php echo $estate->special_price?$estate->special_price:$estate->cost; ?> </span> <span class="ll-data__currency">руб.</span>
        </div>
		<?php if($estate->special_price){?>
        <div class="ll-search-result-list__old-price">
            <span class="ll-data__old-price"><!-- Старая цена -->{{$estate->cost}}</span> <span class="ll-data__currency">руб.</span>
        </div>
		<?php } ?>

        <div class="ll-search-result-list__photo-count">
            <span class="ll-data__photo-count"><!-- количество фотографий -->{{$estate->photo_count}}</span> <i class="fa fa-camera"></i>
        </div>
    </div>

    <div class="ll-search-result-list__description">
        <h4><a class="ll-data__link" href="#"><span class="ll-data__street">{{$estate->street}}</span> <span class="ll-data__district-admin"><!-- Ориентир -->{{$estate->district}}</span></a></h4>
        <div class="ll-search-result-list__city"><span class="ll-data__city"><!-- Город -->{{$estate->city}}</span> <span class="ll-data__district"><!-- Район -->{{$estate->administrative_district}}</span></div>
             <table class="ll-search-result-list__table1">
                  <tr class="ll-search-result-list__table1">
                    <td class="ll-search-result-list__table1"><span class="ll-data__type-apartment"><!-- Комнаты -->{{$estate->type_apartment->type_apartment}}</span></td>
                    <td class="ll-search-result-list__table1"><span class="ll-data__total-area"><!-- площадь -->{{$estate->total_area}}</span>&nbsp;м<sup>2</sup></td>
                    <td class="ll-search-result-list__table1">этаж: <span class="ll-data__floor"><!-- этаж -->{{$estate->floor}}</span> / <span class="ll-data__floor-all"><!-- этажей всего -->{{$estate->floor_all}}</span></td>
                  </tr>
            </table>
    </div>
    <table class="ll-search-result-list__table2">
          <tr>
            <td><a href="#"><i class="fa fa-map-marker"></i> На карте</a></td>
             <td><a href="#">Связаться с агентом</a></td>
             <td><a class="ll-data__link" href="/info/{{$estate->id}}">Посмотреть</a></td>
             <td><a class="add-to-favorites" id="0" onclick="AddToFavorites(this)"><i class="fa fa-star-o list-fa-star-o" id="fa-star-o0"></i> В избранное</a></td>
          </tr>
    </table>
</div>

