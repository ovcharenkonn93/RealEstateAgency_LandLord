<div class="ll-steps-why-buy-with-landlord">
    <h2>8 шагов к покупке недвижимости</h2>
    <img src="images/visuals/steps-to-buy-teaser.jpg" alt=""  title="">
    <p class="margin-top-20px">Решение купить недвижимость является одним из самых важных решений, которое Вы можете сделать! Начать этот процесс сегодня с нашим руководством крайне просто.
    <p><a href="steps-to-buy" class="llg-button-detail">Подробнее</a>
</div>