<div class="ll-steps-why-rent-out-with-landlord">
    <h2>8 шагов к сдаче недвижимости в аренду</h2>
    <img src="images/visuals/steps-to-rent-teaser.jpg" alt=""  title="">
    <p class="margin-top-20px">При сдаче недвижимости в аренду агент ЛЕНДЛОРД будет с Вами рядом на каждом шаге процесса, чтобы удостовериться, что Вы правильно выбрали время и цену
    <p><a href="steps-to-rent-out" class="llg-button-detail">Подробнее</a>
</div>