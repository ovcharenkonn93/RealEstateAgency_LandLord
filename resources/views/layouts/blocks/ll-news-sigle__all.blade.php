<div class="ll-company-news-page__slider">
	<h1 class="text-center red-text">Другие новости</h1>
	<div class="ll-company-news-page__carousel">
	                    <div class="carousel slide company-news-page-carousel" id="company-news-page-carousel">
                        <div class="carousel-inner">
                            <div class="item text-center active">
                                <ul class="thumbnails">
                                    <li>
                                        <div class="ll-company-news-page__carousel-item fff">
										    <p class="text-center">31 мая 2016 г.</p>
											<div class="ll-company-news-page__carousel-img">
													<a href="#"><img src="{{ url('images/placeholders/news0.jpg') }}" alt=""></a>
											</div>
												<h4 class="text-center">Самое бюджетное жилье в Ростове</h4>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="ll-company-news-page__carousel-item fff">
											<p class="text-center">31 мая 2016 г.</p>
											<div class="ll-company-news-page__carousel-img">
													<a href="#"><img src="{{ url('images/placeholders/news1.jpg') }}" alt=""></a>
											</div>
												<h4 class="text-center">Самое бюджетное жилье в Ростове</h4>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="ll-company-news-page__carousel-item fff">
											<p class="text-center">31 мая 2016 г.</p>
											<div class="ll-company-news-page__carousel-img">
													<a href="#"><img src="{{ url('images/placeholders/news2.jpg') }}" alt=""></a>
											</div>
												<h4 class="text-center">Самое бюджетное жилье в Ростове</h4>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- /Slide1 -->

                            <div class="item text-center">
                                <ul class="thumbnails">
                                    <li>
                                        <div class="ll-company-news-page__carousel-item fff">
											<p class="text-center">31 мая 2016 г.</p>
											<div class="ll-company-news-page__carousel-img">
													<a href="#"><img src="{{ url('images/placeholders/news3.jpg') }}" alt=""></a>
											</div>
												<h4 class="text-center">Самое бюджетное жилье в Ростове</h4>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="ll-company-news-page__carousel-item fff">
											<p class="text-center">31 мая 2016 г.</p>
											<div class="ll-company-news-page__carousel-img">
													<a href="#"><img src="{{ url('images/placeholders/news4.jpg') }}" alt=""></a>
											</div>
												<h4 class="text-center">Самое бюджетное жилье в Ростове</h4>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="ll-company-news-page__carousel-item fff ">
											<p class="text-center">31 мая 2016 г.</p>
											<div class="ll-company-news-page__carousel-img">
													<a href="#"><img src="{{ url('images/placeholders/news2.jpg') }}" alt=""></a>
											</div>
												<h4 class="text-center">Самое бюджетное жилье в Ростове</h4>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- /Slide1 -->

                        </div>


                        <nav>
                                <a data-slide="prev" href="#company-news-page-carousel" class="company-news-page-carousel-control-left"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                <a data-slide="next" href="#company-news-page-carousel" class="company-news-page-carousel-control-right"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </nav>
                        <!-- /.control-box -->

                    </div><!-- /#company-news-page-carousel -->
	</div>
</div>
