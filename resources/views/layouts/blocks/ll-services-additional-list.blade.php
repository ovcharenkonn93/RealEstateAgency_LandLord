<div class="col-md-12 ll-services-additional-list">
    <h2 class="red-text">Услуги юридического отдела:</h2>

    <div class="llg-wrapper llg-wrapper-toggle llg-wrapper-toggle__services-list">
            <div class="row">
                @include('layouts.blocks.ll-toggle')
            </div>
    </div>

    <div class="tab-content">
        <div id="llg-panel__main-list" class="tab-pane fade in active llg-panel">
            <div class="data-list" id="data-main-list">
                <table cellpadding="0" cellspacing="0">
                     @if($site_services)
                        @foreach($site_services as $site_service)
							<?php $flag=isset($site_secondary_services[$site_service->id]);?>
                            @if ($site_service->is_enabled === 1)
                                <tr>	
                                    <td>
                                	@if ($site_service->is_new === 1)
                                        <i class="ll-services-additional-list-marker">new</i>
                                    @else
                                        <i class="ll-services-additional-list-marker">&nbsp;&nbsp;&bull;&nbsp;&nbsp;</i>
                                    @endif
                                    </td>
                                    <td>
									@if($flag)
									<a onclick="$('.ll-services-additional-list-secondary').prop('hidden', true);
								$('.ll-services-additional-list-secondary{{$site_service->id}}').prop('hidden',!$('.ll-services-additional-list-secondary{{$site_service->id}}').prop('hidden'))">
									{{ $site_service->title }}
									</a>
									@else
									{{ $site_service->title }}
									@endif
									</td>
                                    <td><i class="ll-services-price">{{ $site_service->price }}</i></td>
                                    <td>{{ $site_service->terms }}</td>
                                </tr>
								@if($flag)
								@foreach($site_secondary_services[$site_service->id] as $site_secondary_service)
									<tr class="ll-services-additional-list-secondary ll-services-additional-list-secondary{{$site_service->id}}" hidden="true">
										<td>@if ($site_service->is_new === 1)
                                        <i class="ll-services-additional-list-marker">new</i>
                                    @else
                                        <i class="ll-services-additional-list-marker">&nbsp;&nbsp;&bull;&nbsp;&nbsp;</i>
                                    @endif</td>
										<td>{{$site_secondary_service->title}}</td>
										<td><i class="ll-services-price">{{$site_secondary_service->price}}</i></td>
										<td>{{$site_secondary_service->terms}}</td>
									</tr>
								@endforeach
								@endif
                            @endif
                        @endforeach
                    @endif
                </table>
                <div class="ll-services-more_info_href">
                    <a href="{{ url('/services-additional') }}">Подробнее</a>
                </div>
            </div>
        </div>

        <div id="llg-panel__documents-list" class="tab-pane fade in llg-panel">
            <div class="data-list" id="data-documents-list">
                <table>
				@foreach($additional_services as $ads)
                    <tr>
						<td><i class="ll-services-additional-list-marker">&nbsp;&nbsp;&bull;&nbsp;&nbsp;</i></td>
						<td>{{$ads->title}}&nbsp;</td>
						<td><i class="ll-services-price">{{$ads->price}}</i></td>
						<td>{{$ads->terms}}</td>
					</tr>
                @endforeach
				</table>
            </div>
            <div class="ll-services-more_info_href">
                <a href="{{ url('/services-additional') }}">Подробнее</a>
            </div>
        </div>
    </div>
</div>