<div class="ll-services__about">
    <p>Наша компания предоставляет своим клиентам полный комплекс риелторских услуг.
    Благодаря комплексному подходу, Вы сможете решить весь спектр вопросов, которые возникают при заключении сделок на рынке недвижимости.
    Это позволит Вам в значительной мере сэкономить не только время, но и средства. В итоге, Вы получите необходимые  услуги и при этом будете иметь не только время, но и средства.
    В итоге, Вы получите необходимые услуги и при этом будете иметь на руках полный пакет документов, прошедший многоступенчатую экспертизную проверку.</p>
    <p> И главное: обо всех тарифах на услуги нашей компании Вы будете знать заранее, а по ее получении Вам будет выдан&nbsp;чек. </p>
</div>
<div class="ll-services__about_our_services">

    <div class="hidden_block">
        <div id="hidden_block_top" class="ll-services__contact_info">
            <h1>(863)2 910-910</h1>
            <h3>Бесплатные консультации</h3>
				@include('layouts.blocks.ll-contact-form',['ContactType'=>'consultation','ContactId'=>'1213','ButtonName'=>'Получить консультацию'])
        </div>
    </div>

<div class="ll-services__our-services">
    <p class="services_header_txt red-text">Услуги предоставляемые нашей компанией</p>
    <ul>
        <li><a href="{{ url('/services-for-buyer') }}">Услуги для покупателя</a></li>
        <li><a href="{{ url('/services-for-seller') }}">Услуги для продавца</a></li>
        <li><a href="{{ url('/services-for-tenant') }}">Услуги для арендатора</a></li>
        <li><a href="{{ url('/services-for-owner') }}">Услуги для арендодателя</a></li>
    </ul>

    <hr>
    <h2>Дополнительные услуги</h2>

    <div class="ll-services-tariffs">
        <a href="{{ url('/services/tariffs') }}" class="llg-button">Тарифы на услуги</a>
    </div>
</div>
<div class="ll-services__our-services-right">
    <div id="right_contact_info" class="ll-services__contact_info">
        <h1>(863)2 910-910</h1>
        <h3>Бесплатные консультации</h3>
        @include('layouts.blocks.ll-contact-form',['ContactType'=>'consultation','ContactId'=>'1213','ButtonName'=>'Получить консультацию'])
    </div>

        <div class="ll-services__about_documents">
            <h3>Что необходимо знать при самостоятельном оформлении документов:</h3>
            <ul class="list-about_documents">
                <li><a href="{{ url('/services/privatization') }}">приватизация жилья</a></li>
                <li><a href="{{ url('/services/alienation') }}">отчуждение недвижимого имущества</a>
                    <ul class="sub_list-about_documents">
                        <li><a href="{{ url('/services/alienation#sale') }}">покупка-продажа</a></li>
                        <li><a href="{{ url('/services/alienation#present') }}">дарение</a></li>
                        <li><a href="{{ url('/services/alienation#exchange') }}">мена</a></li>
                        <li><a href="{{ url('/services/alienation#rent') }}">рента</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/services/titling') }}">оформление прав собственности</a>
                    <ul class="sub_list-about_documents">
                        <li><a href="{{ url('/services/titling#land') }}">на землю</a></li>
                        <li><a href="{{ url('/services/titling#building') }}">на постройки</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/services/inheritance') }}">вступления в права наследования</a></li>
                <li><a href="{{ url('/services/dacha-amnesty') }}">оформление по дачной амнистии</a>
                    <ul class="sub_list-about_documents">
                        <li><a href="{{ url('/services/dacha-amnesty#land') }}">земли</a></li>
                        <li><a href="{{ url('/services/dacha-amnesty#building') }}">построек</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/services/transfer-to-non-residential') }}">перевод в нежилой фонд жилых помещений</a></li>
                <li><a href="{{ url('/services/transfer-to-residential') }}">перевод в жилой фонд нежилых помещений</a></li>
                <li><a href="{{ url('/services/split-of-land') }}">раздел земельных участков</a></li>
                <li><a href="{{ url('/services/replanning-legalization') }}">узаконение перепланировок</a></li>
                <li><a href="{{ url('/services/moving-between-categories') }}">перевод земельных участков из одной категории в другую</a></li>
                <li><a href="{{ url('/services/connecting-to-communications') }}">подключение объектов к коммуникациям</a>
                    <ul class="sub_list-about_documents">
                        <li><a href="{{ url('/services/connecting-to-communications#water') }}">водоснабжение</a></li>
                        <li><a href="{{ url('/services/connecting-to-communications#energy') }}">энергоснабжение</a></li>
                    </ul>
                </li>
            </ul>
            <div class="order_btn1">
                @include('layouts.blocks.ll-contact-form',['ContactType'=>'consultation','ContactId'=>'1213','ButtonName'=>'Заявка на услугу'])
            </div>
        </div>
    </div>
</div>