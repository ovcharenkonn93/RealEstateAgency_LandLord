<div class="ll-user-page">

	<div class="row margin-top-50px">


		<div class="col-md-8 ll-user-page-form">

			 <form action="" method="post">
			 <p>
				<label for="firstname">
				   Имя: 
				</label>
				<input type="text" name="firstname" />
				<br />
				<label for="lastname">
				   Фамилия: 
				</label>
				<input type="text" name="lastname" />
				<br />
				<label for="city">
				   Город: 
				</label>
				<input type="text" name="city" />
				<br />
				<label for="email">
				   E-mail: 
				</label>
				<input type="text" name="email" />
				<br />
				<label for="phone">
				   Телефон: 
				</label>
				<input type="text" name="phone" />
				<br />
				<input type="checkbox" name="sex" value="secret" />
					Получать рассылки
				<input type="submit" value="Сохранить">
			 </p>
		  </form>

		</div>
		<div class="col-md-4">
			<div class="ll-user-page-new_poster">
				<a href="{{ url('/sell') }}">
					<div class="ll-user-page-new_poster-icon">
						<p>+</p>
					</div>	 
				</a>
				<div class="ll-user-page-new_poster-link">
					<a href="{{ url('/sell') }}">Разместить объявление</a>
				</div>	 
            </div>	  
			<div class="ll-user-page-agent_link margin-top-20px text-center">
                <a href="{{ url('/agents') }}">Найти агента</a>
            </div>	
			<div class="ll-user-page-social margin-top-50px">
                <p>Привязать аккаунт</p>
					<div class="ll-user-page-social-buttons">
						<ul>
							<li><a href="#"><i class="fa fa-instagram"> </i></a></li>
							<li><a href="#"><i class="fa fa-vk"> </i></a></li>
							<li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"> </i></a></li>
						</ul>
					</div>
            </div>				
		</div>

	</div>

</div>