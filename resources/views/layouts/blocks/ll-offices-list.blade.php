<div class="ll-offices-list">
	<div class="ll-offices-list-header">
		<div class="ll-offices-list-one">
			Город
		</div>
		<div class="ll-offices-list-two">
			Адрес
		</div>
		<div class="ll-offices-list-five">
			Связаться
		</div>
		<div class="ll-offices-list-four">
			Время работы
		</div>		
		<div class="ll-offices-list-three">
			Контакты
		</div>
	</div>
	@foreach($offices as $office)
	<div class="ll-offices-list-item" onclick="select_office({{$office->id}})">
		<div class="ll-offices-list-one">
			{{$office->city}}
		</div>
		<div class="ll-offices-list-two">
			{{$office->address}}
		</div>
		<div class="ll-offices-list-five">
			<ul>
				<li><a href="">Агенты</a></li>
				<li><a href="">Вакансии</a></li>
				<li>@include('layouts.blocks.ll-contact-form',['ContactType'=>'offices','ContactId'=>3,'ButtonName'=>'Связаться'])</li>
			</ul>
		</div>
		<div class="ll-offices-list-four">
			{{$office->work_time}}
		</div>
		<div class="ll-offices-list-three">
			<ul>
				<li>
					<i class="fa fa-whatsapp"></i> {{$office->telephone}}
				</li>
				<li>
					<i class="fa fa-skype"></i> {{$office->skype}}			
				</li>
				<li>
					<i class="fa fa-envelope-o"></i> {{$office->email}}
				</li>				
			</ul>
		</div>		
	</div>
	@endforeach
</div>
