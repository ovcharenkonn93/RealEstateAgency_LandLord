<div class="ll-home-news-variant-3 col-md-12 llg-hide">
   <div class="col-lg-4 col-md-4 col-sm-4 text-center">
		<div class="ll-home-news-variant-3__element">
			<div class="ll-home-news-variant-3__image">
				<a href="news"><img src="{{ url('images/icons/icon-news.png') }}" alt="Новости" title="Новости"></a>
			</div>
			<h4><a href="news">Новости</a></h4>

			<div class="ll-home-news-variant-3__text">
				<ul>
				    <li><a href="news/0">12-й автобусный тур по новостройкам Нахичевани</a>
				    <li><a href="news/1">Компания ЛЕНДЛОРД столкнулась с проблемами при продажах в Ростове</a>
				    <li><a href="news/2">ЛЕНДЛОРД и «Сбербанк России» приглашают Вас в автобусный тур по новостройкам</a>
				</ul>
			</div>
		</div>
    </div>
   <div class="col-lg-4 col-md-4 col-sm-4 text-center">
		<div class="ll-home-news-variant-3__element">
			<div class="ll-home-news-variant-3__image">
				<a href="news"><img src="{{ url('images/icons/icon-actions.png') }}" alt="Акции" title="Акции"></a>
			</div>
			<h4><a href="news">Акции</a></h4>
			<div class="ll-home-news-variant-3__text">
				<ul>
				    <li><a href="news/3">Агентство недвижимости ЛЕНДЛОРД дарит квартиры по суперцене&nbsp;— 1200 тыс. рублей!</a>
				    <li><a href="news/4">Акция «Цена июня»: осталось 18 квартир по стоп-цене&nbsp;— 1 350 тыс. рублей!</a>
				    <li><a href="news/5">Выбирайте новые эксклюзивы 2016 года.</a>
				</ul>
			</div>
		</div>
    </div>
   <div class="col-lg-4 col-md-4 col-sm-4 text-center">
		<div class="ll-home-news-variant-3__element">
			<div class="ll-home-news-variant-3__image">
				<a href="news"><img src="{{ url('images/icons/icon-law.png') }}" alt="Правовой ликбез" title="Правовой ликбез"></a>
			</div>
			<h4><a href="news">Правовой ликбез</a></h4>
			<div class="ll-home-news-variant-3__text">
				<ul>
				    <li><a href="news/6">Как правильно заключить договор уступки права требования</a>
				    <li><a href="news/7">Можно ли оспаривать дарственную при регистрации договора после смерти дарителя?</a>
				    <li><a href="news/8">Предварительный договор если документы на квартиру находятся в стадии оформления</a>
				</ul>
			</div>
		</div>
    </div>
</div>