<div class="ll-company-news-page__comments">
	<div class="ll-company-news-page__comments-header">
		<div class="ll-company-news-page__comments-count">
			<p>Комментариев: 214634</p>
		</div>
		<div class="ll-company-news-page__comments-sort">
			Сортировать
			<select>
			  <option>Новые</option>
			  <option>Лучшие</option>
			</select>
		</div>
	</div>

<div class="ll-company-news-page__comment-add">
	<div class="ll-company-news-page__comment-add-avatar">
		<img src="{{ url('/images/placeholders') }}/image-placeholder.png" alt="">
	</div>
	<div class="ll-company-news-page__comment-add-massage">
		<form action="" method="POST">
			<textarea name="message" type="text" placeholder="Ваш комментарий" rows="4"></textarea>
		</form>
	</div>
</div>

<div class="ll-company-news-page__comment">
	<div class="ll-company-news-page__comment-avatar">
		<img src="{{ url('/images/placeholders') }}/image-placeholder.png" alt="">
	</div>
	<div class="ll-company-news-page__comment-massage">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
</div>

<div class="ll-company-news-page__comment">
	<div class="ll-company-news-page__comment-avatar">
		<img src="{{ url('/images/placeholders') }}/image-placeholder.png" alt="">
	</div>
	<div class="ll-company-news-page__comment-massage">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
</div>
</div>