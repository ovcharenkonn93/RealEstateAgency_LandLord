<div class="ll-home-offers col-md-12">
    <div class="col-lg-4 col-md-4 col-sm-4 text-center">
		<div class="ll-home-offers__element">
			<div class="ll-home-offers__image">
				<a href="buy-advices"><img src="{{ url('images/icons/icon-search.png') }}" alt="Найти недвижимость" title="Найти недвижимость"></a>
			</div>
			<h4><a href="buy-advices">Найти</a></h4>
			<div class="ll-home-offers__text">
				<p>Получите советы. Начните искать как профессионал.</p>
			</div>
			<br> <a href="buy-advices" class="ll-home-offers__button">Начать поиск</a>
		</div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 text-center">
		<div class="ll-home-offers__element">
			<div class="ll-home-offers__image">
				<a href="sell-advices"><img src="{{ url('images/icons/icon-sell.png') }}" alt="Продать недвижимость" title="Продать недвижимость"></a>
			</div>
			<h4><a href="sell-advices">Продать</a></h4>
			<div class="ll-home-offers__text">
				<p>Увеличьте возможности вместе с нашими агентами.</p>
			</div>
			<br><a href="sell-advices" class="ll-home-offers__button">Увеличить выгоду</a>
		</div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 text-center">
		<div class="ll-home-offers__element">
			<div class="ll-home-offers__image">
				<a href="analytics"><img src="{{ url('images/icons/icon-analytics.png') }}" alt="Аналитика" title="Аналитика"></a>
			</div>
			<h4><a href="analytics">Аналитика</a></h4>
			<div class="ll-home-offers__text">
			<p>Узнайте сегодняшние тенденции развития рынка.</p>
			</div>
			<br><a href="analytics" class="ll-home-offers__button">Ознакомиться</a>
		</div>
    </div>
</div>