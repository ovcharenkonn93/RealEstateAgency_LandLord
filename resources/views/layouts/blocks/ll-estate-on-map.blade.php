<div class="ll-estate-on-map">
    <div class="ll-estate-on-map__title">
        Объекты поблизости:
    </div>
    <div id="map" style="width: 100%; height: 400px"></div>
</div>
<script type="text/javascript">
	var map, mark;
	function initMap(){
		console.log('initMap()');
		map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: {{$estate->latitude}}, lng: {{$estate->longitude}}},
			zoom: 8,
			disableDefaultUI: true,
			zoomControl: true,
			mapTypeControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		mark=new google.maps.Marker({
				position: {lat: {{$estate->latitude}}, lng: {{$estate->longitude}} },
				map: map,
			});
		}
	initMap();
</script>