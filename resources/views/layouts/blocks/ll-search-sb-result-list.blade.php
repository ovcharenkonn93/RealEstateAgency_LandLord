<div class="ll-search-result-list ll-search-result-list-data" id="ll-search-result-list-item{{$id}}">
    <div class="ll-search-result-list__photo">
        <a class="ll-data__link" href="#"><img class="ll-data__cover-src" src="<?php 
		try{
			$link=explode("/",$estate->cover_src,2)[1];
			if(file_exists($_SERVER['DOCUMENT_ROOT']."/".$link)){
				echo url($link);
			}
			else echo url('/images/placeholders/no-photo-estate.png');
		}
		catch(Exception $e){
			echo url('/images/placeholders/no-photo-estate.png');
		}
		?>" alt="" title="" /></a>

        <div class="ll-search-result-list__new-price">
            <span class="ll-data__new-price">{{$estate->price}}<!-- Новая цена --></span> <span class="ll-data__currency">руб.</span>
        </div>

        <div class="ll-search-result-list__photo-count">
            <span class="ll-data__photo-count"><!-- количество фотографий --></span> <i class="fa fa-camera"></i>
        </div>
    </div>

    <div class="ll-search-result-list__description">
        <h4><a class="ll-data__link" href="{{url("/info/".$estate->id)}}"><span class="ll-data__street">{{$estate->street}}<!-- Улица --></span> <span class="ll-data__district-admin">{{$estate->administrative_district}}<!-- Ориентир --></span></a></h4>
        <div class="ll-search-result-list__city"><span class="ll-data__city">{{$estate->city}}<!-- Город --></span> <span class="ll-data__district">{{$estate->district}}<!-- Район --></span></div>

        <!--
        Вот сюда вставляешь данные из базы в таком формате
        <span class="ll-data__city">Город</span>
        -->


             <table class="ll-search-result-list__table1">
                  <tr class="ll-search-result-list__table1">
                    <td class="ll-search-result-list__table1"><span class="ll-data__type-apartment">{{$estate->type_apartment}}<!-- Комнаты --></span></td>
                    <td class="ll-search-result-list__table1"><span class="ll-data__total-area">{{$estate->area_general}}<!-- площадь --></span>&nbsp;м<sup>2</sup></td>
                    <td class="ll-search-result-list__table1">этаж: <span class="ll-data__floor">{{$estate->floor}}<!-- этаж --></span> / <span class="ll-data__floor-all">{{$estate->floor_all}}<!-- этажей всего --></span></td>
                  </tr>
            </table>
    </div>
    <table class="ll-search-result-list__table2">
          <tr>
            <td><a href="#"><i class="fa fa-map-marker"></i> На карте</a></td>
             <td><a href="#">Связаться с агентом</a></td>
             <td><a class="ll-data__link" href="{{url("/info/".$estate->id)}}">Посмотреть</a></td>
             <td><a class="add-to-favorites" onclick="AddToFavorites({{$estate->id}})"><i class="fa fa-star-o list-fa-star-o" id="fa-star-o{{$estate->id}}"></i> В избранное</a></td>
          </tr>
    </table>
</div>

