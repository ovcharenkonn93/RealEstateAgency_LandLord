<div class="col-md-12 ll-services-offers">
    <h2 class="red-text">Услуги, предоставляемые нашей компанией:</h2>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 text-center">
            <div class="ll-services-offers__element">
                <div class="ll-services-offers__image">
                    <a href="{{ url('/services-for-buyer') }}"><img src="{{ url('images/icons/icon-services-for-buyer.png') }}" alt="Услуги для покупателя" title="Услуги для покупателя"></a>
                </div>
                <h4><a href="{{ url('/services-for-buyer') }}">Услуги для<br>покупателя</a></h4>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 text-center">
            <div class="ll-services-offers__element">
                <div class="ll-services-offers__image">
                    <a href="{{ url('/services-for-seller') }}"><img src="{{ url('images/icons/icon-services-for-seller.png') }}" alt="Услуги для продавца" title="Услуги для продавца"></a>
                </div>
                <h4><a href="{{ url('/services-for-seller') }}">Услуги для<br>продавца</a></h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 text-center">
            <div class="ll-services-offers__element">
                <div class="ll-services-offers__image">
                    <a href="{{ url('/services-for-tenant') }}"><img src="{{ url('images/icons/icon-services-for-tenant.png') }}" alt="Услуги для арендатора" title="Услуги для арендатора"></a>
                </div>
                <h4><a href="{{ url('/services-for-tenant') }}">Услуги для<br>арендатора</a></h4>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 text-center">
            <div class="ll-services-offers__element">
                <div class="ll-services-offers__image">
                    <a href="{{ url('/services-for-owner') }}"><img src="{{ url('images/icons/icon-services-for-owner.png') }}" alt="Услуги для арендодателя" title="Услуги для арендодателя"></a>
                </div>
                <h4><a href="{{ url('/services-for-owner') }}">Услуги для<br>арендодателя</a></h4>
            </div>
        </div>
    </div>
</div>