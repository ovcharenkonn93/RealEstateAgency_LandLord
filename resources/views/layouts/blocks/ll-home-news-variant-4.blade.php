<div class="ll-home-news-variant-4 col-md-12 llg-hide">
		<div class="col-md-4">
			<div class="ll-home-news-variant-4__block" onmouseover="link_show(0)" onmouseout="link_hide(0)">
					<div class="ll-home-news-variant-4__text">
						<h3 class="text-center">Новые экслюзивы</h3>
						<p>На календаре конец июня, а это значит, что большая часть лета еще впереди! Как на счет того, чтобы наслаждаться солнечными лучами в своей собственной квартире? Кстати, специально для Вас мы подготовили новые экслюзивы до 4,5 миллионов рублей ...</p>
					</div>	
				<a href="{{ url('/news') }}"><div id="ll-home-news-variant-4__more0" class="ll-home-news-variant-4__more"><p>Читать дальше &gt;</p></div></a>
			</div>		
		</div>
		<div class="col-md-4">	
			<div class="ll-home-news-variant-4__block" onmouseover="link_show(1)" onmouseout="link_hide(1)">
				<div class="ll-home-news-variant-4__photo">	
					<img src="{{ url('/images/placeholders') }}/news0.jpg">
				</div>
				<div class="ll-home-news-variant-4__header">	
					<h3 class="text-center">ЛЕНДЛОРД провел конференцию</h3>
				</div>
				<a href="{{ url('/news') }}"><div id="ll-home-news-variant-4__more1" class="ll-home-news-variant-4__more"><p>Читать дальше &gt;</p></div></a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="ll-home-news-variant-4__block" onmouseover="link_show(2)" onmouseout="link_hide(2)">
				<div class="ll-home-news-variant-4__text">
					<h3 class="text-center">«День отличника»</h3>
					<p>	21 июня в нашем учебном центре прошел «День отличника» для детей сотрудников, посвященный успешному окончанию учебного года! Ребята играли, веселились и ловко отгадывали загадки! Ну и конечно, какой же праздник без подарков и сладкого ... </p>
				</div>
				<a href="{{ url('/news') }}"><div id="ll-home-news-variant-4__more2" class="ll-home-news-variant-4__more"><p>Читать дальше &gt;</p></div></a>
			</div>
		</div>
</div> 

<script>
function link_show(a){
	document.getElementById('ll-home-news-variant-4__more'+a).style.display='inline-block';
}
function link_hide(a){
	document.getElementById('ll-home-news-variant-4__more'+a).style.display='none';
}
</script>