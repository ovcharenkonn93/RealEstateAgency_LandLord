<div class="col-md-12 ll-services-discounts">
	<div class="col-md-6">
	    <div class="row">
		    <h2 class="text-center red-text">Скидка на юридические услуги</h2>
		</div>
		<div class="row ll-services-discounts__percent">
		    5%
		</div>
       <div class="order_btn text-center">
            @include('layouts.blocks.ll-contact-form',['ContactType'=>'service','ContactId'=>'1213','ButtonName'=>'Заказать услугу'])
       </div>
	</div>

	<div class="col-md-6 text-center">
		<div class="ll-services-discounts-ul">
			<ul>
				<li><p><i class="ll-services-additional-list-marker">&nbsp;&nbsp;&#10003;&nbsp;&nbsp;</i>ветеранам, пенсионерам, инвалидам и многодетным семьям</p>
				<li><p><i class="ll-services-additional-list-marker">&nbsp;&nbsp;&#10003;&nbsp;&nbsp;</i>участникам социальных программ (за исключением программы «Материнский капитал»)</p>
				<li><p><i class="ll-services-additional-list-marker">&nbsp;&nbsp;&#10003;&nbsp;&nbsp;</i>при одновременной покупке и продаже</p>
				<li><p><i class="ll-services-additional-list-marker">&nbsp;&nbsp;&#10003;&nbsp;&nbsp;</i>при продаже вторичного жилья и последующей покупке новостройки</p>
				<li><p><i class="ll-services-additional-list-marker">&nbsp;&nbsp;&#10003;&nbsp;&nbsp;</i>при повторном обращении</p>
			</ul>
		</div>	
	</div>
</div>