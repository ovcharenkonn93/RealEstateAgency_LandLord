<div class="ll-agents-grid" id="ll-agents-grid-grid-item{{ $i }}">
	<div class="ll-agents-grid-photo">
		<img class="ll-data__src" src="{{ url(explode('/',$agent->src,2)[1])}}" onerror="this.src='{{url('images/placeholders/no-photo-person.png')}}'" alt="" title="">
	</div>
	<div class="ll-agents-grid-info">
		<div class="ll-agents-grid-name">
			<a href="agent-info/{{ $agent->id }}">
				{{$agent->fullname}}
			</a>
		</div>
		
		<div class="ll-agents-grid-contacts">
			<ul>
			<li><a href="#"><i class="fa fa-2x fa-whatsapp"> </i></a>   {{$agent->phone_mob}}</li>
			<li><a href="#"><i class="fa fa-2x fa-whatsapp"> </i></a>   {{$agent->phone_home}}</li>
			<li><a href="#"><i class="fa fa-2x fa-skype"> </i></a>   {{$agent->skype}}</li>
			<li><a href="#"><i class="fa fa-2x fa-envelope-o"> </i></a>   {{$agent->email}}</li>
			</ul>
		</div>
<!--
		<hr>
		<div class="ll-agents-grid-social-buttons text-center">
			<ul>
				<li><a href="#"><i class="fa fa-instagram"> </i></a></li>
				<li><a href="#"><i class="fa fa-vk"> </i></a></li>
				<li><a href="#"> <i class="fa fa-odnoklassniki"></i></a></li>
				<li><a href="#"> <i class="fa fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter"> </i></a></li>
			</ul>
		</div>
-->
	</div>
</div>
