<div class="ll-estate-info">
    <div class="ll-estate-info__header">
        <div class="ll-estate-info__header-text">
            @if($estate->r_type=='apartment')<div class="ll-estate-info__title">{{$estate->type_apartment}} квартира на продажу</div>
			@elseif($estate->r_type=='rent')<div class="ll-estate-info__title">{{$estate->type_apartment}} квартира в аренду</div>
			@elseif($estate->r_type=='households')<div class="ll-estate-info__title">Дом на продажу</div>
			@elseif($estate->r_type=='land')<div class="ll-estate-info__title">Участок на продажу</div>
			@endif
            <div class="ll-estate-info__address"><span class="ll-data__street">{{$estate->thoroughfare}}</span>, <span class="ll-data__city">{{$estate->locality}}</span></div>
            <div class="ll-estate-info__date-of-publication">Объявление размещено <span class="ll-data__created-at">{{$estate->created_at}}</span></div>
        </div>
        <div class="ll-estate-info__prices">
            <div class="ll-estate-info__new-price"><span class="ll-data__new-price">{{$estate->price}}</span> <span class="ll-data__currency">руб.</span></div>
        </div>
    </div>

    <div class="ll-estate-info__card">
        <div class="ll-estate-info__image">
            <div class="ll-estate-info__main-image">
				@include('layouts.blocks.ll-estate-info-slider')
            </div>
            <div class="ll-estate-info__slider"></div>
        </div>
		
		<div class="ll-estate-info__map_favorites">
             <table class="ll-estate-info_favorites text-center">
                  <tr>
                    <td><a href="#"><i class="fa fa-map-marker"></i> На карте</a></td> <td><a class="add-to-favorites-info" id="{{$estate->id}}" onclick="AddToFavorites(this)"><i class="fa fa-star-o info-fa-star-o" id="info-fa-star-o{{$estate->id}}"></i> В избранное</a></td>
				  </tr>
            </table>
        </div>
		
        <div class="ll-estate-info__parameters">
			<style>
				table.ll-estate-info-viewport{
					width: 100%;
				}
				table.ll-estate-info-viewport td:first-child{
					font-weight: 600;
				}
			</style>
			<table class="ll-estate-info-viewport">
			{{--Адрес--}}
				<tr><td>Город:</td><td>{{$estate->locality}}</td>
				<tr><td>Административный район:</td><td>{{$estate->dependent_locality}}</td>
				<tr><td>Улица:</td><td>{{$estate->thoroughfare}}</td>
				<tr><td>Субрайон/неофициальный район:</td><td>{{$estate->district}}</td>
				{{--Заменить после уточнения имя переменной--}}
				@if(isset($estate->variable)) <tr><td>Открытая продажа:</td><td>{{$estate->variable}}</td>@endif
				{{--Цена--}}
				<tr><td>Цена:</td><td>{{$estate->price}}</td>
				@if(isset($estate->clear_sale)) <tr><td>Чистая продажа:</td><td>{{$estate->clear_sale}}</td>@endif
			@if($estate->r_type=='apartment'||$estate->r_type=='rent')
				<tr><td>Тип квартиры:</td><td>{{$estate->type_apartment}}</td></tr>
				<tr><td>Этаж:</td><td>{{$estate->floor}}/{{$estate->floor_all}}</td></tr>
				<tr><td>Материал стен:</td><td>{{$estate->wall_material}}</td></tr>
				<tr><td>Фонд:</td><td>{{$estate->fund}}</td></tr>
				<tr><td>Площадь общая/жилая/кухня:</td><td>{{$estate->area_general}}м<sup>2</sup>/{{$estate->area_living}}м<sup>2</sup>/{{$estate->area_kitchen}}м<sup>2</sup></td></tr>
				@if(!$estate->is_under_construction)<tr><td>Год постройки:</td><td>{{$estate->year_build}}</td></tr>
				@else<tr><td>Дата сдачи</td><td>{{$estate->date_putting}}</td><tr>@endif
				{{--Доп.информация--}}
				<tr><td>Консьерж</td><td>{{$estate->has_concierge}}</td></tr>
				<tr><td>Резервное энергоснабжение</td><td>{{$estate->is_backup_power}}</td></tr>
				<tr><td>Количество квартир на этаже</td><td>{{$estate->floor_apartments_count}}</td></tr>
				<tr><td>Перепланировка</td><td>{{$estate->is_free_planing}}</td></tr>
				<tr><td>Придомовая территория</td><td>{{$estate->has_around_territory}}</td></tr>
				@if($estate->has_around_territory)
					<tr><td>Ограждение</td><td>{{$estate->is_guarded}}</td></tr>
					<tr><td>Видеонаблюдение</td><td>{{$estate->is_video_monitoring}}</td></tr>
					<tr><td>Контроль доступа</td><td>{{$estate->is_control_access}}</td></tr>
				@endif
				@if(isset($estate->has_parking))<tr><td>Наличие выделенной парковки</td><td></td></tr>
					<tr><td>Охраняемая парковка</td><td>{{$estate->is_parking_guards}}</td></tr>
					<tr><td>Связь лифта с парковкой</td><td></td></tr>
					<tr><td>Наличие собственного паркоместа</td><td></td></tr>
				@endif
			@elseif($estate->r_type=='households')
				@if(isset($estate->floor)) <tr><td>Этажи</td><td>{{$estate->floor}}</td>@endif
				<tr><td>Материал стен</td><td>{{$estate->wall_material}}</td></tr>
				<tr><td>Земельный участок</td><td>{{$estate->stead}} ар</td></tr>
				<tr><td>Двор</td><td>{{$estate->yard}}</td></tr>
				<tr><td>Площадь общая/жилая/кухня:</td><td>{{$estate->area_general}}м<sup>2</sup>/{{$estate->area_living}}м<sup>2</sup>/{{$estate->area_kitchen}}м<sup>2</sup></td></tr>
				@if(!$estate->is_under_construction)<tr><td>Год постройки:</td><td>{{$estate->year_build}}</td></tr>
				@else<tr><td>Дата сдачи</td><td>{{$estate->date_putting}}</td><tr>@endif
				
				{{--Дополнительная информация--}}
				<tr><td>Удобства</td><td>{{$estate->comfort}}</td></tr>
				<tr><td>Гараж</td><td>{{$estate->has_garage?"есть":"нет"}}</td></tr>
				<tr><td>Санузел</td><td>{{$estate->bathroom}}</td></tr>
				<tr><td>Подъезд к участку</td><td>{{$estate->pavement}}</td></tr>
				<tr><td>Состояние</td><td>{{$estate->state}}</td></tr>
				<tr><td>Отопление</td><td>{{$estate->heating}}</td></tr>
				<tr><td>Расположение участка</td><td>{{$estate->placement}}</td></tr>
				<tr><td>Комнаты</td><td>{{$estate->room_count}}</td></tr>
				<tr><td>Горячая вода</td><td>{{$estate->hot_water}}</td></tr>
				<tr><td>Потолки</td><td>{{$estate->ceilings}}</td></tr>
				<tr><td>Наличие придомовой территории:</td><td>{{$estate->has_around_territory?"есть":"нет"}}</td></tr>
				<tr><td>Инженерные системы дома:</td><td>{{$estate->engineer_systems}}</td></tr>
				<tr><td>Выход к водоёму</td><td>{{$estate->output_to_water}}</td></tr>
				<tr><td>Местонахождение дома:</td><td>{{$estate->location}}</td></tr>
				<tr><td>Коммуникации</td><td>@foreach($com as $c){{$c->com}}<br>@endforeach</td></tr>	
				<tr><td>Коммуникации с оплачеными паями</td><td>@foreach($com_mut as $c_mut){{$c_mut->com_mut}}<br>@endforeach</td></tr>
			@elseif($estate->r_type=='land')
				<tr><td>Тип участка</td><td>{{$estate->type_land}}</td></tr>
				<tr><td>Площадь</td><td>{{$estate->stead}} ар</td></tr>
				<tr><td>Размеры</td><td>{{$estate->stead_x}}x{{$estate->stead_y}} м</td></tr>
				<tr><td>Подъезд к участку</td><td>{{$estate->pavement}}</td></tr>
				<tr><td>Расположение</td><td>{{$estate->location}}</td></tr>
				<tr><td>Рельеф</td><td>{{$estate->land_relief}}</td></tr>
				<tr><td>Фасад</td><td>{{$estate->facade}} м</td></tr>
				<tr><td>Выход к воде</td><td>{{$estate->output_to_water}}</td></tr>
				<tr><td>Прилегающая территория</td><td>{{$estate->has_around_territory?"есть":"нет"}}</td></tr>
				<tr><td>Местонахождение</td><td>{{$estate->land_location}}</td></tr>
				<tr><td>Коммуникации</td><td>@foreach($com as $c){{$c->com}}<br>@endforeach</td></tr>	
				<tr><td>Коммуникации с оплачеными паями</td><td>@foreach($com_mut as $c_mut){{$c_mut->com_mut}}<br>@endforeach</td></tr>
			@endif
			</table>
        </div>
		<div class="ll-estate-info__buy-block">
			@include('layouts.blocks.ll-contact-form',['ContactType'=>'info','ContactId'=>$estate->id,'ButtonName'=>'Купить / обменять'])
			     <div class="ll-estate-info-agent-social-buttons">
					<ul>
						<li><a onclick="Share.vkontakte('{{ url('/info') }}/{{$estate->id}}','2-комнатная квартира на продажу','{{ url('/images/placeholders') }}/estate{{$estate->id}}.jpg','Описание')"><i class="fa fa-vk"> </i></a></li>
						<li><a onclick="Share.odnoklassniki('{{ url('/info') }}/{{$estate->id}}')"> <i class="fa fa-odnoklassniki"></i></a></li>
						<li><a onclick="Share.facebook('{{ url('/info') }}/{{$estate->id}}/')"> <i class="fa fa-facebook"></i></a></li>
						<li><a onclick="Share.twitter('{{ url('/info') }}/{{$estate->id}}','2-комнатная квартира на продажу')"><i class="fa fa-twitter"> </i></a></li>
					</ul>
				 </div>
        </div>
        <div class="ll-estate-info__agent">
            <div class="ll-estate-info__agent-phone text-center">
                <strong>Телефон агента:</strong><br> <span class="ll-data__phone-mob">{{$employee->phone_mob}}</span>
            </div>
            <div class="ll-estate-info__agent-photo">
                <img class="ll-data__employee-assigned-src" src="{{ url(explode('/',$employee->src,2)[1]) }}" alt="images/placeholders/no-photo-person.png" title="">
            </div>
			<div class="text-center">
                @include('layouts.blocks.ll-contact-form',['ContactType'=>'infoAgent','ContactId'=>$estate->id,'ButtonName'=>'Связаться с агентом'])
            </div>
        </div>
    </div>

    <div class="ll-estate-info__description">
        <div class="ll-estate-info__description-title">
        Описание недвижимости:
        </div>
        <div class="ll-estate-info__description-content">
			<span class="ll-data__text-website"></span>
		</div>
    </div>
</div>

<script>
/*
//--------------------------Социальные сети-----------------------------------------
Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		url  = 'http://vkontakte.ru/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		Share.popup(url);
	},
	odnoklassniki: function(purl) {
		url  = 'https://connect.ok.ru/offer?';
		url += 'url='    + encodeURIComponent(purl);
		Share.popup(url);
	},
	facebook: function(purl) {
		url  = 'https://www.facebook.com/sharer/sharer.php?';
		url += 'u='      + encodeURIComponent(purl);
		Share.popup(url);
	},
	twitter: function(purl, ptitle) {
		url  = 'http://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.popup(url);
	},
	popup: function(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
	}
};*/
</script>
