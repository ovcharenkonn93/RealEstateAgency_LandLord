<div class="ll-company-news-page">
@if($news)
	<h1>{{$news->name}}</h1>
	<div class="ll-company-news-page__date" align="left">
		<p>{{$news->created_at}}</p>
	</div>
	<div class="ll-company-news-page__social-buttons" align="left">
		<p>Поделиться</p>
		<ul>
			<li><a onclick="Share.vkontakte('{{ url('/news') }}/{{$news->id}}','Агентство недвижимости ЛЕНДЛОРД','{{ url('/images/placeholders') }}/news{{$news->id}}.jpg','Описание')"><i class="fa fa-vk"> </i></a></li>
			<li><a onclick="Share.odnoklassniki('{{ url('/news') }}/{{$news->id}}')"> <i class="fa fa-odnoklassniki"></i></a></li>
			<li><a onclick="Share.facebook('{{ url('/news') }}/{{$news->id}}/')"> <i class="fa fa-facebook"></i></a></li>
			<li><a onclick="Share.twitter('{{ url('/news') }}/{{$news->id}}','Агентство недвижимости ЛЕНДЛОРД')"><i class="fa fa-twitter"> </i></a></li>
		</ul>
	</div>
	<img src="{{url('/images/placeholders')}}/news{{$news->id}}.jpg">
	
	<div class="ll-company-news-page__text">
		<p>{!!$news->content!!}</p>
    </div>
@else
	<h1>Новость не найдена</h1>
@endif
</div>


<script>
Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		url  = 'http://vkontakte.ru/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		Share.popup(url);
	},
	odnoklassniki: function(purl) {
		url  = 'https://connect.ok.ru/offer?';
		url += 'url='    + encodeURIComponent(purl);
		Share.popup(url);
	},
	facebook: function(purl) {
		url  = 'https://www.facebook.com/sharer/sharer.php?';
		url += 'u='      + encodeURIComponent(purl);
		Share.popup(url);
	},
	twitter: function(purl, ptitle) {
		url  = 'http://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.popup(url);
	},
	popup: function(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
	}
};
</script>