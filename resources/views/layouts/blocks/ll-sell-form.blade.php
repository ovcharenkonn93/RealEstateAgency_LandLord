<div class="ll-sell-form">
	<form>
		<div class="col-md-6 col-md-offset-3 text-center">
			<div class="ll-sell-form__radio-buttons" id="ll-sell-form__radio-buttons">
				<div class="ll-sell-form__radio-button">
					<input type="radio" name="sale-or-rent" value="sale" id="ll-sell-form__radio-sale" checked />
					<label for="ll-sell-form__radio-sale">Продажа</label>
				</div>
				<div class="ll-sell-form__radio-button">
					<input type="radio" name="sale-or-rent" value="rent" id="ll-sell-form__radio-rent"  />
					<label for="ll-sell-form__radio-rent">Аренда</label>
				</div>
			</div>	
		</div>
		
		<div class="col-md-12">
			<div class="col-md-6 ll-sell-form__object">
				<p class="header_txt">
					Описание объекта
				</p>
				<label for="type">Тип:</label>
				<select name="type" id="type" class="ll-sell-form__type-select">
					<option value="">Квартира</option>
					<option value="">Участок</option>
				</select>
				<label for="floor">Этаж:</label>
					<input type="text" class="ll-sell-form__input-half">
					<label for="floor" class="ll-sell-form__label-half">из</label>
					<input type="text" class="ll-sell-form__input-half">
				<label for="area">Площадь:</label>
					<input type="text" class="ll-sell-form__input-half" placeholder="общая">
					<label for="area" class="ll-sell-form__label-half"></label>
					<input type="text" class="ll-sell-form__input-half" placeholder="жилая">
				<label for="rooms">Комнат:</label>
				<select name="rooms" id="rooms" class="ll-sell-form__rooms-select">
					<option value="">1</option>
					<option value="">2</option>
					<option value="">3</option>
				</select>
			</div>	
			<div class="col-md-6 ll-sell-form__address">
				<p class="header_txt">
					Адрес
				</p>
				<label for="city">Город:</label>
				<select name="city" id="city" class="ll-sell-form__city-select">
					<option value="">Город</option>
				</select>
				<label for="street">Улица:</label>
				<select name="street" id="street" class="ll-sell-form__street-select">
					<option value="">Вишневая</option>
					<option value="">Абрикосовая</option>
					<option value="">Свободы</option>
				</select>
				<label for="household">Дом:</label>
					<input type="text">
				<label for="flat">Кварира:</label>
					<input type="text">
			</div>	
		</div>	
		<div class="col-md-12 margin-top-30px">
			<div class="col-md-6 ll-sell-form__additionally">

				<p class="header_txt">
					Дополнительно
				</p>
							<div class="llg-input-check additional_check">
								<input type="checkbox" id="ll-sell-form__additional__checkbox-elevator" name="elevator" value="">
								<label class="additional_checkbox" for="ll-sell-form__additional__checkbox-elevator">Лифт</label>
							</div>
							<div class="llg-input-check additional_check">
								<input type="checkbox" id="ll-sell-form__additional__checkbox-loggia" name="loggia" value="">
								<label class="additional_checkbox" for="ll-sell-form__additional__checkbox-loggia">Мусоропровод</label>
							</div>
							<div class="llg-input-check additional_check">
								<input type="checkbox" id="ll-sell-form__additional__checkbox-garbage_chute" name="garbage_chute" value="">
								<label class="additional_checkbox" for="ll-sell-form__additional__checkbox-garbage_chute">Лоджия</label>
							</div>
							<div class="llg-input-check additional_check">
								<input type="checkbox" id="ll-sell-form__additional__checkbox-balcony" name="balcony" value="">
								<label class="additional_checkbox" for="ll-sell-form__additional__checkbox-balcony">Балкон</label>
							</div>
							<div class="llg-input-check additional_check">
								<input type="checkbox" id="ll-sell-form__additional__checkbox-exchange" name="exchange" value="">
								<label class="additional_checkbox" for="ll-sell-form__additional__checkbox-exchange">Обмен</label>
							</div>
							<div class="llg-input-check additional_check">
								<input type="checkbox" id="ll-sell-form__additional__checkbox-ipoteca" name="ipoteca" value="">
								<label class="additional_checkbox" for="ll-sell-form__additional__checkbox-ipoteca">Ипотека</label>
							</div>

			</div>

			<div class="col-md-6 ll-sell-form__photo_and_video">	
					<p class="header_txt">
						Фото и видео
					</p>
					<div class="block_line">
						<p class="label_for_upload_img">
						Фото:
						</p>
							<div class="img_load">
							<a href="#" class="img_stub">
								<span class="plus_in_circle">+</span>
							</a>
						</div>
					</div>
					
					<div class="margin-top-20px">
						<label for="video">Видео:</label>
						<input type="text" id="video_url" name="video-url" placeholder="Ссылка на видео в YouTube">
					</div>
			</div>	
			<div class="col-md-12 ll-sell-form__additionally-description">
				<p class="header_txt_1">
					Описание
				</p>
				<textarea name="additionally-description_text" id="ll-sell-form__additionally-description_text" class="additionally_description_text" cols="30" rows="10"></textarea>
				<p class="additionally-description_footer_text">Впишите сюда дополнительные сведения об объекте, условия оплаты и любую другую информацию, которая поможет вашим потенциальным покупателям принять решение о покупке</p>
			</div>	
		</div>	

		<div class="col-md-12 ll-sell-form__contacts">
			<p class="header_txt">
				Контакты
			</p>
				<label for="contact_person">Контактное лицо:</label>
				<input type="text" name="contact_person" id="contact_person">

				<label for="contact_person_tel">Телефон или email:</label>
				<input type="text" name="contact_person_tel" id="contact_person_tel">
		</div>
		<div class="col-md-12 text-center ll-sell-form__price">
			<label for="sell-form-price">Стоимость:</label>
			<select name="cost" id="sell-form-money">
					<option value="">Руб.</option>
			</select>
			<input type="text" name="sell-form-price" id="sell-form-price">
		</div>
		
		<div class="col-md-12 text-center">
			<input value="Разместить" type="submit">
		</div>
	</form>
</div>