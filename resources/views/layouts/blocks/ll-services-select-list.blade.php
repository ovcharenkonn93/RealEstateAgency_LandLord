@section('services-select-list')
    <!-- class="selectpicker" data-live-search="true" data-width="100%" title="Выберите услугу" -->
        <select class="ll-agent-contact-form__select-100">
             @if($site_services)
                @foreach($site_services as $site_service)
                    @if ($site_service->is_enabled === 1)
                        <option>{{ $site_service->title }}</option>
                    @endif
                @endforeach
            @endif
        </select>
@stop