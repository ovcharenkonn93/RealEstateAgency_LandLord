
<div class="ll-similar-offers">
    <h1 class="ll-similar-offers__header">Похожие предложения</h1>
    <div class="ll-similar-offers__body">
	@foreach($similars as $similar)
        <div class="ll-similar-offers__item">
            <div class="item-header-text">
            @if($similar[0]->r_type=='apartment'||$similar[0]->r_type=='rent')
				<h4>{{$similar[0]->type_apartment}} {{$similar[0]->area_general}} м<sup>2</sup>, {{$similar[0]->floor}}/{{$similar[0]->floor_all}} эт.</h4>
			@elseif($similar[0]->r_type=='households')
				<h4>{{$similar[0]->area_general}} м<sup>2</sup></h4>
			@endif
               <p><a href="{{url("info/".$similar[0]->id)}}">{{$similar[0]->locality}}</a></p>
            </div>
            <div class="item-image">
                <a href="{{url("info/".$similar[0]->id)}}">
                    <img src="{{ $similar[0]->cover_src}}" alt="">
                </a>
            </div>
            <div class="item-bottom-txt">
                <a href="{{url("info/".$similar[0]->id)}}">{{$similar[0]->price}} <span class="ll-data__currency">руб.</span></a>
            </div>
        </div>
	@endforeach
        
	
    </div>
</div>