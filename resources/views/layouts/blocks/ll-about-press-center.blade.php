<h2 class="about_header_txt text-center red-text">Пресс центр</h2>
<div class="ll-about-press-center">
    <div class="ll-about-press-center-map-50">
        <div class="press-center-map-img">
            <img src="images/placeholders/news0.jpg" alt="">
        </div>
        <div class="img-bottom_layer">
            <a href="">Открыть ЛЕНДЛОРД для себя</a>
        </div>
    </div>
    <div class="ll-about-press-center-map-50">
        <div class="ll-about-press-center-map-row_50">
            <div class="ll-press-center-row-block gray-background">
                <h4 class="white-text">С 8 марта!</h4>
                <p class="press-center-row_txt">
                    Агентство недвижимости ЛЕНДЛОРД поздравляет прекрасную половину человечества с чудесным весенним праздником – Международным женским днём! Примите наши самые искренние и теплые поздравления.
                </p>
            </div>
            <div class="ll-press-center-row-block">
                <div class="press-center-map-img">
                    <img src="images/placeholders/news6.jpg" alt="">
                </div>
                <div class="img-bottom_layer">
                    <h5>Достижения сотрудников</h5>
                </div>
            </div>
        </div>
        <div class="ll-about-press-center-map-row_50">
            <div class="ll-press-center-row-block">
                <div class="press-center-map-img">
                    <img src="images/placeholders/news6.jpg" alt="">
                </div>
                <div class="img-bottom_layer">
                    <h5>Достижения сотрудников</h5>
                </div>
            </div>
            <div class="ll-press-center-row-block gray-background">
                <h4 class="white-text">Весна распродаж!</h4>
                <p class="press-center-row_txt">
                    Весна распродаж в агентстве недвижимости ЛЕНДЛОРД! Вы уже слышали, что принимать быстрые решения выгодно? А  весной, во  время обновлений и перемен, это сделать можно еще и экономно! Вместе с
                </p>
            </div>
        </div>
    </div>
</div>