<div class="ll-search-form-panel_open"></div>

<div class="ll-search-form" id="ll-search-form"  data-spy="affix" data-offset-top="60" data-offset-bottom="400">
<form name="search-form" action="URL" method="post">
    <div class="ll-search-form__estate-type">
        <label class="ll-search-form__type-apartment">
            <input type="radio" name="estate-type" value="apartment" id ="ll-search-form__estate-type_apartment" checked /> Квартира
        </label>
        <label class="ll-search-form__type-household">
            <input type="radio" name="estate-type" value="household" id ="ll-search-form__estate-type_household"/> Дом
        </label>
        <label class="ll-search-form__type-land">
            <input type="radio" name="estate-type" value="land" id ="ll-search-form__estate-type_land"/> Участок
        </label>
    </div>
					
    <div class="ll-search-form__tabs-container" >
        <div class="ll-search-form__radio-buttons llg-for-appartment" id="ll-search-form__radio-buttons">
            <div class="llg-input-radio">
                <input type="radio" name="sale-or-rent" value="sale" id="ll-search-form__radio-sale" checked />
                <label for="ll-search-form__radio-sale">Продажа</label>
            </div>
            <div class="llg-input-radio">
                <input type="radio" name="sale-or-rent" value="rent" id="ll-search-form__radio-rent"  />
                <label for="ll-search-form__radio-rent">Аренда</label>
            </div>
        </div>

        <select required class="ll-search-form__city" id="ll-search-form__city" name="ll-search-form__city">
            <option value="" disabled selected>Город</option>
        </select>
        <select required class="ll-search-form__district" name="ll-search-form__district">
            <option value="" disabled selected>Район</option>
        </select>
        <select required class="ll-search-form__subdistrict" name="ll-search-form__subdistrict">
            <option value="" disabled selected>Ориентир</option>
        </select>
        <input type="text" class="ll-search-form__street" placeholder="Название улицы" />

        <div class="ll-search-form__cost">
            <div class="ll-search-form__minus" ></div>
            <div class="ll-search-form__plus" ></div>
            <div class="ll-search-form__diapason-label">Цена (<span class="ll-data__currency">руб.</span>):</div>
            <input type="text" placeholder="От" class="ll-search-form__min-value" value="" />
            <input type="text" placeholder="До" class="ll-search-form__max-value" value="" />
			<div class="ll-search-form__slider"></div>
        </div>

        <div class="ll-search-form__floor llg-for-appartment">
            <div class="ll-search-form__minus" ></div>
            <div class="ll-search-form__plus" ></div>
            <div class="ll-search-form__diapason-label">Этаж:</div>
            <input type="text" placeholder="От" class="ll-search-form__min-value" value="" />
            <input type="text" placeholder="До" class="ll-search-form__max-value" value="" />
			<div class="ll-search-form__slider"></div>
        </div>
		<div class="llg-input-check llg-for-appartment">
            <input type="checkbox" id="ll-search-form__checkbox-not-first" name="checkbox-not-first" value="" />
            <label for="ll-search-form__checkbox-not-first">Не первый</label>
        </div>

        <div class="llg-input-check llg-for-appartment">
            <input type="checkbox" id="ll-search-form__checkbox-not-last" name="checkbox-not-last" value="" />
            <label for="ll-search-form__checkbox-not-last">Не последний</label>
        </div>
        <div class="ll-search-form__total-area llg-for-appartment">
            <div class="ll-search-form__minus" ></div>
            <div class="ll-search-form__plus" ></div>
            <div class="ll-search-form__diapason-label">Площадь:</div>
            <input type="text" placeholder="От" class="ll-search-form__min-value" value="" />
            <input type="text" placeholder="До" class="ll-search-form__max-value" value="" />
			<div class="ll-search-form__slider"></div>
        </div>
        <div class="ll-search-form__sotki llg-for-household llg-for-land">
            <div class="ll-search-form__minus" ></div>
            <div class="ll-search-form__plus" ></div>
            <div class="ll-search-form__diapason-label">Количество соток земли:</div>
            <input type="text" placeholder="От" class="ll-search-form__min-value" value="" />
            <input type="text" placeholder="До" class="ll-search-form__max-value" value="" />
            <div class="ll-search-form__slider"></div>
        </div>
        <div class="ll-search-form__fasad llg-for-household llg-for-land">
            <div class="ll-search-form__minus" ></div>
            <div class="ll-search-form__plus" ></div>
            <div class="ll-search-form__diapason-label">Фасад (м):</div>
            <input type="text" placeholder="От" class="ll-search-form__min-value" value="" />
            <input type="text" placeholder="До" class="ll-search-form__max-value" value="" />
            <div class="ll-search-form__slider"></div>
        </div>

		<select id="SearchRooms" class="ll-search-form__apartment llg-for-appartment select" data-live-search="true" onchange="change_in_SearchRooms()" multiple="">
			<option value="" disabled selected>Комнаты</option>
		</select> 
		
         <select required class="ll-search-form__state llg-for-appartment">
          <option value="" disabled selected>Состояние</option>
        </select>
         <select required class="ll-search-form__walls-households llg-for-household">
          <option value="" disabled selected>Материал стен</option>
        </select>
         <select required class="ll-search-form__entry llg-for-household">
          <option value="" disabled selected>Вьезд (есть/нет)</option>
        </select>
         <select required class="ll-search-form__furniture llg-for-rent">
          <option value="" disabled selected>Мебель</option>
        </select>

        <div class="ll-search-form__additional">
            <div class="llg-input-check llg-for-appartment">
                <input type="checkbox" id="ll-search-form__checkbox-new-appartment" name="checkbox-new-appartment" value="" />
                <label for="ll-search-form__checkbox-new-appartment">Новостройки</label>
            </div>

            <div class="llg-input-check llg-for-rent">
                <input type="checkbox" id="ll-search-form__checkbox-rent-by-day" name="checkbox-rent-by-day" value="" />
                <label for="ll-search-form__checkbox-rent-by-day">Посуточная аренда</label>
            </div>

            <div class="llg-input-check llg-for-land">
                <input type="checkbox" id="ll-search-form__checkbox-communications" name="checkbox-communications" value="" />
                <label for="ll-search-form__checkbox-communications">Коммуникации</label>
            </div>

            {{--<label class="llg-for-household"><input type="radio" name="ownersheep" value="" />Целое домовладение</label>--}}
            {{--<label class="llg-for-household"><input type="radio" name="ownersheep" value="" />Доля</label>--}}
            {{--<label class="llg-for-household"><input type="radio" name="ownersheep" value="" checked/>Не важно</label>--}}

            <label class="llg-for-household"><input type="radio" name="yard" value="2" />Отдельный</label>
            <label class="llg-for-household"><input type="radio" name="yard" value="1" />Общий</label>
            <label class="llg-for-household"><input type="radio" name="yard" value="" checked/>Не важно</label>

            <label class="llg-for-land"><input type="radio" name="corner" value="" />Угловой</label>
            <label class="llg-for-land"><input type="radio" name="corner" value="" />Не угловой</label>
            <label class="llg-for-land"><input type="radio" name="corner" value="" checked />Не важно</label>
        </div>
{{--<p>-сроки сдачи --}}{{--Новостройки:--}}

<input type="submit" value="Обновить">


    </div>

</form>

</div>

<script>
$('#SearchRooms').selectpicker('refresh');

function change_in_SearchRooms()
{
var SearchRoomselect = new Array();
var i = 0;


$("#SearchRooms option:selected").each(function(){
        SearchRoomselect[i] = $(this).val();
        i++;
});

var SearchRooms = new Array();
var i = 0;

$("#SearchRooms option").each(function(){
        SearchRooms[i] = $(this).val();
        i++;
});

if(SearchRoomselect[0] == 0)
{


        SearchRooms.forEach(function(entry) {
            if(entry != "0")
            {
            $('#'+entry).prop('disabled',true);
            $('#'+entry).prop('selected',false);
            }
        });
        $('#SearchRooms').selectpicker('refresh');
}
if( (SearchRoomselect[0] != 0) )
{

        SearchRooms.forEach(function(entry) {
            $('#'+entry).prop('disabled',false);
        });
        $('#SearchRooms').selectpicker('refresh');
}   
}
</script>