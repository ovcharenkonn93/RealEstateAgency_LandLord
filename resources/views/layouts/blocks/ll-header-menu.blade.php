<div class="ll-header-menu">
		<nav class="navbar navbar-default navbar-static-top" role="navigation">				
					
					<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" style="height: 1px;">
						<ul class="nav navbar-nav col-md-12">
							<li id="button_buy" class="dropdown">
                                    <a class="dropdown-toggle" href="{{ url('/buy-advices') }}" data-toggle="dropdown" onclick="location.href = '{{ url('/buy-advices') }}'">Покупка</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ url('/steps-to-buy') }}">8 шагов к покупке недвижимости</a></li>
										<li><a href="{{ url('/why-buy-with-landlord') }}">Зачем покупать с ЛЕНДЛОРД?</a></li>
										<li><a href="{{ url('/mobile-application') }}">Мобильное приложение</a></li>
										<li><a href="{{ url('/analytics') }}">Исследование рынка</a></li>
									</ul>
							</li>

							<li id="button_sale" class="dropdown">
								<a class="dropdown-toggle" href="{{ url('/sell-advices') }}" data-toggle="dropdown" onclick="location.href = '{{ url('/sell-advices') }}'">Продажа</a>
								<ul class="dropdown-menu" role="menu">
									<!-- <li><a href="{{ url('/estate-valuation') }}">Оценить недвижимость</a></li> -->
									<li><a href="{{ url('/steps-to-sell') }}">8 шагов к продаже недвижимости</a></li>
									<li><a href="{{ url('/why-sell-with-landlord') }}">Зачем продавать с ЛЕНДЛОРД?</a></li>
									<li><a href="{{ url('/mobile-application') }}">Мобильное приложение</a></li>
									<li><a href="{{ url('/analytics') }}">Исследование рынка</a></li>
								</ul>
							</li>

                            <li id="button_services" class="dropdown"><a class="dropdown-toggle" href="{{ url('/services') }}" data-toggle="dropdown" onclick="location.href = '{{ url('/services') }}'">Услуги</a>
								<ul class="dropdown-menu" role="menu">
								    <li><a href="{{ url('/services-for-buyer') }}">Покупателю</a></li>
								    <li><a href="{{ url('/services-for-seller') }}">Продавцу</a></li>
								    <li><a href="{{ url('/services-for-tenant') }}">Арендатору</a></li>
								    <li><a href="{{ url('/services-for-owner') }}">Арендодателю</a></li>
									{{--<li><a href="{{ url('/mortgage') }}">Ипотека</a></li>--}}
								</ul>
							</li>

                            <li id="button_about" class="dropdown">
                                <a class="dropdown-toggle" href="{{ url('/about') }}" data-toggle="dropdown" onclick="location.href = '{{ url('/about') }}'">О нас</a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/news') }}">Новости</a></li>
                                    <li><a href="{{ url('/awards') }}">Награды</a></li>
                                    <li><a href="{{ url('/history') }}">История</a></li>
                                    <li><a href="{{ url('/structure') }}">Структура компании</a></li>
                                    <li><a href="{{ url('/leaders') }}">Лидеры компании</a></li>
                                    <li><a href="{{ url('/clients') }}">Отзывы клиентов</a></li>
                                </ul>
                            </li>

							<li id="button_career" class="dropdown" onclick="location.href = '{{ url('/career') }}'">
								<a class="dropdown-toggle" href="#" data-toggle="dropdown">Карьера</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('/education') }}">Обучение</a></li>
									<li><a href="{{ url('/vacancies') }}">Вакансии</a></li>
								</ul>
							</li>

						</ul>
					</div>
		</nav>
</div>

<script>
jQuery('ul.nav > li').hover(function() {
    jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
    jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
})
</script>