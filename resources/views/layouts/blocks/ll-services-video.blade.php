<div class="ll-services-video">
    <a class="ll-services-video-modal_link" data-href="{{$video_src}}">
        <div class="ll-services-video-play_icon"></div>
        <span class="ll-services-video-thumb first-video"></span>
    </a>
</div>
