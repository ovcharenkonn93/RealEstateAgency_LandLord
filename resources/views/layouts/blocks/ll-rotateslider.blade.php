<div class="ll-rotateslider">
    <h1 class="ll-awards__about">Знаки отличия фирмы</h1>
    <div class="ll-rotateslider__container" id="slider">
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/1000-best_enterprises_award.png') }}" alt="Диплом победителя четвертого всероссийского конкурса «1000 лучших предприятий и организаций России — 2003». Номинация «За высокую деловую активность и эффективную деятельность в 2003 году»."
            title="Диплом победителя четвертого всероссийского конкурса «1000 лучших предприятий и организаций России — 2003». Номинация «За высокую деловую активность и эффективную деятельность в 2003 году».">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/1000-best_enterprises_award_2003_2.jpg') }}" alt="Диплом победителя четвертого всероссийского конкурса «1000 лучших предприятий и организаций России — 2003». Номинация «За высокую деловую активность и эффективную деятельность в 2003 году»."
            title="Диплом победителя четвертого всероссийского конкурса «1000 лучших предприятий и организаций России — 2003». Номинация «За высокую деловую активность и эффективную деятельность в 2003 году».">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/arch-of-europe.png') }}" alt="Сертификат Arch of Europe и «Золотая арка Европы», Франкфурт,. 2004 г."
            title="Сертификат Arch of Europe и «Золотая арка Европы», Франкфурт,. 2004 г.">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/arenda_award_2005.jpg') }}" alt="Диплом «Профессиональное признание 2005» — номинант конкурса «Лучшая брокерская фирма на рынке аренды жилья 2005»"
            title="Диплом «Профессиональное признание 2005» — номинант конкурса «Лучшая брокерская фирма на рынке аренды жилья 2005»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/award_2003.jpg') }}" alt="Победитель конкурса «Лучшая брокерская фирма на рынке недвижимости 2003»"
             title="Победитель конкурса «Лучшая брокерская фирма на рынке недвижимости 2003»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/award_pro-2003.jpg') }}" alt="Диплом от газеты «Все для Вас», победитель в номинации «Профессионал 2003»"
            title="Диплом от газеты «Все для Вас», победитель в номинации «Профессионал 2003»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/best_brokers_awards_2005.jpg') }}" alt="Диплом «Профессиональное признание 2005» — номинант конкурса «Лучшая брокерская фирма на рынке ипотечных сделок 2005»"
            title="Диплом «Профессиональное признание 2005» — номинант конкурса «Лучшая брокерская фирма на рынке ипотечных сделок 2005»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/best_company_by_and_sale_award_2005.jpg') }}" alt="Диплом регионального конкурса «Риэлтор года Юга России 2005», победитель в номинации «Лучшая компания на рынке купли-продажи жилья»"
            title="Диплом регионального конкурса «Риэлтор года Юга России 2005», победитель в номинации «Лучшая компания на рынке купли-продажи жилья»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/best_company_in_market_of_mortgage_.jpg') }}" alt="Диплом регионального конкурса «Риэлтор года Юга России 2005», победитель в номинации «Лучшая компания на рынке ипотечного кредитования»"
            title="Диплом регионального конкурса «Риэлтор года Юга России 2005», победитель в номинации «Лучшая компания на рынке ипотечного кредитования»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/best_rieltor_2006.png') }}" alt="Диплом регионального конкурса «Риэлтор года Юга России 2006», победитель в номинации «Вклад в развитие рынка недвижимости»"
            title="Диплом регионального конкурса «Риэлтор года Юга России 2006», победитель в номинации «Вклад в развитие рынка недвижимости»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/brand_of_the_year_2005.jpg') }}" alt="«Бренд года 2005»" title="«Бренд года 2005»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/people_standart_award_2003.jpg') }}" alt="Диплом «Народный стандарт 2003»" title="Диплом «Народный стандарт 2003»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/professional-recognition-2007.jpg') }}" alt="Диплом «Профессиональное признание» — победитель в номинации «Лучшая брокерская фирма на рынке аренды жилья 2007»"
            title="Диплом «Профессиональное признание» — победитель в номинации «Лучшая брокерская фирма на рынке аренды жилья 2007»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/professional-recognition-2004.jpg') }}" alt="Диплом «Профессиональное признание» номинант конкурса «Лучшая брокерская фирма на рынке аренды жилья 2004»"
            title="Диплом «Профессиональное признание» номинант конкурса «Лучшая брокерская фирма на рынке аренды жилья 2004»">
        </div>
        <div class="ll-rotateslider__item">
            <img src="{{ url('/images/awards/trust_partner_award_2003.jpg') }}" alt="Диплом от газеты «Все для Вас», победитель в номинации «Самый надежный партнер 2003»"
            title="Диплом от газеты «Все для Вас», победитель в номинации «Самый надежный партнер 2003»">
        </div>

        <span class="arrow right js-rotateslider-arrow" data-action="next"><i class="fa fa-angle-right fa-3x"></i></span>
        <span class="arrow left js-rotateslider-arrow" data-action="prev"><i class="fa fa-angle-left fa-3x"></i></span>
    </div>

    <div class="slide-description {{$show_or_hidden}}">
    <h3 id="slide_description"></h3>
    </div>

</div>