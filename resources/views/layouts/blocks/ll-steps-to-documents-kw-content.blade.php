<div class="ll-steps-to-documents-framed">
	<ul id="carousel_text">
		<li class="ll-steps-to-documents_carousel-item">
			<div>
				<h2><span class="red-text">Шаг 1:</span> Принятие решения об<br>оформлении документов</h2>
				<p>Самый первый шаг&nbsp;— осознание того, что Вам действительно нужно оформить какие-либо документы. Надо сказать, что это очень важный и нужный этап. Причем, единственный который Вы делаете самостоятельно. Во всем остальном Вам готовы помочь наши специалисты.</p>
			</div>
		</li>
		<li class="ll-steps-to-documents_carousel-item">
			<div>
				<h2><span class="red-text">Шаг 2:</span> Консультация юриста</h2>
				<p>После того, как Вы твердо решили, что документы оформлять Вам необходимо, самое время обратиться за консультацией к специалисту. Кстати, у нас Вы сможете ее получить абсолютно бесплатно! </p>
			</div>
		</li>
		<li class="ll-steps-to-documents_carousel-item">
			<div>
				<h2><span class="red-text">Шаг 3:</span> Заключение договора</h2>
				<p>Предположим, консультация уже получена. Что же дальше? А далее с Вами заключается договор на оказание услуг по юридическому оформлению. По сути&nbsp;— это гарант того, что работа нашими юристами будет вестись до тех пор, пока она не приведет к желаемому результату. </p>
			</div>
		</li>
		<li class="ll-steps-to-documents_carousel-item">
			<div>
                <h2><span class="red-text">Шаг 4:</span> Сбор необходимых документов</h2>
                <p>После того, как договор с агентством заключен, наш сотрудник собирает все необходимые документы и приводит их в порядок (готовит тех. документацию, узаконивает перепланировку, вводит Вас в наследство и т.д.). Если Вы не планируете далее покупку/продажу недвижимости, то этот шаг может стать финальным. </p>
			</div>
		</li>
		<li  class="ll-steps-to-documents_carousel-item">
			<div>
                <h2><span class="red-text">Шаг 5:</span> Оформление документов к сделке</h2>
                <p>Если Вы планируете в дальнейшем приобрести или продать недвижимость, то тогда начинается еще один этап - оформление документов к сделке. Здесь Вам также пригодится оперативная помощь специалиста. На этом этапе Вы заключаете договор о намерениях, то есть, какой объект, на каких условиях и в какие сроки Вы хотите приобрести и, соответственно, какую цену и в каком порядке готовы заплатить продавцу. </p>
			</div>
		</li>
		<li class="ll-steps-to-documents_carousel-item">
			<div>
                <h2><span class="red-text">Шаг 6:</span> Сделка</h2>
                <p>Этот шаг включает в себя следующие пункты: заключение основного договора купли-продажи недвижимости, регистрация перехода права собственности, оплата госпошлины, сбор полного пакета документов, а также получение свидетельства о праве собственности и его замена в случае наличия ошибок и опечаток. Мы готовы предоставить Вам специально оборудованное помещение, безопасное и свободное от посторонних глаз, в котором Вы сможете спокойно подписать все необходимые документы и расписки, а также пересчитать деньги. </p>
			</div>
		</li>
		<li class="ll-steps-to-documents_carousel-item">
			<div>
                <h2><span class="red-text">Шаг 7:</span> Страхование</h2>
                <p>Переживаете, что в будущем сделка может быть аннулирована? На этот случай мы приберегли для Вас еще один шаг&nbsp;— страхование. По сути&nbsp;— это гарант того, что никто из сторон ни расторгнет договор. Почему это так важно? Часто право собственности приобретается в ходе сделки купли-продажи, но при этом крайне редко цепочка ее участников ограничивается только покупателем и продавцом. Случается, что объект переходит от одного собственника к другому несколько раз. В процессе таких переходов, вполне вероятно, могут возникнуть проблемы. Именно поэтому данный шаг является желательным.</p>
                <p>Наши специалисты юридической службы оформления обязательно Вам помогут на каждом этапе и найдут самое верное решение!</p>
			</div>
		</li>
	</ul>
</div>