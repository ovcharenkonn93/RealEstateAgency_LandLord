<div class="ll-steps-to-buy-toggle" id="ll-steps-to-buy-toggle">
	<div class="ll-steps-to-buy-toggle-bg">
		 <div class="ll-steps-to-buy-toggle-h1">
			<h1>8 шагов к покупке дома</h1>
		 </div>
		 
			<div class="ll-steps-to-buy-toggle-numbers">
				<ul class="nav nav-tabs  ll-steps-to-buy-toggle-numbers-tabs">
					<li class="active ll-steps-to-buy-toggle-tab-1"><a data-toggle="tab" href="#ll-steps-to-buy__step-1">1</a></li>
					<li class="ll-steps-to-buy-toggle-tab-2"><a data-toggle="tab" href="#ll-steps-to-buy__step-2">2</a></li>
					<li class="ll-steps-to-buy-toggle-tab-3"><a data-toggle="tab" href="#ll-steps-to-buy__step-3">3</a></li>
					<li class="ll-steps-to-buy-toggle-tab-4"><a data-toggle="tab" href="#ll-steps-to-buy__step-4">4</a></li>
					<li class="ll-steps-to-buy-toggle-tab-5"><a data-toggle="tab" href="#ll-steps-to-buy__step-5">5</a></li>
					<li class="ll-steps-to-buy-toggle-tab-6"><a data-toggle="tab" href="#ll-steps-to-buy__step-6">6</a></li>
					<li class="ll-steps-to-buy-toggle-tab-7"><a data-toggle="tab" href="#ll-steps-to-buy__step-7">7</a></li>
					<li class="ll-steps-to-buy-toggle-tab-8"><a data-toggle="tab" href="#ll-steps-to-buy__step-8">8</a></li>
				</ul>   
			</div>
	</div>
</div>