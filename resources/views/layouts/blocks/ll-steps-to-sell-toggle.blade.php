<div class="ll-steps-to-sell-toggle" id="ll-steps-to-sell-toggle">
	<div class="ll-steps-to-sell-toggle-bg">
		 <div class="ll-steps-to-sell-toggle-h1">
			<h1>7 шагов к продаже дома</h1>
		 </div>
		 
		<div class="ll-steps-to-sell-toggle-numbers">
			<ul class="nav nav-tabs ll-steps-to-sell-toggle-numbers-tabs">
				<li class="active ll-steps-to-sell-toggle-tab-1"><a data-toggle="tab" href="#ll-steps-to-sell__step-1">1</a></li>
				<li class="ll-steps-to-sell-toggle-tab-2"><a data-toggle="tab" href="#ll-steps-to-sell__step-2">2</a></li>
				<li class="ll-steps-to-sell-toggle-tab-3"><a data-toggle="tab" href="#ll-steps-to-sell__step-3">3</a></li>
				<li class="ll-steps-to-sell-toggle-tab-4"><a data-toggle="tab" href="#ll-steps-to-sell__step-4">4</a></li>
				<li class="ll-steps-to-sell-toggle-tab-5"><a data-toggle="tab" href="#ll-steps-to-sell__step-5">5</a></li>
				<li class="ll-steps-to-sell-toggle-tab-6"><a data-toggle="tab" href="#ll-steps-to-sell__step-6">6</a></li>
				<li class="ll-steps-to-sell-toggle-tab-7"><a data-toggle="tab" href="#ll-steps-to-sell__step-7">7</a></li>
			</ul>
		</div>
	</div>
</div>