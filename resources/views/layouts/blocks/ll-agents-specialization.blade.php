<div class="ll-agents-specialization">
    <h2 class="text-center">Поиск агента</h2>
    <div class="ll-search-agents-by-specialization">
        <div class="ll-search-agents-by-specialization-title">
        <h3 class="text-center">Специализация агента</h3>
        </div>

        <div class="ll-search-agents-form">
        <form>
            <div class="ll-search-agents-form-buttons">
                <div class="llg-input-check ll-search-agents-form-input-check"> <input type="checkbox" id="ll-search-agents-form-apartment" name="apartment" value="">
                    <label for="ll-search-agents-form-apartment">Квартира</label>
                </div>
                <div class="llg-input-check ll-search-agents-form-input-check">
                    <input type="checkbox" id="ll-search-agents-form-house" name="house" value="">
                    <label for="ll-search-agents-form-house">Дом/Участок</label>
                </div>
                <div class="llg-input-check ll-search-agents-form-input-check">
                    <input type="checkbox" id="ll-search-agents-form-commerce" name="commerce" value="">
                    <label for="ll-search-agents-form-commerce">Коммерция</label>
                </div>
                <div class="llg-input-check ll-search-agents-form-input-check">
                    <input type="checkbox" id="ll-search-agents-form-rent" name="rent" value="">
                    <label  for="ll-search-agents-form-rent">Аренда</label>
                </div>
            </div>
            
            <div class="ll-search-agents-form-search_button">
                <a href="" class="llg-button">Поиск</a>
            </div>
        </form>
        </div>
    </div>
</div>