<div class="ll-home-carousel">
    <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="images/history/history1.jpg" alt="" title="">
<!--
                <div class="carousel-caption">
                    <h3>Город: Ростов</h3>
                    <p>Площадь: 200 кв.м </p>
                    <p>Цена: 2 000 000 <span class="ll-data__currency">руб.</span> </p>
                </div>
-->
            </div>

            <div class="item">
                <img src="images/history/history2.jpg" alt="" title="">
            </div>
            <div class="item">
                <img src="images/history/history3.jpg" alt="" title="">
            </div>
            <div class="item">
                <img src="images/history/history4.jpg" alt="" title="">
            </div>
            <div class="item">
                <img src="images/history/history5.jpg" alt="" title="">
            </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
            <i class="fa fa-angle-left fa-3x ll-home-carousel-control" aria-hidden="true"></i>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
            <i class="fa fa-angle-right fa-3x ll-home-carousel-control" aria-hidden="true"></i>
        </a>
    </div>
<div class="{{$show_hide_variable}}">
    @include('layouts.blocks.ll-search-form-home')
    @include('layouts.blocks.ll-download-mobile-application')
</div>

</div>

