<div class="ll-mobile-application-features">
	<h1 class="text-center red-text">Особенности</h1>
<div class="row">
		<div class="ll-mobile-application-features-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-mobile-map.png') }}" alt="Динамическая карта" title="Динамическая карта">
					</td>
					<td>
						<p><b>Динамическая карта</b></p>
						<p>Добавьте Pinpoint или нарисуйте периметр, чтобы увидеть близлежащие дома для продажи, открытые дома или свойства на лету. </p>
					</td>
				</tr>
			</table>
		</div>
		<div class="ll-mobile-application-features-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-mobile-time.png') }}" alt="Доступ в любое время" title="Доступ в любое время">
					</td>
					<td>
						<p><b>Доступ в любое время</b></p>
						<p>Сохранить поиск и списки, в том числе заметки или фотографии добавлены собственности, на всех устройствах. </p>
					</td>
				</tr>
			</table>
		</div>

</div>

	<div class="row">

		<div class="ll-mobile-application-features-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-mobile-search.png') }}" alt="Пользовательские поисковые запросы" title="Пользовательские поисковые запросы">
					</td>
					<td>
						<p><b>Пользовательские поисковые запросы</b></p>
						<p>Добавить фильтры поиска, как цена, район, количество мест, открытых домов, и многое другое.</p>
					</td>
				</tr>
			</table>
		</div>
		<div class="ll-mobile-application-features-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-mobile-agent.png') }}" alt="Найти агента" title="Найти агента">
					</td>
					<td>
						<p><b>Найти агента</b></p>
						<p>Не уже работает с агентом? Найти агента и отправить запросы на недвижимость мгновенно.</p>
					</td>
				</tr>
			</table>
		</div>

</div>

	<div class="row">

		<div class="ll-mobile-application-features-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-mobile-info.png') }}" alt="Получить информацию" title="Получить информацию">
					</td>
					<td>
						<p><b>Получить информацию</b></p>
						<p>Посмотрите, что школы и другие интересные объекты поблизости. </p>
					</td>
				</tr>
			</table>
		</div>
		<div class="ll-mobile-application-features-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-mobile-share.png') }}" alt="Поделиться информацией" title="Поделиться информацией">
					</td>
					<td>
						<p><b>Поделиться информацией</b></p>
						<p>Легко поделиться дома через текст, электронную почту, Facebook, или Twitter. </p>
					</td>
				</tr>
			</table>
		</div>

	</div>

	<div class="row">
		<div class="ll-mobile-application-features-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-mobile-photo.png') }}" alt="Полноэкранная фотогалерея" title="Полноэкранная фотогалерея">
					</td>
					<td>
						<p><b>Полноэкранная фотогалерея</b></p>
						<p>Прокрутка яркие, полноэкранные фотографии домов.</p>
					</td>
				</tr>
			</table>
		</div>
		<div class="ll-mobile-application-features-item">
			<table>
				<tr>
					<td>
						<img src="{{ url('/images/icons/icon-mobile-calc.png') }}" alt="Рассчитать" title="Рассчитать">
					</td>
					<td>
						<p><b>Рассчитать</b></p>
						<p>Бесплатный ипотечный калькулятор для оценки ипотечного кредита и ежемесячные платежи.</p>
					</td>
				</tr>
			</table>
		</div>

	</div>


</div>