<div class="ll-steps-why-documents-with-landlord">
    <h2>7 шагов к оформлению документов</h2>
    <img src="images/visuals/steps-to-documents-teaser.jpg" alt=""  title="">
    <p class="margin-top-20px">После того, как Вы твердо решили, что Вам необходимо оформлять документы, самое время обратиться за консультацией к специалисту. Кстати, у нас Вы сможете ее получить абсолютно бесплатно!
    <p><a href="steps-to-documents" class="llg-button-detail">Подробнее</a>
</div>