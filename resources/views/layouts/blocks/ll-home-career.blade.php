<div class="ll-home-career">
	<div class="ll-home-career-h text-center">
		<h2><a href="{{ url('/career') }}">Найди для себя карьеру с ЛЕНДЛОРД!</a></h2>
	</div>
	<div class="ll-home-career-button text-center">
		<a href="{{ url('/career') }}" class="llg-button">Присоединиться</a>
	</div>
</div>