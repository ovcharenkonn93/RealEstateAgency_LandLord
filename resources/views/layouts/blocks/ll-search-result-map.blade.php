<div id="ll-search-result-map" class="ll-search-result-map"></div>
<script>
	$(document).ready(function(){
		$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
			google.maps.event.trigger(map, 'resize');
		});
	});
	function initMap(){
	console.log('Запустилась карта');
		map = new google.maps.Map(document.getElementById('ll-search-result-map'), {
			center: {lat: 48.0064132, lng: 37.7776296},
			zoom: 10,
			disableDefaultUI: true,
			zoomControl: true,
			mapTypeControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		latlngbounds = new google.maps.LatLngBounds();
		infowindow=new google.maps.InfoWindow();
		console.log('{{url('images/interface/m')}}');
		mc = new MarkerClusterer(map, [], {imagePath: '{{url('images/interface/m')}}'});
		map_flag=true;
		if(!point_flag){
			/*marks.forEach(function(mark){
				mark.setMap(map);
			});*/
			sendForm();
		}
	}
	//$('ll-toggle__tabs-map').click(initMap());
	//$(document).ready(initMap);
	initMap();
</script>