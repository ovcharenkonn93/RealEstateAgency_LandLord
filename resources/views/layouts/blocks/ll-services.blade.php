<div class="col-md-12 ll-services">

    {{--<p>Наша компания предоставляет своим клиентам полный комплекс риелторских услуг.--}}
    {{--Благодаря комплексному подходу, Вы сможете решить весь спектр вопросов, которые возникают при заключении сделок на рынке недвижимости.--}}
    {{--Это позволит Вам в значительной мере сэкономить не только время, но и средства. В итоге, Вы получите необходимые  услуги и при этом будете иметь не только время, но и средства.--}}
    {{--В итоге, Вы получите необходимые услуги и при этом будете иметь на руках полный пакет документов, прошедший многоступенчатую экспертизную проверку.</p>--}}
    {{--<p> И главное: обо всех тарифах на услуги нашей компании Вы будете знать заранее, а по ее получении Вам будет выдан&nbsp;чек. </p>--}}

    <h2 class="text-center red-text">Услуги, предоставляемые нашей компанией:</h2>
        
	<div class="col-md-6 ll-services-block">
		<img alt="" src="{{ url('/images/icons/icon-services-for-buyer.png') }}" />
		<a href="{{ url('/services-for-buyer') }}">Услуги для покупателя</a>
	</div>  
    <div class="col-md-6 ll-services-block">		
		<img alt="" src="{{ url('/images/icons/icon-services-for-seller.png') }}" />
		<a href="{{ url('/services-for-seller') }}">Услуги для продавца</a>
  	</div>
    <div class="col-md-6 ll-services-hr">		
		<hr>
  	</div>
    <div class="col-md-6 ll-services-hr">		
		<hr>
  	</div> 		
    <div class="col-md-6 ll-services-block">		
		<img alt="" src="{{ url('/images/icons/icon-services-for-tenant.png') }}" />
		<a href="{{ url('/services-for-tenant') }}">Услуги для арендатора</a>
  	</div> 
    <div class="col-md-6 ll-services-block">		
		<img alt="" src="{{ url('/images/icons/icon-services-for-owner.png') }}" />
		<a href="{{ url('/services-for-owner') }}">Услуги для арендодателя</a>
  	</div>     		        
</div>



