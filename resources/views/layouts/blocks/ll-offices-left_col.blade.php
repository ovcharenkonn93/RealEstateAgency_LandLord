<div class="ll-offices-left_col">
			<ul>
				<li>
					<b>Выберите город:</b>
					<br>
					<select class="city" autocomplete="off" disabled="disabled">
						<option selected="true" disabled="disabled">Выбрать город</option>
					@foreach($cities as $city)
						<option value={{$city->id}}>
							{{$city->city}}
						</option>
					@endforeach
					</select>
				</li>
				<li>
					<b class="address-st" hidden="true">Адрес:</b>
					<div class="address"></div>
				</li>
				<li>
					<b class="telephone-st" hidden="true">Телефон:</b>
					<div class="telephone"></div>					
				</li>	
				<li>
					<b class="work_time-st" hidden="true">Время работы:</b>
					<div class="work_time"></div>				
				</li>					
			</ul>
</div>
