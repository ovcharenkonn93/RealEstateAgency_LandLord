<div class="ll-clients">
@foreach($reviews as $review)
    <div class="ll-clients__note">
        <div class="ll-clients__avatar text-center">
            <div class="client-avatar">
                <img class="img_avatar" src={{ url($review->avatar)}} alt="">
            </div>
            <p class="ll-clients__client-name text-center">{{$review->name}}</p>
        </div>
        <div class="client_note-body">
            <div class="client_note-data">
                <p class="note-data" ><time datetime="{{$review->date}}">{{$review->date}}</time></p>
            </div>
            <div class="note_text">
			{{$review->text}}
            </div>
        </div>
    </div>	
@endforeach
</div>



