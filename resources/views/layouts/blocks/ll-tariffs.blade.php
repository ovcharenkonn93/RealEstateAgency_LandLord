<div class="ll-tariffs">
    <h2>Тарифы</h2>
    <img src="images/visuals/tariffs.jpg" alt=""  title="">
    <ul>
        <li><a href="{{ url('/services-for-buyer') }}">Тарифы для покупателя</a></li>
        <li><a href="{{ url('/services-for-seller') }}">Тарифы для продавца</a></li>
        <li><a href="{{ url('/services-for-tenant') }}">Тарифы для арендатора</a></li>
        <li><a href="{{ url('/services-for-owner') }}">Тарифы для арендодателя</a></li>
    </ul>
</div>