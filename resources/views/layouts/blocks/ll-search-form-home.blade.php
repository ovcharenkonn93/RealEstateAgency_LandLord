<div class="ll-search-form-home col-md-12">
		<form id="ll-search-form-home-page" name="search-form" action="search" method="post">
			<div class="row">
				<div class="ll-search-form-home__radio-buttons" id="ll-search-form-home__radio-buttons">
					<div class="ll-search-form-home__radio-tab" >
								<input type="radio" name="option" id="ll-search-form-home__radio-buy" checked />
								<label for="ll-search-form-home__radio-buy">Купить</label>
					</div>
					<div class="ll-search-form-home__radio-tab" >
								<input type="radio" name="option" id="ll-search-form-home__radio-sale"  />
								<label for="ll-search-form-home__radio-sale">Продать</label>
					</div>
					<div class="ll-search-form-home__radio-tab" >
								<input type="radio" name="option" id="ll-search-form-home__radio-rent"  />
								<label for="ll-search-form-home__radio-rent">Снять</label>			
					</div>
					<div class="ll-search-form-home__radio-tab" >
								<input type="radio" name="option" id="ll-search-form-home__radio-for-rent"  />
								<label for="ll-search-form-home__radio-for-rent">Сдать</label>			
					</div>
				</div>
			</div>

			<div class="row">
			<div class="col-sm-2 col-xs-2">
				<div class="ll-search-form-home__type">
					   <select class="ll-search-form-home__select selectpicker" data-width="100%" title="Тип" id="ll-search-form-home__select-type">
							<option value="apartment">Квартира</option>
							<option value="new">Новостройки</option>
							<option value="households">Домовладения</option>
							<option value="land">Участки</option>
							<option value="commercial">Коммерция</option>
							<option value="exclusive">Эксклюзив</option>
						</select>
				</div>
				</div>
				<div class="col-sm-3 col-xs-3">
				<div class="ll-search-form-home__city" >
					   <select required class="ll-search-form-home__select ll-search-form__city" data-live-search="true" data-width="100%" title="Город" id="ll-search-form-home__select-city">
					        <option value="" disabled selected>Город</option>
					   </select>
				</div>
				</div>

                <div class="col-sm-3 col-xs-3 text-center">
					<div class="ll-search-form-home__commercial-type" >
						   <select class="ll-search-form-home__select ll-search-form__commercial-type selectpicker" data-width="100%" title="Профиль" id="ll-search-form-home__select-commercial-type"></select>
					</div>
					<div class="ll-search-form-home__rooms" >
						<div class="ll-search-form-home__checkbox-buttons-rooms" id="ll-search-form-home__checkbox-buttons-rooms">

							<input type="checkbox" value="1" name="option-rooms" id="ll-search-form-home__checkbox-1_room"  />
							<label for="ll-search-form-home__checkbox-1_room">1</label>

							<input type="checkbox" value="2" name="option-rooms" id="ll-search-form-home__checkbox-2_rooms"  />
							<label for="ll-search-form-home__checkbox-2_rooms">2</label>

							<input type="checkbox" value="3" name="option-rooms" id="ll-search-form-home__checkbox-3_rooms"  />
							<label for="ll-search-form-home__checkbox-3_rooms">3</label>

							<input type="checkbox" value="4" name="option-rooms" id="ll-search-form-home__checkbox-more_rooms"  />
							<label for="ll-search-form-home__checkbox-more_rooms">4+</label>

						</div>
						<div class="ll-search-form-home__rooms-text text-center">
							<p>Комнат</p>
						</div>
					</div>
				</div>

				<div class="col-sm-2 col-xs-2">
				<div class="ll-search-form-home__price">
                    <input type="text" class="ll-search-form-home__select selectpicker" data-width="100%" placeholder="Цена" title="Цена" id="ll-search-form-home__select-price">
				</div>
				</div>
				<div class="col-sm-2 col-xs-2">
				<div class="ll-search-form-home__button">
						<a class="llg-button-search"><i class="fa fa-search"></i>&nbsp;Поиск</a>
				</div>
				</div>

	
			</div>		
	
		</form>
</div>