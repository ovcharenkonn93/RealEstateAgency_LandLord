<div class ="ll-download-mobile-application">
	<div class="container">	
		<a class="ll-download-mobile-application-title" href="mobile-application">Приложение ЛЕНДЛОРД</a>
		<div class="ll-download-mobile-application-button">
		<a href="#"><img src="{{ url('/images/interface/google-small.png') }}" alt="Скачать для Android" title="Скачать для Android"></a>
		<a href="#"><img src="{{ url('/images/interface/app-store-small.png') }}" alt="Скачать для IPhone и IPad" title="Скачать для IPhone и IPad"></a>
		</div>
	</div>
</div>	