<div class="ll-search-results-sorting">
    <form name="search-results-sorting" action="URL" method="post">
    <label>Сортировать</label>
        <select>
            <option value="date_create|desc">По дате добавления, сначала новые</option>
            <option value="date_create|desc">По дате добавления, сначала старые</option>
            <option value="price|asc">По цене, сначала дешевые</option>
            <option value="price|desc">По цене, сначала дорогие</option>
            {{--<option value="date_create / total_area / price / price_per_meter|asc / desc">По цене за кв.м., сначала дешевые</option>--}}
            {{--<option value="date_create / total_area / price / price_per_meter|asc / desc">По цене за кв.м., сначала дорогие</option>--}}
            <option value="total_area|asc">По площади, сначала меньшие</option>
            <option value="total_area|desc">По площади, сначала большие</option>
        </select>
    </form>
</div>