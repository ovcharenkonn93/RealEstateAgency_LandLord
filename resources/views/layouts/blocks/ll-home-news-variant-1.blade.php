<div class="ll-home-news-variant-1 col-md-12">

    <div class="col-md-4">
        <div class="ll-home-news-variant-1__block">
            <div class="ll-home-news-variant-1__photo">
                <img src="{{ url('/images/placeholders') }}/news3.jpg" alt="" title="" >
            </div>
            <div class="ll-home-news-variant-1__news-text">
                <p><strong>Лето суперцен в ЛЕНДЛОРД</strong></p>
                <p>	На календаре конец июня, а это значит, что большая часть лета еще впереди! Как на счет того, чтобы наслаждаться солнечными лучами в своей собственной квартире?</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="ll-home-news-variant-1__block">
            <div class="ll-home-news-variant-1__photo">
                <img src="{{ url('/images/placeholders') }}/news0.jpg" alt="" title="">
            </div>
            <div class="ll-home-news-variant-1__news-text">
                <p><strong>День отличника</strong></p>
                <p>	21 июня в нашем учебном центре прошел «День отличника» для детей сотрудников, посвященный успешному окончанию учебного года! </p>
            </div>
            <a class="ll-home-news-variant-1__more" href="{{ url('/news') }}">Подробнее</a>
        </div>
    </div>

    <div class="col-md-4">
        <div class="ll-home-news-variant-1__block">
            <div class="ll-home-news-variant-1__photo">
                <img src="{{ url('/images/placeholders') }}/news6.jpg" alt="" title="">
            </div>
            <div class="ll-home-news-variant-1__news-text">
                <p><strong>Tуры по новостройкам</strong></p>
                <p>	Как провести июньские праздники с максимальной пользой? Правильный ответ на этот вопрос нашли участники 12-го автобусного тура по новостройкам</p>
            </div>
            <a class="ll-home-news-variant-1__more" href="{{ url('/news') }}">Подробнее</a>
        </div>
    </div>

</div>