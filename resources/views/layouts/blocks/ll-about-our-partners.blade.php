<div class="ll-about-our-partners">
    <h2 class="about_header_txt text-center red-text">Наши партнеры</h2>
    <div class="ll-about-our-partners-photo_block">
<img src="{{ url('/images/partners/logo-akbarsbank.jpg') }}" alt="" title="">
<img src="{{ url('/images/partners/logo-sberbank.jpg') }}" alt="" title="">
<img src="{{ url('/images/partners/logo-sobinbank.jpg') }}" alt="" title="">
<img src="{{ url('/images/partners/logo-svyazbank.jpg') }}" alt="" title="">
<img src="{{ url('/images/partners/logo-vtb24.jpg') }}" alt="" title="">
    </div>
</div>