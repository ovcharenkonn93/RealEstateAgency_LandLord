<div class="ll-tariffs-for-owner">
    <h2>Стоимость услуг при сдаче недвижимости в аренду</h2>
    <img src="images/visuals/tariffs-for-owner.jpg" alt=""  title="">
    <dl>
        <dt>Квартиры на вторичном рынке</dt>
        <dd>без комиссии</dd>

        <dt>Домовладения / коттеджи</dt>
        <dd>без комиссии</dd>

        <dt>Коммерческая недвижимость</dt>
        <dd>70% от стоимости арендной платы</dd>
    </dl>
</div>