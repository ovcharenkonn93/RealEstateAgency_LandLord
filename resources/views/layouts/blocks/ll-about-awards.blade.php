<div class="ll-about-awards">
    <h2>Награды</h2>
    <img src="images/visuals/awards-teaser.jpg" alt="Награды" title="Награды">
    <p class="margin-top-20px">Агентство недвижимости ЛЕНДЛОРД имеет множество наград, полученных на профессиональных конкурсах за качество предоставляемых услуг. Мы рады продолжать совершенствоваться в нашей области!
    <p><a href="awards" class="llg-button-detail">Подробнее</a>
</div>