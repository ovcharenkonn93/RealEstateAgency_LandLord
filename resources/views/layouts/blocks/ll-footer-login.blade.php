@extends('app')

@section('content')
		<div class="ll-footer-login" >
					@if (count($errors) > 0)
						<div class="col-md-4 col-md-offset-4">
							<div class="alert alert-danger">
								Ошибка ввода.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					@endif
					
            <form class="form-horizontal" role="form" method="POST" action="{{ url('auth/login') }}">
                <ul>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <li>
                        <b><i class="red-text">Мой</i>Лендлорд</b>
                    </li>
                    <li>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </li>
                    <li>
                        <input type="password" class="form-control" name="password">
                    </li>
                    <li>
                        <input type="submit" value="Вход">
                    </li>
                    <li>
                        <a class="red-text" href="{{ url('/password/email') }}">Забыли пароль</a>
                    </li>
                </ul>
            </form>
		</div>

@endsection
