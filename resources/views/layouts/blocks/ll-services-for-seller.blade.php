<div class="ll-services-for-seller">
   <h2>Описание услуг:</h2>
    <div class="ll-services-for-category__step">
        <span class="ll-services-for-category__number">
            <img src="{{ url('images/interface/services-step-1.png') }}" alt="" title="">
        </span>
        <span class="ll-services-for-category__text">Поиск клиента на объект недвижимости</span>
        <span class="ll-services-for-category__image">
            <img src="{{ url('images/icons/icon-services-for-buyer-1.png') }}" alt="" title="">
        </span>
    </div>
    <div class="ll-services-for-category__step">
        <span class="ll-services-for-category__number">
            <img src="{{ url('images/interface/services-step-2.png') }}" alt="" title="">
        </span>
        <span class="ll-services-for-category__text">Составление договора купли-продажи</span>
        <span class="ll-services-for-category__image">
            <img src="{{ url('images/icons/icon-services-for-buyer-2.png') }}" alt="" title="">
        </span>
    </div>
    <div class="ll-services-for-category__step">
        <span class="ll-services-for-category__number">
            <img src="{{ url('images/interface/services-step-3.png') }}" alt="" title="">
        </span>
        <span class="ll-services-for-category__text">Помощь в подготовке документов</span>
        <span class="ll-services-for-category__image">
            <img src="{{ url('images/icons/icon-services-for-buyer-3.png') }}" alt="" title="">
        </span>
    </div>
    <div class="ll-services-for-category__step">
        <span class="ll-services-for-category__number">
            <img src="{{ url('images/interface/services-step-4.png') }}" alt="" title="">
        </span>
        <span class="ll-services-for-category__text">Сопровождение сделки</span>
        <span class="ll-services-for-category__image">
            <img src="{{ url('images/icons/icon-services-for-buyer-4.png') }}" alt="" title="">
        </span>
    </div>
    <div class="ll-services-for-category__step">
        <span class="ll-services-for-category__number">
            <img src="{{ url('images/interface/services-step-5.png') }}" alt="" title="">
        </span>

        <span class="ll-services-for-category__text">Страхование сделки</span>
        <span class="ll-services-for-category__image">
            <img src="{{ url('images/icons/icon-services-for-buyer-5.png') }}" alt="" title="">
        </span>
    </div>
</div>