<div class="ll-analytics">
	<h2 class="text-center">Динамика рынка недвижимости</h2>
	<div class="row ll-analytics-bg ">
		<div class="col-md-12 ll-analytics-top_bottom__block">
			<div class="container  text-center">
				<label for="ll-analytics-type">Тип данных:</label>
				<select required class="ll-analytics-type" id="ll-analytics-type" name="ll-analytics-type">
					<option value="" disabled selected>Радиальная</option>
				</select>
				<label for="ll-analytics-X">Ось X:</label>
				<select required class="ll-analytics-X" id="ll-analytics-X" name="ll-analytics-X">
					<option value="" disabled selected>Цена</option>
				</select>
				<label for="ll-analytics-Y">Ось Y:</label>
				<select required class="ll-analytics-Y" id="ll-analytics-Y" name="ll-analytics-Y">
					<option value="" disabled selected>Комнаты</option>
				</select>
				<label for="ll-analytics-Z">Ось Z:</label>
				<select required class="ll-analytics-Z" id="ll-analytics-Z" name="ll-analytics-Z">
					<option value="" disabled selected>Объекты</option>
				</select>				
			</div>	
		</div>
		<div class="col-md-12 ll-analytics-main_block">
			<div class="container">
				<div class="col-md-5">
					<div class="ll-analytics-left_column">
						<table class="ll-analytics-table_header">
							<tr>
								<th class="active">
									Показать цену
								</th>
								<th>
									Показать предложение
								</th>
							</tr>						
						</table>
						<table class="ll-analytics-table">
							<tr>
								<th>
									Тип недвижимости
								</th>
								<th>
									Средняя цена предложения
								</th>
								<th>
									Изменение на %
								</th>
							</tr>
							<tr>
								<td>
									1-комнатная
								</td>
								<td>
									45 000 руб/кв.м.
								</td>
								<td>
									<p class="red-text">47.8%</p>
								</td>
							</tr>
							<tr>
								<td>
									2-комнатная
								</td>
								<td>
									55 000 руб/кв.м.
								</td>
								<td>
									<p class="red-text">17.2%</p>
								</td>
							</tr>
							<tr>
								<td>
									3-комнатная
								</td>
								<td>
									45 000 руб/кв.м.
								</td>
								<td>
									<p class="red-text">75.8%</p>
								</td>
							</tr>
								<tr>
								<td>
									Многокомнатная
								</td>
								<td>
									85 000 руб/кв.м.
								</td>
								<td>
									<p class="red-text">1.7%</p>
								</td>
							</tr>						
						</table>
						<p class="ll-analytics-text">В Июле 2016-го объем предложения снизился на 65% и составил 10324 объектов. Возросла доля двухкомнатных квартир и составила 34%.</p>
					</div>
				</div>
				<div class="col-md-7">
					<div id="ct-chart-line"></div>
				</div>
			</div>	
		</div>
		<div class="col-md-12 ll-analytics-top_bottom__block">
			<div class="container  text-center">
				<select required class="ll-analytics-object_type" id="ll-analytics-object_type" name="ll-analytics-object_type">
					<option value="" disabled selected>Недвижимость</option>
				</select>
				<select required class="ll-analytics-city" id="ll-analytics-city" name="ll-analytics-city">
					<option value="" disabled selected>Город</option>
				</select>
				<select required class="ll-analytics-year_start" id="ll-analytics-year_start" name="ll-analytics-year_start">
					<option value="" disabled selected>2011</option>
				</select>
				<select required class="ll-analytics-year_end" id="ll-analytics-year_end" name="ll-analytics-year_end">
					<option value="" disabled selected>2016</option>
				</select>				
			</div>		
		</div>	
	</div>
</div>