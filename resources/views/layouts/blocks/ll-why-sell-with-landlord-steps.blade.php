<div class="ll-steps-why-buy-with-landlord">
    <h2>8 шагов к продаже недвижимости</h2>
    <img src="images/visuals/steps-to-sell-teaser.jpg" alt=""  title="">
    <p class="margin-top-20px">Продать недвижимость часто много сложнее, чем купить. Поэтому агент ЛЕНДЛОРД будет с Вами рядом на каждом шаге процесса, чтобы удостовериться, что Вы продаете в нужное время и за хорошую цену
    <p><a href="steps-to-sell" class="llg-button-detail">Подробнее</a>
</div>