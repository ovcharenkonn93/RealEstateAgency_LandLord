<div class="ll-vacancies">
	<div class="col-md-12 ll-vacancies-filter margin-top-20px">
		<form name="vacancies-by-name" action="URL" method="post">
			<div class="col-md-4 text-center margin-top-20px">		
				<label>Вакансия</label>
				<select>
					<option>Секретарь</option>
					<option>Агент</option>
				</select>
			</div>
			<div class="col-md-4 text-center margin-top-20px">	
				<label>Город</label>
				<select>
					<option>Все города</option>
				</select>
			</div>
			<div class="col-md-4 text-center margin-top-20px">
				<input type="submit" value="Поиск">
			</div>
		</form>
	</div>
	<div class="col-md-4 margin-top-20px text-center">
		<p class="red-text">Найдено 4 результата: </p>
	</div>
	<div class="col-md-6 col-md-offset-2 margin-top-20px text-center">
			<form name="vacancies-results-sorting" action="URL" method="post">
			<label>Сортировать</label>
				<select>
					<option>По дате</option>
					<option>По рейтингу</option>
				</select>
			</form>
	</div>
</div>