@section('title','Информация об объекте')
@section('description', ' ') 
@section('meta_keywords')<meta name="keywords" content=" "/>@stop

@include('layouts.header')
<div class="llg-wrapper llg-wrapper-toggle llg-wrapper-toggle__info">
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-toggle')
        </div>
    </div>
</div>

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__info">
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-estate-info')
        </div>
        <div class="row">
            @include('layouts.blocks.ll-estate-on-map')
        </div>
        <div class="row">
            @include('layouts.blocks.ll-ipoteka-calculator')
        </div>
        <div class="row">
            @include('layouts.blocks.ll-similar-offers')
        </div>
    </div>

</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')