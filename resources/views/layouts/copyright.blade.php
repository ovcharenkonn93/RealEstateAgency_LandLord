@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__copyright">
    <div class="container">
        <div class="row">

        <?php
        $imageURL = '../images/visuals/career.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/education.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/home-slider-1.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/home-slider-2.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/home-slider-3.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/mobile-application.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-analitics.jpg';
        $sourceURL = 'http://www.freepik.com/free-photo/close-up-of-executives-holding-a-report_867857.htm';
        $sourceName = 'Freepik.com';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = 'http://www.freepik.com/free-photo/close-up-of-executives-holding-a-report_867857.htm';
        $licenseName = 'Free for commercial use by crediting the author';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-buy-1.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-buy-2.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-buy-3.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-buy-4.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-buy-5.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-buy-6.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-buy-7.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-buy-8.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-sell-1.jpg';
        $sourceURL = 'http://kaboompics.com/one_foto/427/keys';
        $sourceName = 'KaboomPics';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = 'http://kaboompics.com/terms';
        $licenseName = 'Free';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-sell-2.jpg';
        $sourceURL = 'https://unsplash.com/photos/OQMZwNd3ThU';
        $sourceName = 'Unsplash';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = 'https://unsplash.com/license';
        $licenseName = 'Creative Commons Zero';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-sell-3.jpg';
        $sourceURL = 'https://unsplash.com/photos/5fNmWej4tAA';
        $sourceName = 'Unsplash';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = 'https://unsplash.com/license';
        $licenseName = 'Creative Commons Zero';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-sell-4.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-sell-5.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-sell-6.jpg';
        $sourceURL = 'https://unsplash.com/photos/DEdM9Vs6s8w/';
        $sourceName = 'Unsplash';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = 'https://unsplash.com/license';
        $licenseName = 'Creative Commons Zero';
        ?>
        @include('layouts.blocks.ll-copyright-grid')


        <?php
        $imageURL = '../images/visuals/steps-to-sell-7.jpg';
        $sourceURL = '?';
        $sourceName = '?';
        $authorURL = '?';
        $authorName = '?';
        $licenseURL = '?';
        $licenseName = '?';
        ?>
        @include('layouts.blocks.ll-copyright-grid')

        </div>
     </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')