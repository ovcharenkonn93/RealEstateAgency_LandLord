<?php /*@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop */?>

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-toggle llg-wrapper-toggle__agents">
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-toggle')
        </div>
    </div>
</div>

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__agents">
    <div class="container">
		<div class="row">
@include('layouts.blocks.ll-agents-specialization')
		</div>
		<div class="llg-panel">
			<div class="row">					 
				
					<div class="col-md-5" >
						@include('layouts.blocks.ll-search-results-limit')
					</div>
					<div class="col-md-7" >
						@include('layouts.blocks.ll-agents-sorting')
					</div> 
					
			</div>
			<div class="row" id="agents-list">
					@for ($i = 0; $i < count($agents); $i++)
						<div class="col-md-6">
							@include('layouts.blocks.ll-agents-grid',['i'=>$i, 'agent'=>$agents[$i]])
						</div>
					@endfor
			</div>
			<div class="row text-center">
				@include('layouts.blocks.ll-pagination')
			</div>
		</div>
	</div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')