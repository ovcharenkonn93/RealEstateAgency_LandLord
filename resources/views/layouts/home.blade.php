<div class="llg-wrapper llg-wrapper-main llg-wrapper-main-home">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.blocks.ll-home-without-carousel')
        </div>
    </div>

<div class="container">
    <div class="row">
    @include('layouts.blocks.ll-home-offers')
	</div>
</div>
    <div class="row">
		@include('layouts.blocks.ll-home-career')
    </div>

</div>
<div class="llg-wrapper llg-wrapper-main-home-news">
<h1 class="llg-news-splitter text-center">Новости и специальные предложения</h1> <!--Нужно куда-то перенести-->
    {{--<div class="llg-news-variant-1">1</div>--}}
    {{--<div class="llg-news-variant-2">2</div>--}}
    {{--<div class="llg-news-variant-3">3</div>--}}
    {{--<div class="llg-news-variant-4">4</div>--}}

<div class="container">
	<div class="row">
{{--		@include('layouts.blocks.ll-home-news-variant-1')--}}
		@include('layouts.blocks.ll-home-news-variant-2')
		{{--@include('layouts.blocks.ll-home-news-variant-3')--}}
		{{--@include('layouts.blocks.ll-home-news-variant-4')--}}
	</div>
</div>
</div>