@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__history">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'history','Header'=>'История компании'])

    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-history')
        </div>
    </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')