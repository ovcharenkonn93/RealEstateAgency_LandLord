<h2>Ипотека на новостройку</h2>
    <div class="mortgage new-building">
        <div>
            <div>
                <p>С нашей помощью вы можете получить ипотечный кредит на квартиру в строящемся доме у надежного застройщика аккредитованного банком, причем <strong>без комиссии агентству</strong>.
                    <br>
                </p>
            </div>

            <div class="cl"></div>

            <p>Зная все программы кредитования, предлагаемых банками Ростова-на-Дону, мы выберем для вас оптимальные условия кредитования и будем сопровождать вас до получения кредита.</p>
        </div>

        <div class="file-group bordered">
            <h2 class="text-center">Приобретаете квартиру с нашей помощью?
                <br>
                Все услуги по ипотеке — в ПОДАРОК!</h2>
            <img class="fr" src="/images/mortgage/new_building_2.jpg">
            <p>Поиск банка, оптимальной ипотечной программы, которая учитывала бы ваше финансовое положение, оформление пакета документов, в который входят справки с ограниченным сроком действия — вот неполный перечень того, с чем вам придется столкнуться при самостоятельном оформлении ипотечного кредита.
                <br>
                <strong>Мы предлагаем вам другой вариант:</strong>
                <br>
                <a class="underlined" href="/rnd/mortgage/">весь комплекс ипотечных услуг</a> вы  получите в подарок при заключении сделки купли--продажи объекта недвижимости с помощью нашего агентства. При этом мы поможем Вам с выбором желаемого объекта из нашей базы данных новостроек. Вы можете быть уверены в том, что найденная квартира обязательно будет соответствовать как вашим пожеланиям, так и требованиям банка.</p>
        </div>

        <h2 class="even" style="font-size: 16px;"><a class="underlined" href="/rnd/mortgage/documents">Ознакомиться с полным перечнем документов для рассмотрения заявления на получение ипотечного кредита</a></h2>

      
    </div>
