<h2>Ипотека для молодых семей</h2>
<h2 class="subtitle">Первоначальный взнос&nbsp;— 20%!</h2>
<div class="mortgage family">
    <img class="fl" src="/images/mortgage/family_1.jpeg">
    <div>
        <p class="first">В рамках программы «Обеспечение жильем молодых семей», государство безвозмездно предоставляет субсидию, размер которой зависит от состава семьи. При этом размер субсидии &nbsp;зависит от того, где именно будет приобретаться квартира (в г. Ростове-на-Дону или городах Ростовской области).</p>

        <br>   <p>Молодые семьи, получившие свидетельства на предоставление социальной выплаты, приобретают жилье на вторичном рынке. Для желающих купить жилье в новостройке следует учесть, что они смогут это сделать в том случае, если дом введен в эксплуатацию.</p>
    </div>

    <div class="cl"></div>

    <div class="file-group bordered">

        <div class="cl"></div>
        <h2 class="even" style="font-size: 16px;"><a class="underlined" href="/rnd/mortgage/documents">Ознакомиться&nbsp;с&nbsp;полным&nbsp;перечнем&nbsp;документов&nbsp;для&nbsp;рассмотрения&nbsp;заявления на&nbsp;получение&nbsp;ипотечного&nbsp;кредита</a></h2>
    </div>
</div>