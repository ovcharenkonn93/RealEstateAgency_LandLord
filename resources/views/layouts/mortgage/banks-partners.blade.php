<div class="right-side">
    <h2>Банки-партнеры</h2>
    
<!--    <style>
        #table213
        {
            border:1px;

        }
        #td1
        {

            background-color: #321102;
            cursor: pointer;

            -moz-transition: background-color 1s ease;
            -o-transition: background-color  1s ease;
            -webkit-transition: background-color 1s ease;
        }

        #td1:hover
        {

            background-color: #946542;
            color: black;
        }

        #td2
        {
            background-color: #946542;
            cursor: pointer;

            -moz-transition: background-color 1s ease;
            -o-transition: background-color  1s ease;
            -webkit-transition: background-color 1s ease;
        }

        #td2:hover
        {

            background-color: #b9916d;
            color: black;
        }

    </style>
    -->

    <div class="mortgage banks">
        <p>Агентство недвижимости ЛЕНДЛОРД сотрудничает со всеми банками Ростова-на-Дону. Со многими из них нас связывают давние партнерские отношения. Поэтому при поиске оптимальной программы кредитования у вас всегда есть самые широкие возможности для выбора. При этом  ипотечный кредит вы сможете получить на льготных условиях, предоставляемых банками нашим клиентам.</p>

        <p>
            <br>
        </p>

        <p>Наиболее выгодные ставки на сегодняшний день: </p>

        <p>1) ПАО «Сбербанк России»&nbsp;— от 11,4% для новостроек, от 12,5% для вторичного жилья.</p>
        <p>2) ПАО «ВТБ24»&nbsp;— от 13% годовых.</p>
        <p>3) ОАО КБ «Центр-Инвест» &nbsp;— от 12,5% годовых.</p>
        <br>
        <p>Другие банки предоставляют ипотечный кредит&nbsp;— от 15% годовых.</p>

        <p>
            <br>
        </p>

        <p> <img src="/upload/medialibrary/761/761c57b4b1d56dd3e8543cf0011bc445.jpg" title="логотип сбербанк.jpg" border="0" alt="логотип сбербанк.jpg" width="168" height="50">            <img src="/upload/medialibrary/6b9/6b9df2597be29c870a1382b62f66ef08.jpg" title="логотип втб24.jpg" border="0" alt="логотип втб24.jpg" width="168" height="50">            <img src="http://landlord.ru/upload/resize_cache/medialibrary/3b6/140_105_1/3b6ae5833e641075546b7eb895185379.png" title="логотип центр-инвест.jpg" border="0" alt="логотип центр-инвест.jpg" width="168" height="50"></p>

        <p><span style="font-size: 21px; font-weight: bold; text-align: center;">
      <br>
     </span></p>

       
    </div>
</div>