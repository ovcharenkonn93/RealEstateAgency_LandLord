@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__vacancies">

	<h2 class="text-center">Поиск вакансий</h2>
	
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-vacancies')
        </div>
    </div>

	<div class="container">
		<div class="row">
			@include('layouts.blocks.ll-vacancies-list')
		</div>
	</div>

</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')