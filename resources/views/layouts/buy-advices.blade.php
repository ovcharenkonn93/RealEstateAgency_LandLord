@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__buy-advices">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'buy-advices','Header'=>'Покупка Вашей квартиры'])

    <div class="container">
        <div class="row ll-buy-advices-one">
            <div class="col-md-8 white-bg margin-top-20px">
                @include('layouts.blocks.ll-buy-advices')
            </div>
            <div class="col-md-4 margin-top-20px ll-buy-advices-indents">
                    @include('layouts.blocks.ll-steps-to-buy-buttons')
                    @include('layouts.blocks.ll-steps-why-buy-with-landlord')
                    @include('layouts.blocks.ll-steps-analitics')
            </div>
        </div>

        <div class="row ll-buy-advices-two white-bg">
            @include('layouts.blocks.ll-buy-advices-two')
        </div>
		<div class="ll-buy-advices-two">
            <div class="col-md-4 margin-top-20px ll-buy-advices-indents">
                    @include('layouts.blocks.ll-steps-to-buy-buttons')
                    @include('layouts.blocks.ll-steps-why-buy-with-landlord')
                    @include('layouts.blocks.ll-steps-analitics')
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')