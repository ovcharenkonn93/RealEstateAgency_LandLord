		<footer id="footer">
			<div class="llg-wrapper llg-wrapper-footer">
			   <div class="container">
					<div class="row">
						<div class="col-md-7">
							@include('layouts.blocks.ll-footer-login')
						</div>
						<div class="col-md-5">
							@include('layouts.blocks.ll-footer-social-buttons')
						</div>
					</div>
					<div class="row">
						@include('layouts.blocks.ll-footer-seo-links')
					</div>
					<div class="row">
						@include('layouts.blocks.ll-copyright')
					</div>
				</div>
			</div>
		</footer>
	</body>
</html>