@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__steps-to-sell">

	<div class="ll-steps-to-sell-content">
		<div id="carousel">
			<div id="carousel_banner"><img alt="" src="{{ url('/images/visuals/steps-to-sell-8.jpg') }}" />
				<img alt="" src="{{ url('/images/visuals/steps-to-sell-1.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-sell-2.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-sell-3.jpg') }}" />		         
				<img alt="" src="{{ url('/images/visuals/steps-to-sell-4.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-sell-5.jpg') }}" />         
				<img alt="" src="{{ url('/images/visuals/steps-to-sell-6.jpg') }}" />
				<img alt="" src="{{ url('/images/visuals/steps-to-sell-7.jpg') }}" />
			</div>
		 
			<a title="Previous" id="prevCarou" href="#">&nbsp;</a>     
			<a title="Next" id="nextCarou" href="#">&nbsp;</a>
		</div>
		
		<!-- #carousel -->
		<div id="carousel_controls">
			<div id="carousel_overlay">
				<h1 class="red-text">8 шагов к продаже</h1>
				<div id="carousel_pagination">&nbsp;</div>
				<div class="clear_left">&nbsp;</div>
			</div>
		</div>
		<!-- #carousel_controls -->
		<div class="ll-steps-to-sell-content-item">
			<div class="ll-steps-to-sell-content-item_content">
				<div class="ll-steps-to-sell-content-main white-bg">
					@include('layouts.blocks.ll-steps-to-sell-kw-content')
				</div>
				
				<!-- main -->
				<div class="ll-steps-to-side">			 
					 @include('layouts.blocks.ll-steps-to-sell-buttons')
					 @include('layouts.blocks.ll-steps-why-sell-with-landlord')
					 @include('layouts.blocks.ll-steps-analitics')				 
				</div>

			</div>
		</div>
	</div>		

</div>

@include('layouts.footer')