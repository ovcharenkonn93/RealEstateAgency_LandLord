@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

    <?php
    $foundation_date=new DateTime("1994-02-01");
    $this_date=new DateTime("now");
    $interval=$foundation_date->diff($this_date);
    $head="";
    switch($interval->y%10)
    {
        case 1:
            $head='ЛЕНДЛОРД&nbsp;— '.$interval->y.' год с Вами';
            break;
        case 2:
        case 3:
        case 4:
            $head='ЛЕНДЛОРД&nbsp;— '.$interval->y.' года с Вами';
            break;
        default:
            $head='ЛЕНДЛОРД&nbsp;— '.$interval->y.' лет с Вами';
    }
    ?>

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__about">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'about','Header'=>$head])

    <div class="llg-wrapper llg-wrapper-main__about-company">
        <div class="container">
                <div class="col-md-8 margin-top-13px">
                    @include('layouts.blocks.ll-about-about-our-company-block')
                </div>
                <div class="col-md-4 margin-top-13px">
                    @include('layouts.blocks.ll-steps-to-buy-buttons')
                    @include('layouts.blocks.ll-about-awards')
                </div>
        </div>
    </div>

    <div class="ll-about-our_partners">
        <div class="container">
            @include('layouts.blocks.ll-about-our-partners')
        </div>
    </div>
        <div class="llg-wrapper llg-wrapper-splitter"></div>
    <div class="ll-about-press_center">
        <div class="container">
            @include('layouts.blocks.ll-about-press-center')
        </div>
    </div>

</div>
@include('layouts.blocks.ll-splitter')
@include('layouts.footer')