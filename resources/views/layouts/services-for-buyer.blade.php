@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__services-for-buyer">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'services-for-buyer','Header'=>'Услуги для покупателя'])

    <div class="container">
        <div class="row">
            <div class="col-md-8 white-bg margin-top-20px">
                @include('layouts.blocks.ll-services-select-list')
                @include('layouts.blocks.ll-services-for-buyer')
                @include('layouts.blocks.ll-services-additional-list')
                @include('layouts.blocks.ll-services-discounts')
            </div>
            <div class="col-md-4 margin-top-20px">
                    @include('layouts.blocks.ll-steps-to-buy-buttons')
                    @include('layouts.blocks.ll-tariffs-for-buyer')
                    @include('layouts.blocks.ll-steps-why-buy-with-landlord')
            </div>
        </div>
    </div>

</div>

@include('layouts.footer')