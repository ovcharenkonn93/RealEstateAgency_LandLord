@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="ll-offices container">

</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')