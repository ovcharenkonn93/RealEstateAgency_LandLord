@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__analytics">
    @include('layouts.blocks.ll-visual-in-header',['Page'=>'analytics','Header'=>'Аналитика'])
	
	<div class="row">		
		@include('layouts.blocks.ll-analytics')
	</div>
</div>

@include('layouts.footer')