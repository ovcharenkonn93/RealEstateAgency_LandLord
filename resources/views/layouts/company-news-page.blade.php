
@include('layouts.header')

<div class="ll-news-page">
    <div class="container">
        <div class="row">
           @include('layouts.blocks.ll-company-news-page')
        </div>
    </div>

    <div class="row">
       @include('layouts.blocks.ll-news-sigle__all')
    </div>

    <div class="container">
		<div class="row">
           @include('layouts.blocks.ll-news-sigle__comments-facebook')
        </div>
    </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')