@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-toggle llg-wrapper-toggle__favorites">
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-toggle')
        </div>
    </div>
</div>

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__search">
    <div class="container">
        <div class="row">
						 
			<div id="llg-panel__result-favorites" class="tab-pane fade in active llg-panel">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
						 @include('layouts.blocks.ll-search-results-limit')
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" >
						@include('layouts.blocks.ll-search-results-sorting')
					</div>
				</div>	
				<script type="text/javascript">
					var favorites=JSON.parse($.cookie("favorites"));
					if(!favorites||favorites.length==0)
					{
						$("#llg-panel__result-favorites").append("<h2 class=\"text-center\">Ничего не выбрано</h2>");
					}
					else
					{
						$("#llg-panel__result-favorites").append("<div class=\"data-list\" id=\"data-list-favorites\">");
						favorites.forEach(function(item){
							$.ajax({
								url: "{{url('/search-sb/get_estate_from_id')}}",
								type: "GET",
								data: "id="+item,
								success: function(response){
									$("#data-list-favorites").append(response);
									$("#fa-star-o"+item.toString()).removeClass("fa-star-o").addClass("fa-star  yellow-star");
								}
							})
						});
						$("#llg-panel__result-favorites").append("</div>");	
					}
				</script>
					<div class="row text-center">
						@include('layouts.blocks.ll-pagination')
					</div>
                
            </div>
        </div>
    </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')


