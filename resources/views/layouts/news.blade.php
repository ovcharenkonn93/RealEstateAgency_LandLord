@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__news">

    @include('layouts.blocks.ll-visual-in-header',['Page'=>'news','Header'=>'Новости'])

    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-news')
        </div>
    </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')