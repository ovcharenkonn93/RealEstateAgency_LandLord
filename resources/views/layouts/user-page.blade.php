@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-toggle llg-wrapper-toggle__user-page">
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-toggle')
        </div>
    </div>
</div>
    <div class="container">
        <div class="tab-content">
            <div id="llg-panel__tabs-profile" class="tab-pane fade in active llg-panel">
                @include('layouts.blocks.ll-user-page')
            </div>
        </div>
        <div class="tab-content">
            <div id="llg-panel__tabs-my-ads" class="tab-pane fade in active llg-panel">
                Мои объявления
            </div>
        </div>
    </div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')