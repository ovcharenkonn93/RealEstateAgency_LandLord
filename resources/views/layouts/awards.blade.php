@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__awards">
    @include('layouts.blocks.ll-visual-in-header',['Page'=>'awards','Header'=>'Награды и достижения компании',
    'Description'=>'Агентство недвижимости ЛЕНДЛОРД имеет множество наград, полученных на профессиональных конкурсах за качество предоставляемых услуг. Мы рады продолжать совершенствоваться в нашей области!'])
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-awards')
        </div>
    </div>
    <div class="row">
        @include('layouts.blocks.ll-rotateslider',['show_or_hidden'=>'show_slide_description'])
    </div>
</div>

@include('layouts.footer')