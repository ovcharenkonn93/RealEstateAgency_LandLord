@section('title','Информация об агенте')
@section('description', ' ') 
@section('meta_keywords')<meta name="keywords" content=" "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-toggle llg-wrapper-toggle__agent-info">
    <div class="container">
        <div class="row">
            @include('layouts.blocks.ll-toggle')
        </div>
    </div>
</div>

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__search">
    <div class="container">
	    <div class="row">
			<div class="col-md-12" >
				@include('layouts.blocks.ll-agent-info-card')
            </div>
			<div class="col-md-12 text-center" >
				<h2 class="red-text">Недвижимость, которой занимается агент</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center" >
                 @include('layouts.blocks.ll-search-form')
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 text-center tab-content" >
                <div id="llg-panel__result-list" class="tab-pane fade in active llg-panel">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
                             @include('layouts.blocks.ll-search-results-limit')
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" >
                             @include('layouts.blocks.ll-search-results-sorting')
                        </div>
                    </div>

                       @include('layouts.blocks.ll-search-result-list')
					   
                    @include('layouts.blocks.ll-pagination')
                </div>
                <div id="llg-panel__result-grid" class="tab-pane fade llg-panel">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
                             @include('layouts.blocks.ll-search-results-limit')
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" >
                             @include('layouts.blocks.ll-search-results-sorting')
                        </div>
                    </div>
					
                       @include('layouts.blocks.ll-search-result-grid')

                     @include('layouts.blocks.ll-pagination')
                </div>
                <div id="llg-panel__result-map" class="tab-pane fade llg-panel">
                     @include('layouts.blocks.ll-search-result-map')
                </div>

            </div>
        </div>
    </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')


<script>
//--------------------------Заполнение данных-----------------------------------------
    $.ajax(
    {
    	url: '../api',
    	type: 'GET',
    	data: {
    		type: 'agent',
    		id: '{{ $agentInfoId }}'
    	},
    	success: function(data){
            var agentinfo = [JSON.parse(data)];
            //console.log('agentinfo', agentinfo);
            var estate = agentinfo[0].realestate_assigned;
            FillSearchResult (agentinfo, '.ll-agent-card', 0);
			$(".ll-search-result-list").hide();
			$(".ll-search-result-grid").hide();
			$.each(estate, function(index, value){
                console.log('estate:', estate);
                console.log('estate[index]:', estate[index]);
                console.log('index:', index);
                console.log('value:', value);
                FillSearchResult (estateinfo, '#ll-search-result-list-item'+index, index);
                FillSearchResult (estateinfo, '#ll-search-result-grid-item'+index, index);
			});
    	}
    });

</script>

