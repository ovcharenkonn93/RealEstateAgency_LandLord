@section('title','Подключение объектов к комуникациям')
@include('layouts.header')

<div class="llg-wrapper-main__single-service">
<div class="container">
    @include('layouts.blocks.ll-headline-for-services-page',['caption'=>'Водоснабжение (канализация)','anchor_id' => 'water'])
    <div class="ll-single-service">
       <!-- <ul>
            <li> Получение технических условий.
                <p>Для подключения объекта к системе водоснабжения и канализации, Заказчику
                    (представителю по доверенности) необходимо обратиться в ОАО «ПО Водоканал» с
                    запросом о наличии технической возможности присоединения объекта к системам
                    водоснабжения и канализации и предоставления технических условий подключения объекта
                    к системе коммунальной инфраструктуры.</p>
                <p>При обращении Заказчику необходимо представить следующую информацию и документы:</p>
            </li>
            <li></li>
            <li></li>
        </ul>








-->
        <h3>1. Получение технических условий.</h3>
        <p>Для подключения объекта к системе водоснабжения и канализации, Заказчику
            (представителю по доверенности) необходимо обратиться в ОАО «ПО Водоканал» с
            запросом о наличии технической возможности присоединения объекта к системам
            водоснабжения и канализации и предоставления технических условий подключения объекта
            к системе коммунальной инфраструктуры.</p>
        <p>При обращении Заказчику необходимо представить следующую информацию и документы:</p>
        <ul class="ll-single-service__list-no-markers">
            <li>Наименование лица, направившего запрос, его местонахождение, почтовый адрес и номер
                контактного телефона;</li>
            <li>Нотариально заверенные копии учредительных документов, а также документы, подтверждающие
                полномочия лица, подписавшего запрос;</li>
            <li>Правоустанавливающие документы на земельный участок (для правообладателя земельного
                участка);</li>
            <li>Необходимые виды ресурсов, получаемых от сетей инженерно-технического обеспечения;</li>
            <li>Планируемую величину необходимой подключаемой нагрузки, м<sup>3</sup>/сут.( м<sup>3</sup>/час);</li>
            <li>Планшетную топографическую съемку в масштабе 1:500.</li>
        </ul>
        <p>После предоставления Заказчиком запроса, технические условия либо
            мотивированный отказ предоставляются ему через 14 рабочих дней. Для получения
            технических условий Заказчику либо представителю, имеющему нотариально заверенную
            доверенность необходимо обратиться в ОАО «ПО Водоканал»</p>
        <p>Технические условия не являются основанием для изготовления проекта и строительства
            сетей, без заключенного договора о подключении.</p>
        <h3>2. Заключение договора о подключении.</h3>
        <p>После получения технических условий, Заказчику в течение одного года
            необходимо направить в ОАО «ПО Водоканал» заявление на заключение договора о
            подключении и предоставления условий подключения объекта к сетям коммунальной
            инфраструктуры.</p>
        <p>При смене правообладателя земельного участка, которому были выданы
            технические условия, новый правообладатель вправе воспользоваться этими техническими
            условиями, уведомив ОАО «ПО Водоканал» о смене правообладателя.</p>
        <p>При обращении с заявлением, Заказчик предоставляет следующие документы:</p>
        <ul class="ll-single-service__list-no-markers">
            <li>копии правоустанавливающих документов на земельный участок (в случае смены
                правообладателя);</li>
            <li>ситуационный план расположения объекта капитального строительства с привязкой
                к территории населенного пункта;</li>
            <li>документы, подтверждающие полномочия лица действовать от имени заказчика (в случае
                если заявка подается в адрес исполнителя представителем заказчика);</li>
        </ul>
        <p>Для получения (подписания) договора о подключении, Заказчику либо представителю,
            необходимо обратиться в ОАО «ПО Водоканал»</p>
        <p>Срок подготовки договора о подключении не более 30 календарных дней.</p>
        <h3>3. Изготовление проекта.</h3>
        <p>После заключения между ОАО «ПО Водоканал» и Заказчиком договора о
            подключении, Заказчику необходимо обратиться в проектную организацию (имеющую
            соответствующий допуск СРО на выполнение проектных работ) для изготовления проекта
            водоснабжения (канализования) объекта.</p>
        <p>Проектирование и в дальнейшем строительство сетей необходимо выполнять в
            соответствии с выданными ОАО «ПО Водоканал» техническими условиями и условиями
            подключения. Проект согласовать в заинтересованных службах в установленном порядке.</p>
        <p>Для изготовления проекта Заказчику потребуется изготовленная ранее
            откорректированная топографическая съемка в масштабе 1:500.</p>
        <h3>4. Строительство сетей.</h3>
        <p>Для строительства сетей необходимых для подключения объекта к системе водоснабжения
            (канализации) Заказчику необходимо обратиться в строительную организацию (имеющую
            соответствующий допуск СРО на выполнение работ по устройству сетей водоснабжения и
            канализации).</p>
        <p>До начала производства работ (за 3 рабочих дня) Заказчику необходимо направить в ОАО
            «ПО Водоканал» уведомление о дате начала производства работ и во время их выполнения
            направлять уведомления о дате проведения скрытых работ, гидравлических испытаний
            напорных и безнапорных трубопроводов, для участия технического надзора ОАО «ПО
            Водоканал» их освидетельствования.</p>
        <p>Заявление на вызов представителя направлять в ОАО «ПО Водоканал»
            Строительство сетей необходимо выполнять в соответствии с согласованным в
            установленном порядке проектом и условиями подключения, заключенного договора о
            подключении.</p>
        <p>Во время производства работ заказчику необходимо:</p>
        <ul class="ll-single-service__list-no-markers">
            <li>предоставить в ОАО «ПО Водоканал» 1 экземпляр (оригинал) согласованного в
                установленном порядке проекта водоснабжения (канализации).</li>
            <li>При осуществлении строительного контроля представителем ОАО «ПО Водоканал», на
                объекте необходимо присутствие собственника объекта, а так же подрядчика,
                осуществлявшего работы по строительству сетей.</li>
            <li>Присоединение к водопроводной (канализационной) магистрали <b>не производить.</b></li>
            <li>После завершения строительства водопроводной сети необходимо выполнить её промывку
                и дезинфекцию.</li>
        </ul>
        <h3>5. Приемка сетей в эксплуатацию.</h3>
        <p>После строительства водопроводной и (или) канализационной сетей объекта, в том
            числе установки прибора учета в соответствии с проектом и условиями подключения, а
            также подготовки водопроводной сети к присоединению (благоустройства территории,
            получения справки о благоустройстве) Заказчику необходимо предоставить в ОАО «ПО Водоканал» проектную документацию (два экземпляра) с
            перечнем документов <b>(см. ниже)</b>, а также заявление о вызове представителей ОАО «ПО
            Водоканал» на объект для составления акта о готовности сети к подключению.</p>
        <p>Перечень документов необходимых для приемки сетей в эксплуатацию</p>
        <ol>
            <li>Акт на скрытые работы (готовит подрядчик).</li>
            <li>Акт гидравлического испытания коммуникаций и сооружений на прочность и
                герметичность (готовит подрядчик).</li>
            <li>Акт на чистоту траншеи (готовит подрядчик).</li>
            <li>Письмо о невыполнении демонтажа (готовит подрядчик).</li>
            <li>Сертификаты качества трубы и арматуры (готовит подрядчик).</li>
            <li>Справка о благоустройстве территории (готовит подрядчик).</li>
            <li>Письмо&nbsp;— подтверждение от начальника (мастера) участка (района)
                всех имеющихся абонентов (в случае реконструкции уличных сетей) .</li>
            <li>Исполнительная топографическая съемка (со справкой геодезической службы) или
                откорректированная топографическая съемка построенных сетей (с печатью «принята в
                геофонд») (готовит подрядчик).</li>
            <li>Платежный документ за врезку, врубку, промывку.</li>
            <li>Справка о источнике финансирования (при необходимости).</li>
            <li>Справка о сметной (балансовой) стоимости объекта(при необходимости).</li>
        </ol>

        <p>На момент выезда сотрудников ОАО «ПО Водоканал» необходимо обеспечить
            беспрепятственный доступ на объект, личное присутствие собственника и присутствие
            представителя подрядчика, осуществлявшего работы по строительству водопроводных
            (канализационных) сетей, колодцы и камеры открыть для осмотра.</p>
        <p>После оформления и подписания всеми сторонами акта о готовности сети, необходимо:
            согласовать с ПП «Водопровод» в ОАО «ПО Водоканал» ( дату и время мероприятий по
            отключению и опорожнению участка трубопровода, к которому будет осуществляться
            присоединение объекта.</p>
        <p>До начала работ по присоединению (пять рабочих дней), письменно уведомить технический
            надзор ОАО «ПО Водоканал» о дате и времени проведения мероприятий по присоединению,
            для оформления акта о присоединении.</p>
        <p>В день проведения работ по присоединению обеспечить представителю технического
            надзора доступ для осмотра внутриплощадочных и внутридомовых сетей водопровода
            (канализации). Выполнение работ по присоединению, без уведомления ОАО «ПО Водоканал» запрещается.</p>
        <p>На момент выезда представителей ОАО «ПО Водоканал» необходимо обеспечить личное
            присутствие и присутствие подрядной организации (для осуществления присоединения).</p>
        <p>После выполнения присоединения составляется акт о присоединении, производится
            опломбирование запорной арматуры в месте присоединения к городским сетям.</p>
        <p>После присоединения и составления всех соответствующих актов необходимо отобрать
            пробы поступившей на объект воды и произвести её лабораторное исследование.</p>
        <p>Протокол лабораторного исследования воды необходимо предоставить в ОАО «ПО
            Водоканал»</p>
        <p>Гарантийное письмо на обслуживание в течение 2-х лет.
            Оформленная техническая документация выдается Заказчику при заключении (подписании)
            договора на отпуск питьевой воды и прием сточных вод.</p>
        <p>Примечание:
                <ul class="ll-single-service__list-no-markers">
                    <li>При наличии замечаний к построенным сетям Заказчику выдается предписание по их
                        устранению (устанавливается срок устранения).</li>
                    <li>После устранения замечаний необходимо письменно уведомить об этом ОАО «ПО
                        Водоканал» для проверки их выполнения.</li>
                </ul>


        <h3>6. Заключение договора на отпуск воды и прием сточных вод.</h3>
        <p>После оформления проектно-технической документации, Заказчику необходимо
            обратиться с заявлением на заключение договора на отпуск воды и прием сточных вод.</p>
        </div>

    @include('layouts.blocks.ll-headline-for-services-page',['caption'=>'Энергоснабжение','anchor_id' => 'energy'])

    <div class="ll-single-service">
    <p>В первую очередь, необходимо оформление заявки на технологическое присоединение.
            В соответствии с п. 9, п. 10 «Правил технологического присоединения энергопринимающих
            устройств потребителей электрической энергии, объектов по производству электрической энергии, а
            также объектов электросетевого хозяйства, принадлежащих сетевым организациям и иным лицам, к
            электрическим сетям», утвержденных постановлением Правительства РФ от 27.12.2004 No 861 для
            технологического присоединения к электрическим сетям заявитель направляет заявку в сетевую
            организацию, к электрическим сетям которой намеревается произвести технологическое
            присоединение. В заявке, направляемой заявителем, должны быть в зависимости от конкретных
            условий указаны следующие сведения:</p>
        <ol>
            <li>реквизиты заявителя (для юридических лиц&nbsp;— полное наименование и номер записи в Едином
                государственном реестре юридических лиц, для индивидуальных предпринимателей&nbsp;— номер записи
                в Едином государственном реестре индивидуальных предпринимателей и дата ее внесения в реестр,
                для физических лиц&nbsp;— фамилия, имя, отчество, серия, номер и дата выдачи паспорта или иного
                документа, удостоверяющего личность в соответствии с законодательством Российской Федерации);</li>
            <li>наименование и место нахождения энергопринимающих устройств, которые необходимо
                присоединить к электрическим сетям сетевой организации;</li>
            <li>место нахождения заявителя;</li>
            <li>максимальная мощность энергопринимающих устройств и их технические характеристики,
                количество, мощность генераторов и присоединяемых к сети трансформаторов;</li>
            <li>количество точек присоединения с указанием технических параметров элементов
                энергопринимающих устройств;</li>
            <li>заявляемый уровень надежности энергопринимающих устройств;</li>
            <li>заявляемый характер нагрузки (для генераторов&nbsp;— возможная скорость набора или снижения
                нагрузки) и наличие нагрузок, искажающих форму кривой электрического тока и вызывающих
                несимметрию напряжения в точках присоединения;</li>
            <li>величина и обоснование величины технологического минимума (для генераторов),
                технологической и аварийной брони (для потребителей электрической энергии);</li>
            <li>сроки проектирования и поэтапного введения в эксплуатацию энергопринимающих устройств (в
                том числе по этапам и очередям);</li>
            <li>поэтапное распределение мощности, сроков ввода и сведения о категории надежности
                электроснабжения при вводе энергопринимающих устройств по этапам и очередям.</li>
        </ol>
        <p>Также к заявке прилагаются следующие документы:</p>
        <ol>
            <li>план расположения энергопринимающих устройств, которые необходимо присоединить к
                электрическим сетям сетевой организации;</li>
            <li>однолинейная схема электрических сетей заявителя, присоединяемых к электрическим сетям
                сетевой организации, номинальный класс напряжения которых составляет 35 кВ и выше, с
                указанием возможности резервирования от собственных источников энергоснабжения (включая
                резервирование для собственных нужд) и возможности переключения нагрузок (генерации) по
                внутренним сетям заявителя;</li>
            <li>перечень и мощность энергопринимающих устройств, которые могут быть присоединены к
                устройствам противоаварийной автоматики;</li>
            <li>копия документа, подтверждающего право собственности или иное предусмотренное законом
                основание на объект капитального строительства и (или) земельный участок, на котором
                расположены (будут располагаться) объекты заявителя, либо право собственности или иное
                предусмотренное законом основание на энергопринимающие устройства;</li>
            <li>доверенность или иные документы, подтверждающие полномочия представителя заявителя,
                подающего и получающего документы, в случае если заявка подается в сетевую организацию
                представителем заявителя.</li>
        </ol>
        <p>Процедура технологического присоединения к электрическим сетям оговорена в п.7 Правил
            технологического присоединения энергопринимающих устройств потребителей электрической
            энергии, объектов по производству электрической энергии, а также объектов электросетевого
            хозяйства, принадлежащих сетевым организациям и иным лицам, к электрическим сетям,
            утвержденных постановлением Правительства РФ от 27.12.2004 No 861 (далее&nbsp;— Правила), и
            включает в себя следующие этапы:</p>
        <ol>
            <li>подача заявки юридическим или физическим лицом (далее&nbsp;— заявитель), которое имеет намерение
                осуществить технологическое присоединение, реконструкцию энергопринимающих устройств и
                увеличение объема присоединенной мощности, а также изменить категорию надежности
                электроснабжения, точки присоединения, виды производственной деятельности, не влекущие
                пересмотр (увеличение) величины присоединенной мощности, но изменяющие схему внешнего
                электроснабжения энергопринимающих устройств заявителя;</li>
            <li>заключение договора;</li>
            <li>выполнение сторонами договора мероприятий, предусмотренных договором;</li>
            <li>получение разрешения уполномоченного федерального органа исполнительной власти по
                технологическому надзору на допуск в эксплуатацию объектов заявителя (за исключением объектов
                лиц, указанных в пунктах 12.1&nbsp;— 14 настоящих Правил):
                <ul>
                    <li>осуществление сетевой организацией фактического присоединения объектов заявителя к
                        электрическим сетям. Для целей настоящих Правил под фактическим присоединением понимается
                        комплекс технических и организационных мероприятий, обеспечивающих физическое соединение
                        (контакт) объектов электросетевого хозяйства сетевой организации, в которую была подана заявка, и
                        объектов заявителя (энергопринимающих устройств) без осуществления фактической подачи
                        (приема) напряжения и мощности на объекты заявителя</li>
                    <li>фактический прием (подача) напряжения и мощности, осуществляемый путем включения
                        коммутационного аппарата</li>
                </ul>
            </li>
            <li>составление акта о технологическом присоединении и акта разграничения балансовой
                принадлежности и эксплуатационной ответственности.</li>
        </ol>
    </div>
</div>
</div>
@include('layouts.footer')