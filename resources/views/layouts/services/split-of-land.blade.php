@section('title','Раздел земельных участков')
@include('layouts.header')
<div class="llg-wrapper-main__single-service">
    <div class="container">
        @include('layouts.blocks.ll-headline-for-services-page',['caption'=>'Раздел-объединение земельных участков','anchor_id' => 'split-f-land'])
        <div class="ll-single-service">
    <div class="services split-of-land">

        <h3><b>Для раздела-объединения земельных участков, необходимо:</b></h3>
        <ul class="no-padding">
            <li>получение межевого дела на раздел/объединение участка</li>
            <li>согласование межевого дела на раздел/объединение земельного участка</li>
            <li>присвоение почтовых адресов</li>
            <li>постановка на кадастровый учет вновь образованных земельных участков</li>
            <li>получение кадастровых паспортов на вновь образованные земельные участки</li>
            <li>регистрация права собственности в Управлении Росреестра на вновь
                образованные земельные участки</li>
        </ul>
    </div>
</div>
</div>
</div>

@include('layouts.footer')