@section('title','Перевод земельных участков')
@include('layouts.header')

<div class="llg-wrapper-main__single-service">
    <div class="container">
        @include('layouts.blocks.ll-headline-for-services-page',['caption'=>'Перевод земельных участков','anchor_id' => 'moving-between-categories'])
        <div class="ll-single-service">
            <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <p>Земли в Российской Федерации по целевому назначению подразделяются на следующие категории:</p>
                <ol>
                    <li>Земли сельскохозяйственного назначения;</li>
                    <li>Земли поселений;</li>
                    <li>Земли промышленности, энергетики, транспорта, связи, радиовещания, телевидения,
                        информатики, земли для обеспечения косметической деятельности, земли обороны,
                        безопасности и земли иного специального назначения;</li>
                    <li>Земли особо охраняемых территорий и объектов;</li>
                    <li>Земли лесного фонда;</li>
                    <li>Земли водного фонда;</li>
                    <li>Земли запаса.</li>
                </ol>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <p>Отнесение земель к категориям перевод из одной категории в другую осуществляется в отношении:</p>
                <ol>
                    <li>Земель, находящихся в федеральной собственности,&nbsp;— Правительством Российской Федерации;</li>
                    <li>Земель, находящих в собственности субъектов Российской Федерации, и земель
                        сельскохозяйственного назначения, находящихся в муниципальной собственности,&nbsp;— органами
                        исполнительной власти субъектов Российской Федерации;</li>
                    <li>Земли, находящиеся в частной собственности;</li>
                    <li>Земли сельскохозяйственного назначения,&nbsp;— органами исполнительной власти субъектов
                        Российской Федерации;</li>
                    <li>Земли иного целевого назначения,&nbsp;— органами местного самоуправления.</li>
                </ol>

            </div>
            </div>

        <p>Порядок перевода земель из одной категории в другую устанавливается федеральными законами.</p>
        <p>В соответствии с п.3 п.2 ст.11 Федерального закона «О переводе земель или земельных участков из
            одной категории в другую» от 21.12.2004 года №172 Федерального закона перевод земельных
            участков в составе таких земель в другую категорию допускается только при наличии
            положительного заключения государственной экологической экспертизы. В отношении земель
            сельскохозяйственных угодий или земельных участков в составе таких земель в другую категорию
            допускается в исключительных случаях, связанных с добычей полезных ископаемых при наличии
            утверждённого проекта рекультивации земель.</p>
        <p>Закон в соответствии с Земельным кодексом РФ, Федеральным законом «О переводе земель или
            земельных участков из одной категории в другую» регулирует отношения, возникающие в связи с
            отнесением земель или земельных участков в составе таких земель к определённой категории и их
            переводам из одной категории в другую .</p>
        <p>Проводимые в России социально-экономические преобразования в значительной степени затронули
            проблему распределения и использования земельных ресурсов страны.</p>
        <p>Земельные ресурсы, являясь основным национальным богатством страны, в силу своих природных и
            экономических качеств представляют собой объект социально-экономических и общественно-политических отношений, что, в свою
            очередь, диктует необходимость создания принципиально иной системы управления этими,
            отличающейся от управления другими видами материальных ресурсов.</p>
        <p>В соответствии с Земельным кодексом РФ все земли по целевому назначению подразделяются на
            различные категории. Для каждой категории земель предусмотрен свой правовой режим. В
            некоторых случаях может возникнуть необходимость изменить категорию земли, то есть перевести
            земельный участок из одной категории в другую. С 5 января 2005 года действует новый
            Федеральный закон «О переводе земель или земельных участков из одной категории в другую». В
            нём определены порядок инициирования перевода земель, в том числе по решению суда, к
            содержание ходатайств о переводе земель, условия и сроки их рассмотрения и принятия решений, а
            также для отказа в переводе, порядок обоснования решений. В частности, не допускается перевод
            земель в случае законодательного запрета на его осуществление; при наличии отрицательного
            заключения государственной экологической экспертизы и в случае установления несоответствия
            целевого назначения земель утверждённым документом территориального планирования.
            Постановление Правительства Ростовской области от 25.06.2012 No 527 «Об уполномоченном
            органе и утверждении Порядка рассмотрения ходатайств о переводе земель или земельных участков
            из одной категории в другую» регулирует данные отношения на территории Ростовской области</p>
        <p>Для того чтобы изменить категорию земельного участка, необходимо подать ходатайство о переводе
            земель из одной категории в другую или ходатайство о переводе земельных участков из состава
            земель одной категории в другую в исполнительный орган государственной власти или орган
            местного самоуправления, уполномоченные на рассмотрения этого ходатайства.</p>
        <p>В ходатайстве о переводе земельных участков из состава земель сельскохозяйственного назначения
            в другую категорию указывается:</p>
        <ul>
            <li>Кадастровый номер земельного участка;</li>
            <li>Категория земель, в состав которых входит земельный участок, и категория земель, перевод в
                состав которых предполагается осуществить;</li>
            <li>Обоснование перевода земельного участка из состава земель одной категории в другую;</li>
            <li>Права на земельный участок, перевод которого предполагается осуществить.</li>
        </ul>
        <p>К ходатайству о переводе земельных участков из состава земельных участков из состава земель
            сельскохозяйственного назначения в другую категорию прилагается:</p>
        <ul>
            <li>Землеустроительный проект, обосновывающий перевод земельного участка из одной категории
                в другую;</li>
            <li>Выписка из государственного земельного кадастра относительно сведений о земельном участке
                перевод, которого из состава земель одной категории в другую предполагается осуществить;</li>
            <li>Копию документов, удостоверяющих личность заявителя&nbsp;— физического лица, либо выписка из
                единого государственного реестра индивидуальных предпринимателей, или выписка из единого
                государственного реестра юридических лиц;</li>
            <li>Выписка из единого государственного реестра прав на недвижимое имущество и сделок с ним о
                правах на земельный участок, который планируют перевести из одной категории в другую;</li>
            <li>Заключение государственной экологической экспертизы, в случае если её проведение
                предусмотрено федеральными законами;</li>
            <li>Согласование правообладателя земельного участка на перевод земельного участка из состава
                земель одной категории в другую, за исключением случаев, предусмотренных действующим
                законодательством, когда такое согласование с правообладателем не требуется;</li>
            <li>Согласование перевода земель сельскохозяйственного назначения с исполнительным органом
                государственной власти Ростовской области, уполномоченным в сфере сельского хозяйства и
                продовольствия.</li>
        </ul>
        <p>По результатам рассмотрения хозяйства органы исполнительной власти или органы местного
            самоуправления принимают решение о переводе земель в другую категорию или об отказе в
            переводе.</p>
        <p>Не всегда намереваясь изменить категорию земли, можно получить желанный результат. Законом
            предусмотрены основания для отказа в переводе земель в другую категорию.</p>
        <p>Во-первых, это случай, когда с ходатайством обращается ненадлежащее лицо. Во-вторых, откажут
            также, если к ходатайству приложены документы, состав, форма и содержание которых
            противоречат закону. Если ходатайство не подлежит рассмотрению по данным основаниям, то его
            возвращают заинтересованному лицу в течение 30 дней со дня его поступления с указанием причин
            для отказа в принятии ходатайства в делопроизводство. Кроме того, перевод земель из одной
            категории не допускается:</p>
        <ul>
            <li>В случае установления в соответствии с федеральными законами ограничения перевода земель
                или земельных участков в составе таких земель из одной категории в другую либо запрета на
                такой перевод;</li>
            <li>При наличии отрицательного заключения государственной экологической экспертизы, если её
                проведение предусмотрено федеральными законами;</li>
            <li>Когда установлено несоответствие испрашиваемого целевого назначения земель или земельных
                участков утверждённым документом территориального планирования и документации по
                планировке территории, землеустроительной или лесоустроительной документации.</li>
        </ul>
        <p>Акт об отказе в переводе земли может быть обжалован в судебном порядке. Исполнительные органы
            государственной власти или органы местного самоуправления могут ходатайствовать об изменении
            категории земельных участков без согласия правообладателей. Данная ситуация возможна в случаях
            перевода земельных участков в другую категорию для создания особо охраняемых природных
            территорий без изъятия земельных участков у их правообладателей либо в связи с установлением
            или изменением черты поселений. Если в результате будет принят акт о таком переводе, то
            правообладатели могут обжаловать данный документ в судебном порядке.</p>
        <p>Решение о переводе земель в другую категорию или об отказе в таком переводе принимается в
            следующие сроки:</p>
        <ul>
            <li>В течение трех месяцев со дня поступления ходатайства, если иное не установлено
                нормативными правовыми актами РФ, правительством РФ;</li>
            <li>В течение двух месяцев со дня поступления ходатайства&nbsp;— исполнительным органом
                государственной власти субъекта РФ или органом местного самоуправления. По результатам
                рассмотрения принимается акт об отказе в переводе. Данный акт направляется
                заинтересованному лицу в течение 14 дней с момента его принятия. Акт о переводе земель или
                земельных участков не может быть принят на определённый срок.</li>
        </ul>
    </div>
    </div>
</div>

@include('layouts.footer')