@section('title','Тарифы на услуги')
@include('layouts.header')
<div class="llg-wrapper-main__single-service">
<div class="container">
    @include('layouts.blocks.ll-headline-for-services-page',['caption'=>'Тарифы на оказываемые услуги','anchor_id' => 'tariffs'])

    <div class="ll-single-service">
        <div class="services tariffs">
            <table class="table table-bordered" style="text-align: left;"> <thead>
                <tr> <th class="bold">№<br> п/п</th> <th colspan="2" class="bold text-center title">Виды оказываемых услуг</th> <th class="bold text-center title">Стоимость (руб.)</th> </tr>
                </thead>
                <tbody>
                <tr> <td colspan="4" class="bold text-center title">Консультационные услуги</td> </tr>
                <tr> <td>1.</td> <td colspan="2">Устная консультация. Первичный анализ ситуации, общие рекомендации.</td> <td class="text-center text-middle bold">бесплатно</td> </tr>
                <tr> <td colspan="4" class="bold text-center title">Юридические и иные услуги</td></tr>
                <tr> <td>1.</td> <td colspan="2">Разработка  индивидуального гражданско-правового договора в т.ч. купли-продажи, дарения, аренды</td> <td class="text-center text-middle bold">от 3 000</td> </tr>
                <tr> <td>2.</td> <td colspan="2">Комплексное юридическое сопровождение сделок с недвижимым имуществом по самостоятельно подобранному клиентом варианту</td> <td class="text-center text-middle bold">от 20 000</td> </tr>
                <tr> <td>3.</td> <td colspan="2">Представление интересов клиента в Управлении Росреестра по Ростовской области по вопросам регистрации права собственности</td> <td class="text-center text-middle bold">4 000</td> </tr>
                <tr> <td>4.</td> <td colspan="2">Помощь в составлении исковых заявлений в суд</td> <td class="text-center text-middle bold">По договоренности</td> </tr>
                <tr> <td colspan="4" class="bold text-center title">Оформление документов по различным операциям</td></tr>
                <tr> <td>1.</td> <td colspan="2">Открытие лицевых счетов по оплате коммунальных платежей</td> <td class="text-center text-middle bold">5 000</td> </tr>
                <tr> <td>2.</td> <td colspan="2">Оформление новой домовой книги</td> <td class="text-center text-middle bold">2 500</td> </tr>
                <tr> <td rowspan="2">3.</td> <td rowspan="2">Оформление документов на вступление в наследство на общих основаниях</td> <td>Жилые помещения в квартирах</td> <td class="text-center text-middle bold">12 000</td> </tr>
                <tr> <td>Домовладения, садовые и земельные участки</td> <td class="text-center text-middle bold">15 000</td> </tr>
                <tr> <td>4.</td> <td colspan="2">Оформление документов на вступление в наследство с установлением родственных связей и других оснований (без представления интересов в суде)</td> <td class="text-center text-middle bold">20 000</td> </tr>
                <tr> <td>5.</td> <td colspan="2">Получение выписки из ЕГРП</td> <td class="text-center text-middle bold">350</td> </tr>
                <tr> <td rowspan="2">6.</td> <td rowspan="2">Получение дубликата правоустанавливающего документа, взамен утраченного</td> <td>Жилые объекты недвижимости</td> <td class="text-center text-middle bold">9 000</td> </tr>
                <tr> <td>Иные объекты недвижимости</td> <td class="text-center text-middle bold">12 000</td> </tr>
                <tr> <td>7.</td> <td colspan="2">Ввод жилого дома в эксплуатацию</td> <td class="text-center text-middle bold">20 000</td> </tr>
                <tr> <td>8.</td> <td colspan="2">Регистрация объектов незавершенного строительства, а так же объектов завершенного строительства по дачной амнистии</td> <td class="text-center text-middle bold">8 000</td> </tr>
                <tr> <td>9.</td> <td colspan="2">Получение строительной документации для строительства жилых домов, объектов капитального строительства иного назначения (при условии соответствия вида разрешенного использования земельного участка)</td> <td class="text-center text-middle bold">30 000</td> </tr>
                <tr> <td>10.</td> <td colspan="2">Раздел (объединение) домовладений и земельных участком</td> <td class="text-center text-middle bold">25 000</td> </tr>
                <tr> <td rowspan="2">11.</td> <td rowspan="2">Узаконение самовольно возведенных строений</td> <td>Жилой дом</td> <td class="text-center text-middle bold">20 000</td> </tr>
                <tr> <td>Хоз. постройки, пристройки</td> <td class="text-center text-middle bold">10 000</td> </tr>
                <tr> <td>12.</td> <td colspan="2">Узаконение самовольного переоборудования в квартире (комнате коммунальной квартире)</td> <td class="text-center text-middle bold">25 000</td> </tr>
                <tr> <td>13.</td> <td colspan="2">Оформление земельного участка в собственность или аренду (упрощенка)</td> <td class="text-center text-middle bold">12 000</td> </tr>
                <tr> <td>14.</td> <td colspan="2">Изменение вида разрешенного использования земельного участка (без публичных слушаний)</td> <td class="text-center text-middle bold">6 000</td> </tr>
                <tr> <td>15.</td> <td colspan="2">Оформление договора аренды земельного участка с регистрацией в Управлении Росреестра</td> <td class="text-center text-middle bold">25 000</td> </tr>
                <tr> <td>16.</td> <td colspan="2">Выкуп земельного участка</td> <td class="text-center text-middle bold">25 000</td> </tr>
                <tr> <td>17.</td> <td colspan="2">Изменение назначения помещений (перевод из нежилого в жилое, перевод из жилого в нежилое)</td> <td class="text-center text-middle bold">40 000</td> </tr>
                <tr> <td>18.</td> <td colspan="2">Перевод в нежилой фонд частных домовладений с изменением вида целевого использования земельного участка (без публичных слушаний)</td> <td class="text-center text-middle bold">25 000</td> </tr>
                <tr> <td>19.</td> <td colspan="2">Ввод в эксплуатацию нежилого помещения, переведенного из жилого</td> <td class="text-center text-middle bold">25 000</td> </tr>
                <tr> <td>20.</td> <td colspan="2">Ввод в эксплуатацию нежилых вновь построенных зданий (при наличии разрешения)</td> <td class="text-center text-middle bold">от 25 000</td> </tr>
                <tr> <td>21.</td> <td colspan="2">Снятие обременений (рента, арест, ипотека )</td> <td class="text-center text-middle bold">4 000</td> </tr>

                </tbody>
            </table>

            <br>
            <p><b>Примечания:</b> <br>
                1. Цена указана без учета стоимости необходимых документов, оплаты государственных пошлин.<br>
                2. Данный перечень не является исчерпывающим. По согласованию сторон, в соответствии с компетенцией сотрудников агентства, оказываются и иные виды услуг. При этом цены  на такие услуги согласовываются сторонами дополнительно.<br>
                3.  Оказание услуг осуществляется на основании заключенных договоров. Агентство оставляет за собой право предоставлять клиентам скидки.<br>
                4. Постоянные скидки для граждан следующих категорий: <br>
                – пенсионеры (при предъявлении пенсионного удостоверения)&nbsp;— 10%<br>
                – молодая семья (один из членов семьи в возрасте до 35 лет)&nbsp;— 10%<br>
                – военнослужащие&nbsp;— 10%<br>
                5. Постоянная акция «Счастливый понедельник»&nbsp;— с 9:00 до 12:00 скидка&nbsp;— 20%<br>
                6. Скидки указанные в п.4. и п.5. не суммируются.<br>
                7. Скидки предоставляются до заключения договора.<br></p> </div>

        </div>
    </div>
</div>

@include('layouts.footer')