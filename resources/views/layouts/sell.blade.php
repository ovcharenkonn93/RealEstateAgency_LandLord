@section('title')
	{{$title}}
@stop

@section('description')
	{{$description}}
@stop

@section('meta_keywords')<meta name="keywords" content=" {{$meta_keywords}} "/>@stop

@include('layouts.header')

<div class="llg-wrapper llg-wrapper-main llg-wrapper-main__sell">

        <div class="row">
            @include('layouts.blocks.ll-sell-header')
        </div>
    <div class="container">
    <div class="row">
            @include('layouts.blocks.ll-sell-form')
        </div>
    </div>
    <div class="container margin-top-50px">
		<div class="row">
			@include('layouts.blocks.ll-similar-offers')
		</div>
    </div>
</div>

@include('layouts.blocks.ll-splitter')
@include('layouts.footer')