@extends('app')

@section('content')

					@if (count($errors) > 0)
						<div class="col-md-4 col-md-offset-4 margin-top-20px">
							<div class="alert alert-danger">
								Ошибка ввода.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					@endif
					
<div class="row margin-top-20px" >

		<div class="col-md-4 col-md-offset-4">
					<form class="form-horizontal" role="form" method="POST" action="auth/register">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Имя</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail адрес</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Пароль</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Повтор пароля</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
							
								<input type="submit" value="Регистрация">

							</div>
						</div>
					</form>	

					<div class="text-center">
						<a href="login">Вход</a>
					</div>
		</div>
</div>

@endsection



