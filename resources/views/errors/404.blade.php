<html>
	<head>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href="{{ url('css/landlord.css') }}" rel="stylesheet">
		<style>
			body{
				background-color: #f1f1f1;	
				font-family: 'San Francisco', 'Arial', sans-serif;	
				text-align: center;				
			}
			.error_page{
				width: 100%;
				height: 100%;
				position: absolute;
			}
			.error {
				width: 100%;
				color: #B72C2C;
				font-size: 130px;
				text-align: center;
				pading: 30px;
				margin-top: 30px;
				border-bottom: 2px solid #B72C2C;
			}
			h1{
				color: gray;
				text-align: center;
				font-weight: lighter;
			}
			a{
				font-weight: lighter;
				font-size: 20px;
				color: #000;
			}
			a{
				font-weight: lighter;
				font-size: 20px;
			}
		</style>
	</head>

	<body>
		<div class="error_page">		
			<div class="error">404</div>
			<h1>Страница не найдена</h1>
			<div class="container">
                <div class="row">
                @include('layouts.blocks.ll-home-offers')
            	</div>
            </div>
		</div>
	</body>

</html>
