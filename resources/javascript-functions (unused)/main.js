jQuery(document).ready(function(){
    var width_window=$(window).width();
    var classJS = 'hsj-open';
    //------------------------------------------------
    if ($('.hsb-contacts').length > 0) (function(){
	$('.hsb-contacts').each(function(){
	    var self = $(this);
	    var icon = self.find('.hsb-contacts__icons li');
	    
	    //var content = self.find('.hsb-contacts__popover');
	    
	    var time = 400;
	    
	    icon.click(function(){
		//var index = $(this).data('index');
		var content = self.find('.hsb-contacts__popover');
		self.find(content);
		
		if ($(this).hasClass(classJS)) {
		    self.find(content).slideDown(time);
		}else{
		    self.find(content).slideUp(time);
		}
		
		$(this).toggleClass(classJS);
	    });
	});
    })();
    //------------------------------------------------
    if (width_window > 768) {
	if ($('.hsb-products').length > 0) (function(){
	    var self = $('.hsb-products');
	    var left_arrow = self.find('.hs-carousel__prev');
	    var right_arrow = self.find('.hs-carousel__next');
	    var container = parseInt($(document).find('.container:first-child').css('width'));
	    console.log(container);
	    var width = ($(window).width() - container)/2;
	    
	    left_arrow.css('left',width);
	    right_arrow.css('right',width);
	})();
    }
    //------------------------------------------------
    if ($('.hsj-toggle').length > 0) (function(){
	$('.hsj-toggle').each(function(){
	    var button = $(this).find('.hsj-toggle__btn');
	    var list = $(this).find('.hsj-toggle__list');
	    var clssJS = 'hsj-toggle_open';
	    var time = 300;
	    
	    button.click(function(){
		if ($(this).hasClass(classJS)) {
		    list.stop().slideUp(time);
		}else{
		    list.stop().slideDown(time)
		}
		
		$(this).toggleClass(classJS);
	    });
	    
	});
    })();
    //------------------------------------------------
    $.widget( "hsj-select_icon", $.ui.selectmenu, {
	_renderItem: function( ul, item ) {
	    var li = $( "<li>", { text: item.label } );
	    
	    if ( item.disabled ) {
	      li.addClass( "ui-state-disabled" );
	    }
	    
	    $( "<span>", {
	      style: item.element.attr( "data-style" ),
	      "class": "ui-icon " + item.element.attr( "data-class" )
	    })
	      .appendTo( li );
	      
	    return li.appendTo( ul );
	}
    });
});